﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticController :MonoBehaviour {

    public void MoveToAndProcess(GameObject location)
    {
        Controller controller = TurnManager.active_player.GetComponent<Controller>();
        controller.MoveToAndProcess(location);
    }

    //if is local palyer...

    public void MoveTo(GameObject location, bool process_location = true)
    {
        Controller controller = TurnManager.active_player.GetComponent<Controller>();
        controller.MoveTo(location,process_location);
    }



}
