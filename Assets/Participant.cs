﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Participant : MonoBehaviour {




	Character character;
	public string character_name ="";
	[SerializeField] public List<Stat> Stats = new List<Stat>(){};
	[SerializeField] public Stat hp = new Stat("hp");
	[SerializeField] public Stat mp = new Stat("mp");
	public List<string> unlocked_skills = new List<string>();
	float default_stat;
	public int gold_drop=2;
	public PlayerVariables pvar;
	public Encounter encounter;



	public void Initialize(Character original, Encounter new_encounter)
	{
		character_name = original.character_name;
		mp.Base = original.mp.Base;
		hp.Base = original.hp.Base;
		mp.Actual = original.mp.Base;
		hp.Actual = original.hp.Base;
		this.encounter=new_encounter;
		for(var i=0; i<original.Stats.Count ;i++)
		{
			this.Stats.Add (new Stat (original.Stats [i].Name, original.Stats [i].Base, original.Stats [i].Actual,
				original.Stats [i].Min, original.Stats [i].Max, original.Stats [i].Repuation, original.Stats [i].Decay));
		}


	}
	public void Initialize(PlayerVariables original, Encounter new_encounter)
	{
		character_name = original.character_name;
		mp.Base = original.mp.Base;
		hp.Base = original.hp.Base;
		mp.Actual = original.mp.Base;
		hp.Actual = original.hp.Base;
		this.encounter=new_encounter;

		for(var i=0; i<original.Stats.Count ;i++)
		{
			this.Stats.Add (new Stat (original.Stats [i].Name, original.Stats [i].Base, original.Stats [i].Actual,
				original.Stats [i].Min, original.Stats [i].Max, original.Stats [i].Repuation, original.Stats [i].Decay));
		}

		action_button_panel = original.action_button_panel;
		target_button_panel=original.target_button_panel;
		is_player = true;
		pvar = original;
	}





	public string unique_name ="";
	public int unique_index = 1;


	public Sprite sprite;



	public List<string> status_effects = new List<string>() ;


	public Dictionary<Participant, float> aggros = new Dictionary<Participant, float>();
	public Dictionary<Participant, bool> enemies = new Dictionary<Participant, bool>();
	public Dictionary<Participant, bool> allies = new Dictionary<Participant, bool>();





	[SerializeField] GameObject action_button_panel;
	[SerializeField] GameObject target_button_panel;


	//[SerializeField] Button button;  //questionable idea!  maybe assign a UI button to each participant..
	//sounds simple, unless we want to show opposing views...  in which case this whole 'participant' idea dosn't work in multiplayer...

	public float time_till_next_turn=0;
	public int player_number=-1;

	public bool is_player = false;

	bool choosing_target = false;

	//list of skills or wathever...


	public List <Act>  possible_actions = new List<Act>();
	public List <Act>  accessible_actions = new List<Act>();

	public List <Participant>  possible_targets = new List<Participant>();
	public List <Participant>  selected_targets = new List<Participant>();


	int enemy_threshold =-10;
	int friend_threshold= 10;



	//	void set_enemies_allies()
	//	{
	//		foreach (var entry in enemies) {
	//			if (is_my_enemy(entry.Key))	
	//				entry.Value = true;
	//				else
	//				entry.Value = false;
	//			}
	//
	//		foreach (var entry in allies) {
	//			if (is_my_ally(entry.Key))	
	//				entry.Value = true;
	//			else
	//				entry.Value = false;
	//		}
	//	}

	public bool is_my_enemy(Participant participant)
	{
		float aggro;
		if (aggros.TryGetValue (participant, out aggro)) {
			if (aggro < enemy_threshold)
				return true;
			else
				return false;
		}
		return false;
	}
	public bool is_my_ally(Participant participant)
	{
		float aggro;
		if (aggros.TryGetValue (participant, out aggro)) {
			if (aggro < friend_threshold)
				return true;
			else 
				return false;
		}
		return false;
	}




	public void create_unique_name()
	{
		unique_name = character_name;
		if (encounter.participants.Count == 1)
			return;
		bool acceptable_name_found = false;

		while (!acceptable_name_found) {
			int unacceptable_names = encounter.participants.Count;
			foreach (Participant participant in encounter.participants) {
				if (participant == this) {
					unacceptable_names -= 1;
					continue;
				}
				else if (participant.character_name == this.unique_name) {
					unique_name = character_name + " " + 1.ToString ();
					break;  //start over again but with 1s..
					//drunk drunk  drunk : becomes drunk 1, drunk 1,drunk 1
				} else if (participant.unique_name == this.unique_name) {
					unique_index += 1;
					unique_name = character_name + " " + unique_index.ToString ();
					break;
					//drunk 1, drunk 2,drunk 3
				} else
					unacceptable_names -= 1;
			}
			if (unacceptable_names == 0)
				acceptable_name_found = true;
			//this is an incredibly dumb way to do this, but basically loop until all the names do not match.
		}
	}



	//relationship to every other participant?  i guess this shoudl actually be a dictionary...
	//maybe a 2d tuple dictionary? kept in the encoutner rather than out here???
	//I don't know.


	public void take_turn()
	{
		time_till_next_turn += (float)encounter.participants.Count;
		//so we don't take the same turn twice...
		possible_actions.Clear();
		accessible_actions.Clear();

		//start by coming up with a list of valid acts...
		foreach (var entry in ActDictionary.Acts) 
		{
			Act act = entry.Value;
			if (act.name == "")
				continue; 
			if (!act.valid_states.Contains (encounter.state))
				continue;
			possible_actions.Add (act);
		}

		//then further limit that list based on resources...
		foreach (var act in possible_actions)
		{	
			accessible_actions.Add (act);
		}

		//then decide what to do...

		Debug.Log (character_name + " is taking their turn!");




		if (is_player) 
		{
			show_buttons();
			encounter.encounter_text.DisplayText ("Choose an action");

		} else {
			action_button_panel.SetActive(false);
			have_ai_decide_on_action ();

		}


	}

	void show_buttons(){
		for (int i = 0; i < action_button_panel.transform.childCount;i++) 
		{
			Button button = action_button_panel.transform.GetChild(i).GetComponent<Button> ();
			Text button_text = button.transform.GetComponentInChildren<Text>();
			if (i >= possible_actions.Count) 
			{
				button.gameObject.SetActive(false);
				button_text.text="error doh!";
			}
			else 
			{
				Act possible_act = possible_actions [i];

				button_text.text = possible_act.name;
				button.gameObject.SetActive(true);
				button.enabled=false;

				foreach(Act act in  accessible_actions)
				{
					if(act.name==possible_act.name) 	button.enabled=true;
				}

			}
		}
		action_button_panel.SetActive(true);



		for (int i = 0; i < target_button_panel.transform.childCount;i++) 
		{
			Button button = target_button_panel.transform.GetChild(i).GetComponent<Button> ();
			Text button_text = button.transform.GetComponentInChildren<Text>();
			if (i >= encounter.participants.Count) 
			{
				button.gameObject.SetActive(false);
				button_text.text="error doh!";
			}
			else 
			{
				button_text.text = encounter.participants[i].unique_name;
				button.gameObject.SetActive(true);
				button.enabled=false;

				//				foreach(Act act in  accessible_actions)
				//				{
				//					if(act.name==possible_act.name) 	button.enabled=true;
				//				}

			}
		}
		target_button_panel.SetActive(true);


	}


	public void get_list_of_participants()
	{
	}


	public bool is_my_turn(){
		return encounter.current_participant == this;	
	}

	public bool player_clicks_on_action(Text action_text){
		string action_name = action_text.text;
		Act act;
		Debug.Log ("you clicked on " + action_name);
		bool valid_action = false;
		if (!is_my_turn()) {
			Debug.Log ("it's not "+unique_name+"'s turn");
			return false;
		}
		if (pvar == null) return false;

		if (ActDictionary.Acts.TryGetValue(action_name, out act))
		{

			if (count_of_possible_targets (act.valid_target) == 0) {
				return false;
			}

			else if (count_of_possible_targets (act.valid_target) == 1) {
				act.do_act (this, selected_targets);
				return true;
			}

			else if (count_of_possible_targets (act.valid_target) > 1) {
				choosing_target = true;
				return true;
			}	

		}

		return false;
		end_turn ();
	}




	public bool player_clicks_on_target(Text target_text){
		if (!choosing_target)
			return false;

		//somehow need to get the participant from the target...


		choosing_target = false;
		return true;
	}

	int count_of_possible_targets(Act_On act_on)
	{
		int count_of_options = 0;

		possible_targets.Clear();
		selected_targets.Clear();

		switch (act_on) 
		{
		case Act_On.enemies:
			foreach (Participant participant in encounter.participants) {
				if (is_my_enemy (participant)) {
					selected_targets.Add (participant);
				}	
			}
			if (selected_targets.Count > 0)
				count_of_options = 1;
			break;
		case Act_On.allies:
			foreach (Participant participant in encounter.participants) {
				if (is_my_ally(participant)){
					selected_targets.Add (participant);
				}	
			}
			if(selected_targets.Count>0) count_of_options= 1;
			break;

		case Act_On.self:
			selected_targets.Add (this);
			count_of_options= 1;
			break;

		case Act_On.all:
			foreach (Participant participant in encounter.participants) {
				selected_targets.Add (participant);
			}
			count_of_options= 1;
			break;

		case Act_On.others:
			foreach (Participant participant in encounter.participants) {
				if (participant!=this)
					selected_targets.Add (participant);
			}
			count_of_options= 1;
			break;

		case Act_On.none:
			count_of_options= 1;
			break;

		case Act_On.enemy:
			foreach (Participant participant in encounter.participants) {
				if (is_my_enemy(participant)) possible_targets.Add(participant);	
			}
			break;

		case Act_On.ally:
			foreach (Participant participant in encounter.participants) {
				if (is_my_ally(participant)) possible_targets.Add(participant);	
			}
			break;

		case Act_On.player:
			foreach (Participant participant in encounter.participants) {
				if (participant.is_player) possible_targets.Add(participant);	
			}
			break;

		case Act_On.npc:
			foreach (Participant participant in encounter.participants) {
				if (!participant.is_player) possible_targets.Add(participant);	
			}
			break;

		default:
			return 0;
		}
		if (count_of_options == 0)
			count_of_options = possible_targets.Count;

		return count_of_options;

	}


	void have_ai_decide_on_action()
	{
		//	StartCoroutine ("AI_is_acting");
		//checks threats?
		//if an action imparts a status effect then don't use that..
		//if can kill then kill
		//only heal if wounded


	}


	IEnumerator AI_is_acting(){
		yield return new WaitForSeconds (3f);
		end_turn ();
	}


	void end_turn(){
		encounter.pass_time();
	}




	/// <summary>
	/// unused...
	/// </summary>
	/// <returns><c>true</c>, if individual target was checked, <c>false</c> otherwise.</returns>
	/// <param name="act_on">Act on.</param>
	/// <param name="target">Target.</param>
	bool check_individual_target(Act_On act_on, Participant target)
	{
		switch (act_on) {
		case Act_On.enemy:
			if (is_my_enemy (target))
				return true;
			break;
		case Act_On.ally: 
			if (is_my_ally(target))
				return true;
			break;
		case Act_On.player: 
			if (target.is_player)
				return true;
			break;
		case Act_On.npc:
			if (!target.is_player)
				return true;
			break;

		default: return false;
		}
		return false;
	}


}

