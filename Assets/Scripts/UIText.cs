﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


public class UIText : MonoBehaviour {

	public PlayerVariables pvar;
	public Transform text_box;
	//public TextMeshProUGUI text_box_text;
	public TextMeshProUGUI text_box_text;

	string text_to_type = "";
	float pause_between_letters= .05f;
	List <string> list_of_text = new List<string> ();

	public bool normally_disappear=true;
	//running total woudl be a list or dictionary or something....?

	public List<string> current_actions_to_display;
	//this is raw...


	// Use this for initialization
	void Start () {
	    QuitText();
		text_box_text.text = "";

	}

	//effing up with the action code.
	//the action code shoudl be a class.. which has a split.. and returns an action, a description, and a text..
	//instead... the code is split in player variables, card_image_manager, and here...
	//on display it activates the text.. on click it deactivates the textbox...
	//text needs to expadn to the length of the lsit.
	//how do we handle success/failure?  text shoudl not be conbined in that case-- it shoudl be replacing text...
	//are there other text expression we do want to combine though?

	public void print_all_actions(List<string> actions, string optionaltext="")
	{
		text_box.gameObject.SetActive(true);
		string final_output = "";
		foreach (string action_string in current_actions_to_display) {
			if(action_string!=""){
				string formatted_action_string = format_action_string (action_string);
				if (final_output == "")
					final_output = formatted_action_string;
				else final_output += '\n' + formatted_action_string;
				//don't want to accidentally create an extra space at the top.....
			}
		}
		if (optionaltext != "") {
			if (final_output == "")
				final_output = optionaltext;
			else
				final_output += '\n' + optionaltext;
		}

		DisplayText (final_output);

	}



	//this whole part is asking if we can combin actinos, eg. player gains 2 strength, player gains 1 strength, then we can display: player gains 3 strength.
	public void add_to_current (string action_string_to_add)
	{
		string[] parts_to_add = action_string_to_add.Split ('|');

		bool string_found = false;
		string combined_string = "";

		for (var i = 0; i < current_actions_to_display.Count; i++) {

			string current_action_string = current_actions_to_display [i];
			string[] parts = current_action_string.Split ('|');

			if(parts[0] != parts_to_add[0]) continue;

			if (parts [0] == "Text" || parts [0] == "Unlock") {
				if (parts [1] == parts_to_add [1]) {
					string_found = true;
					current_actions_to_display [i] = current_action_string;
					break;
				}
			} else if (parts [0] == "GiveItem") {
				if (parts [1] != parts_to_add [1])
					continue;
				int count1=1;
				int count2=1;
				if (parts.Length == 3 )
					int.TryParse (parts [2], out count1);
				if( parts_to_add.Length==3)	
					int.TryParse (parts_to_add [2], out count2);
				string_found = true;
				current_actions_to_display [i] = parts[0]+'|' + parts[1]+ '|' + (count1 + count2).ToString();
				break;
			}

			else if (parts [0] == "GiveStat") {
				if (parts [1] != parts_to_add [1])
					continue;
				int amount1=0;
				int amount2=0;
				if (parts.Length == 3 )
					int.TryParse (parts [2], out amount1);
				if( parts_to_add.Length==3)	
					int.TryParse (parts_to_add [2], out amount2);
				string_found = true;
				current_actions_to_display [i] = parts[0]+'|' + parts[1]+ '|' + (amount1 + amount2).ToString();
				break;
			}
		}
		if (!string_found)
			current_actions_to_display.Add (action_string_to_add);
	}




	public string format_action_string (string action_string)
	{
		string formatted_string = "";
		string[] parts = action_string.Split ('|');
		if (parts [0] == "Unlock") {
			formatted_string = "You've unlocked a new skill: " + parts [1];

		} else if (parts [0] == "Text") {
			formatted_string = parts [1];

		} else if (parts [0] == "GiveItem") {
			int count = 1;
			if (parts.Length == 3)
				int.TryParse (parts [2], out count);
			if (count == 1)
				formatted_string = pvar.player_name + " gets " + parts [1];
			else if (count > 1)
				formatted_string = pvar.player_name + " gets " + count + " " + parts [1];
			else if (count < 1)
				formatted_string = pvar.player_name + " loses " + count + " " + parts [1];

		} else if (parts [0] == "GiveStat") {
			float amount = 0;
			if (float.TryParse (parts [2], out amount)) {
				if (amount > 0)
					formatted_string = pvar.player_name + " gains " + amount + " " + parts [1] + ". Now it is " + pvar.get_stat_from_string(parts[1]).Actual;
				else if (amount < 0)
					formatted_string = pvar.player_name + " loses " + amount + " " + parts [1] +". Now it is " + pvar.get_stat_from_string(parts[1]).Actual;
			}

		} else if (parts [0] == "MakeHome") {
			formatted_string = pvar.player_name + " now lives at " + parts [1];

		} else if (parts [0] == "DestroyItemType")
			formatted_string = pvar.player_name + " no longer has " + parts [1];

		return formatted_string;
	} 

	public void sort_action_list()
	{
	}

	public void clear_current_actions_to_display()
	{
		current_actions_to_display.Clear ();
		text_box.gameObject.SetActive (false);
	}







	//+ '\n'
	//needs to be a button that you can click on to scroll through things...

	//need to create a list of lines..
	//gain 0 gold
	//gain 1 reason
	//gain 2 strength 
	//need to add to the list..
	//need to check the list for each thing that's getting displayed...
	//then display the whole list...
	//need an inbetween step... telling it to display a stat or item gain..
	//then that inbetween step can be searched through for duplicates...
	//so the in between step has something like: 
	//stat 3/strength
	//or text/blah blah blah
	//or item/something
	//so basically all the action translators should be duplicated over here.
	//that way they can be sorted and checked for duplicates between actions.


	//also we need a timer and a click interface so it goes the heck away.
	//accumulate a display text... so maybe if you just worked a job, it just keeps running total of each stat, and replaces the old total with the new.
	//have the text disappear when when you click on something else.
	//have the text remain sometimes..
	//have different texts accumulate, either into a longer paragraph that drifts down the screen, or else they are waiting in a queue for the player to hit the button or whatever.
	//in any case CONSIDER A TEXT MANAGER.  that keeps track of more than just strings.  and notes recent strings?
	//like something that collects all your actions?  together... and then relates text based on all your actions.
	//and knows if youv'e been doing some things a lot or whatever..?
	//so maybe it accumulates, and then either you click on it, or it just disappears after a while,
	//if it's inconsequential-- which is another thing-- simplest is an optional work for click argument..




	public void TypeOutText(string text_to_type, float pause_between_letters=.05f){
		text_box_text.text = "";


		if (text_to_type == text_box_text.text)
			return;
		
		this.text_to_type = text_to_type;
		this.pause_between_letters = pause_between_letters;
		StartCoroutine("TypeText");
	}


	IEnumerator TypeText()
	{
		for (var i =1;i<=text_to_type.Length;i++)

			{
			
			text_box_text.text =text_to_type.Substring(0,i);

			yield return new WaitForSeconds (pause_between_letters);
			}

	}


	//really no matter what we should be appending a list of strings...
	//and if the list is longer than on we shoudl be waiting for clicks?  potentially between them.

	public void DisplayText(string text_to_display, bool wait_for_click =false)
	{
		//if(text_box_text!="") 
			//if there's already something there then we hsould be waiting for the playr to click first...
		if(text_to_display==text_to_type) return;
		if(text_to_display==text_box_text.text) return;


		StopCoroutine("QuitText");
		text_box.gameObject.SetActive(true);
		TypeOutText (text_to_display);

		//text_box_text.text = text_to_display;

		if (!wait_for_click&&normally_disappear) {
			StartCoroutine ("QuitText");
		}
		else StartCoroutine ("Wait_for_click");
	}

	IEnumerator Wait_for_click()
	{
		//GameObject text_disappears = default(GameObject)
		//if (text_disappears == default(GameObject))
		//	text_disappears = text_box.gameObject;

		GameObject text_disappears = text_box.gameObject;
		yield return new WaitForSeconds (.3f);

		bool have_clicked = false;

		while (have_clicked == false) {
			yield return new WaitForSeconds (.05f);
			if (Input.GetMouseButtonDown(0) || Input.GetKeyDown("space")) {
				text_disappears.SetActive (false);
				text_box_text.text = "";
				have_clicked = true;
			}
		}
	}


	IEnumerator QuitText()
	{
		yield return new WaitForSeconds(2f);
		text_box_text.text = "";
		text_box.gameObject.SetActive(false);
	}



	// Update is called once per frame
	void Update () {

	}
}
