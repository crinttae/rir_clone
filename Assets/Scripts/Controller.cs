﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Controller : MonoBehaviour {

    //public GameObject current_location;
	public PlayerVariables pvar;
    
    public View_Inventory view_inventory;
    GameObject last_location_entered;

    CardImageManager card_image_manager;
    List<Vector3> path = new List<Vector3>();

    public GameObject player_sprite;
    [SerializeField] GameObject player_sprite_prefab;

    //should create a sprite on start...
    void Awake()
    {

        player_sprite = Instantiate(player_sprite_prefab);
        player_sprite.transform.SetParent(GameObject.Find("PlayerSprites").transform);
       // player_sprite.transform.SetParent(GameObject.Find("Hills").transform);

        player_sprite.transform.position = GameObject.Find("Hills").transform.position ;

        //player_sprite.name = "player sprite "+
        //uuhh don't actually knwo what index i am... weird...

        pvar = GetComponent<PlayerVariables>();
        //view_inventory = GetComponent<View_Inventory>();
        card_image_manager = GetComponent<CardImageManager>();
    }

    void Update()
    {

        var input = Input.inputString;
        if (input != "")
        { 
             switch (input)
            {
                
                case "1": DataManager.Switch_Save_Slot(input); break;
                case "2": DataManager.Switch_Save_Slot(input); break;
                case "3": DataManager.Switch_Save_Slot(input); break;
                case "4": DataManager.Switch_Save_Slot(input); break;
                case "5": DataManager.Switch_Save_Slot(input); break;
                case "6": DataManager.Switch_Save_Slot(input); break;
                case "7": DataManager.Switch_Save_Slot(input); break;
                case "8": DataManager.Switch_Save_Slot(input); break;
                case "9": DataManager.Switch_Save_Slot(input); break;
             }
        }

        if(Input.GetKeyDown(KeyCode.F5)) DataManager.Save_Game();
        if (Input.GetKeyDown(KeyCode.F7)) DataManager.Load_Game();
        



    }

    void get_cards_for_location(GameObject location)
    {
        
        view_inventory.Process(location.GetComponent<Inventory>());
        
        
    }

    void next_waypoint_and_process()
    {
        path.RemoveAt(0);
        if (path.Count == 0)
        {
       // PlayerVariables.location = last_location_entered;
        get_cards_for_location(last_location_entered); 
        return;
        }
        Vector3 position = path[0];


		MovePlayer (position, true);
    }



	void MovePlayer(Vector2 target, bool check_for_cards=true)
	{


		float speed = pvar.actual_speed;
		float time = Vector2.Distance (player_sprite.transform.position , target) / 500 / speed;
		card_image_manager.ImmediatelyCancelDescription ();



		pvar.give_time (time);
		if(check_for_cards) LeanTween.move(player_sprite, target, time).setEase(LeanTweenType.linear).setOnComplete(next_waypoint_and_process);
		else LeanTween.move(player_sprite, target, time).setEase(LeanTweenType.linear).setOnComplete(next_waypoint_no_process);

	}


    void next_waypoint_no_process()
    {
        path.RemoveAt(0);
        //PlayerVariables.location = last_location_entered;
        if (path.Count == 0)
        {
            
            return;
        }
        Vector3 position = path[0];
    	
		//movement speed?

		MovePlayer (position, false);

	}




    public void MoveToAndProcess(GameObject location)
    {

		pvar.uitext.clear_current_actions_to_display ();

        MoveTo(location,true);
    }

        //if is local palyer...

    public void MoveTo(GameObject location, bool process_location = true)
    {
        last_location_entered = location;
		pvar.location = last_location_entered;
        //this is a totally sketch way to handle this-- basically setting location up front, and then whent he player gets there, we tell him he has this location...?

        if (card_image_manager!=null) card_image_manager.destroy_spread();
        //stats.location = location;
        var position = location.transform.GetChild(0).position;
        


        path.Add(position);

        if (path.Count > 1) return;
		MovePlayer (position, process_location);
    }


    



}
