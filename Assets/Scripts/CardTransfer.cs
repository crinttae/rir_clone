﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardTransfer : MonoBehaviour {

    public GameObject realcard;
    public bool keeprealcard=true;

    //void move(GameObject original, Transform target, bool keep = true)
    //{
    //    realcard = original;
    //    keeprealcard = keep;
    //    realcard.SetActive(false);    
        
    //    if (keep)
    //    {
    //        realcard.transform.SetParent(target);
    //        target = realcard.transform;
    //    }

    //    LeanTween.move(gameObject, target, 0.4f).setEase(LeanTweenType.easeInQuad).setOnComplete(EnableRealCard);
    //}
    
    public void EnableRealCard()
    {
        if (keeprealcard)
        {
            if(realcard!=null) realcard.SetActive(true);
        }
        Destroy(gameObject);
    }
    
}
