﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Action : Card
{
    //public List<string> requirements = new List<string>();
    public int gold = 0;
    public int stress = 0;
    public float virtue = 0;
    public int count_per_week = 0;
    public int count_this_week = 0;
    public bool at_home = false;
    public List<string> stat_modifiers = default(List<string>);
    public string job_name = "";
    //action is just a card that can be done # of times per week.  Don't actually want to use rest here do we?

    public Action(string newcard_name, string newtype, string newdescription = "", string newlocation = "", List<string> newrequirements = default(List<string>),
		string newaction = "", float newtime = 0, string newsubtype= "", int newgold = 0, int newstress = 0, float newvirtue = 0, int newcount_per_week = 0, int newcount_this_week = 0,
        bool newat_home = false, List<string> newstat_modifiers = default(List<string>), string newjob_name="")
		: base(newcard_name, newtype, newdescription, newlocation, newrequirements, newaction, newtime,newsubtype)
    {
        //base constructor is called first?
        type = "Action";
        stress = newstress;
        gold = newgold;
        time = newtime;
		subtype = newsubtype;

		virtue = newvirtue;
        count_per_week = newcount_per_week;
        count_this_week = newcount_this_week;
        at_home = newat_home;
        stat_modifiers = newstat_modifiers;
        job_name = newjob_name;

		if (sprite == default(Sprite) && job_name != "") {
			sprite = DataManager.get_sprite_from_name (job_name);
		}
        //where you get Action vs where you do Action?
        //this could also apply to home stuff...?

    }


    public override void initialize(string name)
	{
		base.initialize (name);


	}
    public Action Clone()
    {
        return (Action)this.MemberwiseClone();
    }


}


public static class Action_Dictionary
{
    public static Dictionary<string, Action> Actions = new Dictionary<string, Action> 
	{ 


		{"Enroll", new Action("Enroll","Action","","School",new List<string>(){},"Acquire|Class",0,"",0,0,0,0,0,false,new List<string>(){},"")},
		{"Find a job", new Action("Find a job","Action","","Notice Board",new List<string>(){},"Acquire|Job",0,"",0,0,0,0,0,false,new List<string>(){},"")},
		{"Rent a house", new Action("Rent a house","Action","","Notice Board",new List<string>(){},"Acquire|Home",0,"",0,0,0,0,0,false,new List<string>(){},"")},
		{"Build Log Cabin", new Action("Build Log Cabin","Action","","Forest",new List<string>(){"Item|Wood|5"},"And|GiveItem|Wood|-5&MakeHome|Log Cabin",3,"",0,0,0,1,0,false,new List<string>(){},"")},
		{"Heal", new Action("Heal","Action","","Church",new List<string>(){},"",1,"",-50,0,0,0,0,false,new List<string>(){},"")},
		{"Donate", new Action("Donate","Action","","Church",new List<string>(){},"And|GiveStat|Life|1&GiveStat|Purity|1",1,"",-20,0,0,0,0,false,new List<string>(){},"")},
		{"Pay the Rent", new Action("Pay the Rent","Action","","Notice Board",new List<string>(){"Rent is due|==|true"},"PayRent",1,"",0,0,0,0,0,false,new List<string>(){},"")},



		{"Visit Library", new Action("Visit Library","Action","","School",new List<string>(){"Scribe|>=|15"},"Random|GiveStat|Reason|4/GiveStat|Wit|4/GiveStat|Cooking|4/GiveStat|Ritual|4/GiveStat|Science|4",0,"",0,0,0,1,0,false,new List<string>(){},"Scribe")},
		{"Dig Iron", new Action("Dig Iron","Action","","Hills",new List<string>(){"Miner|>=|15"},"GiveItem|Ingot|1",1,"",0,0,0,0,0,false,new List<string>(){},"Miner")},
		{"Chop Wood", new Action("Chop Wood","Action","","Home",new List<string>(){"Woodcutter|>=|15"},"GiveItem|Wood|1",1,"",0,0,0,1,0,false,new List<string>(){},"Woodcutter")},
		{"Sew Clothes", new Action("Sew Clothes","Action","","Market",new List<string>(){"Seamstress|>=|15"},"Acquire|Sew Clothes",1,"",0,0,0,1,0,false,new List<string>(){},"Seamstress")},
		{"Shop Lift", new Action("Shop Lift","Action","","Forest",new List<string>(){"Pickpocket|>=|15"},"Random|GiveStat|Gold|15/Text|You were caught!|GiveStat|Gold|-5/Text|You slip a fish into your pocket.GiveItem|Fish|1",1,"",0,0,0,1,0,false,new List<string>(){},"Pickpocket")},
		{"Hunt", new Action("Hunt","Action","","Home",new List<string>(){"Squire|>=|15"},"Random|Text|You caught a rabbit.|GiveStat|Food|1/Text|You caught a deer.|GiveStat|Food|3/Text|You caught nothing!GiveStat|Food|0",1,"",0,0,0,0,0,false,new List<string>(){},"Squire")},
		{"Bake Bread", new Action("Bake Bread","Action","","Hills",new List<string>(){"Baker|>=|15"},"GiveStat|Food|1",1,"",0,0,0,1,0,false,new List<string>(){},"Baker")},
		{"Adventure", new Action("Adventure","Action","","Home",new List<string>(){"Town Guard|>=|15"},"GiveStat|Gold|20",1,"",0,0,0,1,0,false,new List<string>(){},"Town Guard")},
		{"Ripoff", new Action("Ripoff","Action","","Home",new List<string>(){"Con Artist|>=|15"},"GiveStat|Gold|20",1,"",0,0,0,1,0,false,new List<string>(){},"Con Artist")},
		{"See Future", new Action("See Future","Action","","School",new List<string>(){"Fortune Teller|>=|15"},"GiveStat|Gold|20",1,"",0,0,0,1,0,false,new List<string>(){},"Fortune Teller")},
		{"Craft Equipment", new Action("Craft Equipment","Action","","Home",new List<string>(){"Blacksmith|>=|15"},"Acquire|Craft Equipment",1,"",0,0,0,1,0,false,new List<string>(){},"Blacksmith")},
		{"Research", new Action("Research","Action","","Home",new List<string>(){"Tutor|>=|15"},"Random|GiveStat|Reason|5/GiveStat|Ritual|5",1,"",0,0,0,1,0,false,new List<string>(){},"Tutor")},
		{"Dance Party", new Action("Dance Party","Action","","Forest",new List<string>(){"Dancer|>=|15"},"And|GiveStat|Fame|5&GiveStat|Stress|-5",1,"",0,0,0,1,0,false,new List<string>(){},"Dancer")},
		{"Brew Beer", new Action("Brew Beer","Action","","Hills",new List<string>(){"Bartender|>=|15"},"And|GiveStat|Food|1&GiveStat|Stress|-5",1,"",0,0,0,1,0,false,new List<string>(){},"Bartender")},
		{"Collect Mushrooms", new Action("Collect Mushrooms","Action","","Forest",new List<string>(){"Herbalist|>=|15"},"Random|GiveItem|Mushroom|1/GiveItem|Magic Mushroom|1",1,"",0,0,0,1,0,false,new List<string>(){},"Herbalist")},
		{"Set Ambush", new Action("Set Ambush","Action","","Hills",new List<string>(){"Bandit|>=|15"},"Random|GiveStat|Gold|60/GiveStat|Gold|20/GiveStat|Gold|30/Text|You were captured by the guards!|GiveStat|Stress|20",1,"",0,0,0,1,0,false,new List<string>(){},"Bandit")},
		{"Enchant Item", new Action("Enchant Item","Action","","Home",new List<string>(){"Artificer|>=|15"},"Acquire|Enchant Item",1,"",0,0,0,1,0,false,new List<string>(){},"Artificer")},
		{"Brew Potion", new Action("Brew Potion","Action","","Home",new List<string>(){"Alchemist|>=|15"},"Acquire|Brew Potion",1,"",0,0,0,1,0,false,new List<string>(){},"Alchemist")},
		{"Transform", new Action("Transform","Action","","Home",new List<string>(){"Druid|>=|15"},"Acquire|Transform",1,"",0,0,0,1,0,false,new List<string>(){},"Druid")},
		{"Reciprocate", new Action("Reciprocate","Action","","Home",new List<string>(){"Empath|>=|15"},"GiveStat|Wit|6",1,"",0,0,0,1,0,false,new List<string>(){},"Empath")},
		{"Burgle", new Action("Burgle","Action","","Home",new List<string>(){"Burglar|>=|15"},"GiveStat|Gold|80",1,"",0,0,0,1,0,false,new List<string>(){},"Burglar")},
		{"Bake Cake", new Action("Bake Cake","Action","","Home",new List<string>(){"Chef|>=|15"},"Acquire|Bake Cake",1,"",0,0,0,1,0,false,new List<string>(){},"Chef")},
		{"Seduce", new Action("Seduce","Action","","Home",new List<string>(){"Burlesque|>=|15"},"GiveStat|Gold|50",1,"",0,0,0,1,0,false,new List<string>(){},"Burlesque")},
		{"Sacrifice", new Action("Sacrifice","Action","","Home",new List<string>(){"Cultist|>=|15"},"Unacquire|Pet&GiveStat|Strength|8",1,"",0,0,0,1,0,false,new List<string>(){},"Cultist")},
		{"Healing", new Action("Healing","Action","","Home",new List<string>(){"Acolyte|>=|15"},"",1,"",0,0,0,1,0,false,new List<string>(){},"Acolyte")},
		{"Style", new Action("Style","Action","","Home",new List<string>(){"Courtesan|>=|15"},"GiveItem|Beautiful Hair",0,"",0,0,0,1,0,false,new List<string>(){},"Courtesan")},



	
	};
}
