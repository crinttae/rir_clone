﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;


public class Card 
{
    public string card_name;
    //public string header;
    public string description;
    public string type;
	public string subtype;

	public string location;
    public float time;
	public Sprite sprite=default(Sprite);
    public List<string> requirements = new List<string>();
	public int sorting_value = 0;

    public string action;

	public Card(string newcard_name,  string newtype, string newdescription, string newlocation, List<string> newrequirements , string newaction = "", float newtime =0, string newsubtype="", int new_sorting_value = 0)
    {
		//could do a list of player descriptions...
        card_name = newcard_name;
       // header = newheader;
        type = newtype;
		subtype = newsubtype;
        description = newdescription;
        location = newlocation;
        requirements = newrequirements;
        action = newaction;
        time = newtime;
		if (subtype == "")
			subtype = type;
		
		sprite = DataManager.get_sprite_from_name (card_name);
		sorting_value = new_sorting_value;
    }
  
    
	//one way to speed this up would be to initialize this in the dicitonary?
	//all this stuff shoudl be cloned though right?

	public virtual void initialize(string name)
    {
        card_name = name;

		sprite = DataManager.get_sprite_from_name (name);



		//		
//
//        if (Card_Dictionary.Cards.ContainsKey(card_name))
//        {
//            description = Card_Dictionary.Cards[card_name].description;
//            type = Card_Dictionary.Cards[card_name].type;
//            requirements= Card_Dictionary.Cards[card_name].requirements;
//            time = Card_Dictionary.Cards[card_name].time;
//        }



    }



	//this is all pretty AWKWARD.  should probably have another more general approach without having to pass views along...
	//there typically needs to be a description but that doesn't have to be fixed to a view...  maybe it should just be part of the image manager...
	//is there ever a case wehre I want one view to show one thing and the other to show the other????????  maybe but even then the ownership probably 
	//makes it more not less confusing...

	//Inventory
	//Player
	//Gamewide

	//what if everyone loses their job for exmaple.. that's not really part of a card is it???  I guess it would still have a picture...

	//shoudl probably have an option here in do action that displays the card...

	public void DoAction(View_Inventory view =null , bool display_card=false ,bool display_message=false, string optional_message ="")
	{
		string display_card_name = "";

		if (display_card) {
		//if (type = "Job")


			display_card_name = this.card_name;
		}
		view.pvar.DoActionString (action,display_message,optional_message,view, display_card_name);
	}
}



//so this is technically working now but it shouldn't be...
public static class Card_Dictionary
{
    public static Dictionary<string, Card> Cards = new Dictionary<string, Card>
    {
        //{"Assassin", new Card("Assassin","Job","Kill for money","Forest",new List<string>(), 10f)},
        //{"Woodcutter", new Card("Woodcutter","Job","Kill for money","Forest",new List<string>(),10f)},
        //{"Alchemist", new Card("Alchemist","Job","Kill for money","Forest",new List<string>(),10f)},

    };
}



