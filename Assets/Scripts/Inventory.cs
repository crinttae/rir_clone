﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Inventory : MonoBehaviour {
    public bool player_inventory =false;
    public bool location_inventory = false;
    public bool adventure_inventory = false;
    public bool home_inventory = false;

  //  private string delimiter = "|";
    public View_Inventory view;

    //do all these inventories have a place?
    //if i go into a shop does it automatically load my inventory?

    //if so does it show up at the bottom of the screen?



    //that's all assuming you go to cut wood.. you don't really ever need to...
    //or that you stock up on potions...




    //of course I could just WRITE code for stackable items rather than deal specifically with tuples...
    //or just not EVEN incorporate any stacks... because who relaly needs to make a house using 50 wood or whatever....
    //I suppose it will be important to track education-- how long you've been in a class, assumign you want some kind of educational promotion...
    //but i certainly don't need to define that here do I???


    //   public List<Tuple<Card, int>> cards= new List<Tuple<Card, int>>();

    [SerializeField]  public List<Card> cards = new List<Card>();


     
        //there's probably a proper way to check if each player has newly arrived at conditions os they can see more cards..
        //what if htere's a special hidden magicians sword at hte magic shcool?    do I also ahve to check conditions each time??  what about just every time you load an inventory..
        //or even every time you opne a player inventory?
   



    //well that's simple enough.. but wat about counts?


    //public TupleList<Card, int> cards = new TupleList<Card, int>();
    //public class TupleList<T1, T2> : List<Tuple<T1, T2>>
    //public void Add(T1 item, T2 item2)
    //{
    //    Add(new Tuple<T1, T2>(item, item2));
    //}




    //don't honestly need the whole card in this palce do we?  I suppose if we want to limit the dictionary search?
    //but could theoretically just store the strings... or enums or whatever...



    //[SerializeField]   public List<Item> items = new List<Item>();
    //[SerializeField]   public List<Job> jobs = new List<Job>();
    /// <summary>
    /// Just holds a list of cards?
    /// cards which can have subclasses and shit... right?
    /// 
    /// and it needs some way of being opened and looked at...
    /// has to be attached to a player's (equpitment, inventory, home) OR a location
    /// could include a quest too...
    /// 
    /// Also clicking on the item is contextual, duh....
    /// Ideally the card itelf needs an ONCLICK demand.
    /// And clicking from the inventory sends a message to the card...
    /// The card also has to care about WHENCE it got clicked upon?
    /// 
    /// Home inventory is hidden unless you're there.. Job card is hidden unless you're there...
    /// </summary>

    //should objects be pulled or should they be pushed???
    //serializable list...
    //the card dictionary should have all the requirements for every card???
    //should at least have an indicator for where each card belongs???  or do i just go through all 3 or 4 dictionaries??

    //List<Action> jobs = new List<Action>();
    //Action: something you can use but can't pick up?..

    void Awake() {
        if (this.tag == "Location") location_inventory = true;
        Transform Homes = GameObject.Find("Homes").transform.parent;
        if (Homes == null) print("Ohno Can't find homes!");
        if (transform.parent == Homes) home_inventory = true;
    }


	//sort?
	//this could happen between turns or whatever no hurry...

	//should there be a seperate view for equipment?  for buffs/debuffs?



}
