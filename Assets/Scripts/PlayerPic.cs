﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class PlayerPic : MonoBehaviour, IPointerClickHandler
{
    CardImageManager card_image_manager;
	public Encounter encounter;

	//alternatively there's an encounter and need to pull that..

    private void Start()
    {
        card_image_manager = GetComponentInParent<CardImageManager>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
    
		card_image_manager.switch_stats_screen();
    }
		
	
}
