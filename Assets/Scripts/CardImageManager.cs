﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//maybe should check to see if any locations are valid at all?
using System.Linq;


public class CardImageManager : MonoBehaviour {

	#region declarations
	public Transform card_slot_transform;
    public GameObject card_prefab;
    public RectTransform hand_area;
    static public Transform example_card_transform;
    public Transform card_canvas;
    public Transform card_images;

    public Transform Location_Home;

    
    bool showing_character_screen = false;
	bool showing_map_screen = false;
	bool showing_encounter_screen = false;


    public Transform map;
    public Transform character_sheet;

    public Transform flying_cards;
	public Transform nonflying_cards;
    public Transform player_image;
    public UIText UIText;
	public PlayerVariables pvar;

	string coroutine_string;

	public View_Inventory TopView;
	public View_Inventory BottomView;

	public CardUIImage[] card_ui_images;


	public int card_ui_image_count;
	public CardUIImage current_card_ui_image;
	public bool disappears_on_own1 = true;
	public GameObject invisbutton;

	public List<GameObject> screens;



	//one option is to have a button behind the card that overrides all interaction.. and clicking on it closes the card..
	//an advantage there is that you can leave things like the menu untouched...

    void Awake()
    {
      
		pvar = GetComponent<PlayerVariables> ();
		show_screen (ScreenName.MapScreen);


		for (var i = 0; i < card_ui_images.Length; i++) {
			card_ui_images [i].DescriptionImageNumber = i;
		}

        if (example_card_transform == null) example_card_transform = transform.GetChild(0);



    }
	public bool is_busy(){
		if (!disappears_on_own1) return true;

		return false;
	}
	#endregion





	#region Description_Code
	//this is all different ways of having cards pop up over the map, get large, and then disappear.
	//right now there's a timer that checks how long the mouse has been hovering before it pops up.
	//alternatively it can be popped up through an event, in which case it won't go away.




	public int Active_description_card_count()
	{
		int cnt = 0;
		for (var i=0;i<card_ui_images.Length;i++)
		{
			if (card_ui_images [i].gameObject.activeSelf)
				cnt++;
		}
		return cnt;
	}

	public int Active_description_card_choice()
	{
		int cnt = 0;
		for (var i=0;i<card_ui_images.Length;i++)
		{
			if (card_ui_images [i].gameObject.activeSelf)
				if (!card_ui_images [i].disappear_on_own)
					cnt++;
		}
		return cnt;
	}



	void Manage_Description_Coroutine(int i= 0, bool cancel=false, bool start=true)
	{
		if (cancel)
			coroutine_string = "Description_to_Cancel_Coroutine" + i;
		else
			coroutine_string = "Description_Coroutine" + i;

		if (start)
			StartCoroutine (coroutine_string);
		else
			StopCoroutine (coroutine_string);
	}


	IEnumerator Description_Coroutine0()
	{
		yield return new WaitForSeconds(2f);
		card_ui_images[0].gameObject.SetActive(true);
		LeanTween.scale(card_ui_images[0].gameObject, new Vector3(1.0f, 1.0f, 1.0f), .3f);
	}
	IEnumerator Description_to_Cancel_Coroutine0()
	{
		if (disappears_on_own1) {
			yield return new WaitForSeconds (.7f);

			Logger.log ("killing description1");
			LeanTween.scale (card_ui_images[0].gameObject, new Vector3 (.7f, .7f, .7f), .3f).setOnComplete (DisableImage0);
		}
	}
	IEnumerator Description_Coroutine1()

	{
		yield return new WaitForSeconds(1f);
		card_ui_images[1].gameObject.SetActive(true);
		LeanTween.scale(card_ui_images[1].gameObject, new Vector3(1.0f, 1.0f, 1.0f), .3f);
	}	
	IEnumerator Description_to_Cancel_Coroutine1()
	{
		yield return new WaitForSeconds(.7f);
		Logger.log ("killing description2");
		LeanTween.scale(card_ui_images[1].gameObject, new Vector3(.7f, .7f, .7f), .3f).setOnComplete(DisableImage1);
	}
	IEnumerator Description_Coroutine2()
	{
		yield return new WaitForSeconds(1f);
		card_ui_images[2].gameObject.SetActive(true);
		LeanTween.scale(card_ui_images[2].gameObject, new Vector3(1.0f, 1.0f, 1.0f), .3f);
	}
	IEnumerator Description_to_Cancel_Coroutine2()
	{
		yield return new WaitForSeconds(.7f);
		LeanTween.scale(card_ui_images[2].gameObject, new Vector3(.7f, .7f, .7f), .3f).setOnComplete(DisableImage2);
	}
	IEnumerator Description_Coroutine3()
	{
		yield return new WaitForSeconds(1f);
		card_ui_images[3].gameObject.SetActive(true);
		LeanTween.scale(card_ui_images[3].gameObject, new Vector3(1.0f, 1.0f, 1.0f), .3f);
	}
	IEnumerator Description_to_Cancel_Coroutine3()
	{
		
		yield return new WaitForSeconds(.7f);
		LeanTween.scale(card_ui_images[3].gameObject, new Vector3(.7f, .7f, .7f), .3f).setOnComplete(DisableImage3);
	}

	//if time deactivate... then... do this... 


	void DisableImage0()
	{
		card_ui_images[0].gameObject.SetActive(false);
	}

	void DisableImage1()
	{
		card_ui_images[1].gameObject.SetActive(false);
	}
	void DisableImage2()

	{
		card_ui_images[2].gameObject.SetActive(false);
	}	
	void DisableImage3()
	{
		card_ui_images[3].gameObject.SetActive(false);
	}

	void pick_current_ui_image(int i = 0)
	{
		current_card_ui_image = card_ui_images [0];
	}

	public void Keep_all_previous_cards(int current_card_number)
	{
		for (var i = 0; i <= current_card_number; i++) {
			Cancel_Cancelling_Description (i);
		}
	}
	public void Cancel_Cancelling_Description(int card_number_to_not_cancel)
	{
		//LETS CALL THE CALLING OFF OFF!
		//we shoudl have a specific ui image?
		Manage_Description_Coroutine (card_number_to_not_cancel, true, false);
		//StopCoroutine("Description_to_Cancel_Coroutine");
	}

	public void Cancel_Description_Maybe(int current_card_number)
	{
		pick_current_ui_image (current_card_number);
		current_card_ui_image.Stop_description_coroutine ();
		//also what's this?

		Manage_Description_Coroutine (current_card_number, false, false);
		//stop the description coroutine

		if(current_card_ui_image.gameObject.activeSelf)
			Manage_Description_Coroutine (current_card_number, true, true);
		//start the cancel coroutine
		//should probably only do this if th card actually exists??
	}
	public void Cancel_Description_Maybe()
	{
		//properly speaking	 this is a dumb way to do it and we should know how many there are....
		for (var i=0;i<=3;i++)
		{
			Cancel_Description_Maybe (i);
		}
	}

	public void View_Description(Sprite sprite, string header, string description, int current_card_number =0)
	{
		//decide which ui image we want to "view"?  so set card_ui_image...
		//better woudl be a way of hovering over text thought.
		pick_current_ui_image(current_card_number);

		if(current_card_ui_image.gameObject.activeSelf)
			current_card_ui_image.Set_delayed_description(this ,sprite, header ,description);
		else 
			current_card_ui_image.initializeBig(this ,sprite, header ,description);



		//for some reason it's still killing extra descriptions...
		if (current_card_ui_image.gameObject.activeInHierarchy == false)  		
		Manage_Description_Coroutine (current_card_number,false,true);
		
		//start the description coroutine
		Manage_Description_Coroutine (current_card_number,true,false);
		//stop the cancel coroutine
	}

	public void View_Description(Card card, string header= "", string description="",int current_card_number =0)
	{
		if (header == "")
			header = card.card_name;
		if (description == "")
			description = get_standard_description ( card);
		if (current_card_number > 1 && card.type == "Item") {
			Item item = card as Item;
			description += get_craft_description (item);
		}
		View_Description(card.sprite, header , description,current_card_number);
	}

	public void View_Description(string card_name,int description_card_number=1)
	{
		Card card = DataManager.get_dictionary_card (card_name);
		View_Description (card, "", "", description_card_number);
	}




	//probably should just always include the standard description, and occasionally alter it
	public void Get_Immediate_Descriptions(List<string> card_names, List<string> descriptions = default (List<string>) )
	{
		
		for (var i = 0; i < card_names.Count; i++) {
			string description = "";
			string header = "";
			if (descriptions != default (List<string>)) {
				description = descriptions [i];
			}
			Get_Immediate_Description (card_names [i], header, description, i);
		}

	}

	public void Get_Immediate_Description(string card_name,string header= "", string description="", int description_card_number=1)
	{
		Card card = DataManager.get_dictionary_card (card_name);
		Get_Immediate_Description (card, header, description, description_card_number);
	}






	public void Get_Immediate_Description(Card card, string header= "", string description="",int current_card_number =0)
	{
		if (header == "")
			header = card.card_name;


		if (description == "")
			description = get_standard_description (card);

		//problem: standard description doesn't include stat requirements...
		invisbutton.SetActive(true);
		//need some rule abotu whether or not to the button does anything.
		//could make the invis button call fro mhre...

		pick_current_ui_image (current_card_number);

		current_card_ui_image.initializeBig(this ,card.sprite, header ,description,false);
		current_card_ui_image.gameObject.SetActive(true);

		LeanTween.scale(current_card_ui_image.gameObject, new Vector3(1.0f, 1.0f, 1.0f), .3f);

		disappears_on_own1 = false;
	}


	public void description_button_click(int current_card_number)
	{
		//chooses an action or something..
		//makes that underlying card do something...
		print ("player clicked on card number " + current_card_number);


//		if(Active_description_card_count>1)
			pvar.DoActionString (pvar.stored_actions [current_card_number]);
//		else pvar.DoActionString (pvar.stored_actions [current_card_number]);

		ImmediatelyCancelDescription ();

		pvar.stored_actions.Clear();



		pvar.check_time();  //maybe want tod o this at the end fo the transfer... so maybe have a check on that too...
	}


	public void invisbuttonclick()
	{
		print ("InvisClick");

		if (Active_description_card_choice () > 1)
			return;

		if (Active_description_card_choice () == 1)
			description_button_click (0);

		disappears_on_own1 = true;
		invisbutton.SetActive (false);
		ImmediatelyCancelDescription ();
		//gameObject.SetActive(false);

	}


	//then smoehow need an on click that makes it actually disappear...
	//though rihgt now technically just going somewhere will do that.



	public void ImmediatelyCancelDescription(int current_card_number=0)
	{
//		if (current_card_number!=0)
//		{
//			pick_current_ui_image(current_card_number);
//			return;
//		}
		Logger.log ("killing all dscriptions immdiately");
		invisbutton.SetActive (false);
		LeanTween.scale(card_ui_images[0].gameObject, new Vector3(.7f, .7f, .7f), .3f).setOnComplete(DisableImage0);
		LeanTween.scale(card_ui_images[1].gameObject, new Vector3(.7f, .7f, .7f), .3f).setOnComplete(DisableImage1);
		LeanTween.scale(card_ui_images[2].gameObject, new Vector3(.7f, .7f, .7f), .3f).setOnComplete(DisableImage2);
		LeanTween.scale(card_ui_images[3].gameObject, new Vector3(.7f, .7f, .7f), .3f).setOnComplete(DisableImage3);
	}


	public void Create_semi_permanent_description()
	{
	//so we pull a card, at random wtihout basing it on another card.....
	//and more importantly there's no way to hover over other cards to make it go away...
	
	
	}




	public string get_standard_description (Card card){
		string type = card.type;
		string standard_description = type+ '\n';


		if (card.type == "Home") 
		{
			Job job = card as Job;
			standard_description += "First Month Rent: " + Mathf.RoundToInt(-1 * DataManager.home_move_in_factor * job.gold) + '\n';
			standard_description += "Monthly Rent: " + (-1 * job.gold) + '\n';
			standard_description += "Reduces Stress: " + job.stress + '\n';

		}

		if (card.type == "Job") {
			
			Job job = card as Job;
			standard_description += "Location: " + job.location + '\n';
			standard_description += "Wage: " + job.gold + "gp/hr" + '\n' +'\n';

			if (job.action != "") {
				standard_description += "Mastery: " + '\n' + get_standard_action_description (job.action) + '\n';

			}
		} else if (card.type == "Item" || card.type=="Status") {
			Item item = card as Item;

			//standard_description += item.item_type + '\n';
			if (pvar.is_item_equipped (item)) 
			{
				standard_description += "Is equipped to: " + item.equip_to + "\n\n";
			}
			else if (item.equip_to != "")
				standard_description += "Can be equipped to: " + item.equip_to + "\n\n";


			foreach (string modifier in item.stat_modifiers) {
				standard_description += modifier + '\n';
			}
			if (item.gradual)
				standard_description += "every week" + '\n';

			//	if (item.sellable)
			//		{
			//			standard_description += "Sells for " + item.sellprice + '\n';
			//		}
		} else if (card.type == "Action") {
			
			Action action = card as Action;
			if (action.count_per_week != 0) {
				standard_description += "Once per week:\n";
			}

			standard_description += get_standard_action_description (action.action);
		
		}

		return standard_description;
	}


	//should we actually get these standard descriptions and stuff before wel look at the card??? seems possibly excessive...
	public string get_standard_action_description(string action_string)
	{
		string partial_description = "";
		string[] parts = action_string.Split ('|');


		if (parts [0] == "And") 
		{
			string[] multiactions = action_string.Substring (4).Split ('&');

			foreach (string action_part in multiactions) {
				partial_description += get_standard_action_description (action_part) ;
			}
		}
		if (parts [0] == "Random") {
			string[] multiactions = action_string.Substring (7).Split ('/');
			partial_description += "Random:" + '\n';
			foreach (string action_part in multiactions) {
				partial_description += get_standard_action_description (action_part);
			}
		} 
		else if (parts [0] == "Acquire") {
			if (parts [1] == "Job")
				partial_description +=  "Look for a new job.";
			else if (parts [1] == "Home")
				partial_description +=  "See if there is a room available.";
			else foreach (KeyValuePair<string , Item> entry in Item_Dictionary.Items)
				{
					if (entry.Value.subtype == parts [1]) 
					{
						if (entry.Value.crafting_requirements != default(List<string>)) 
							{
								partial_description +=  "Craft ";
							}
					partial_description += create_link (entry.Key) +'\n';
					}
				//somehow we should include crafting ingredients if the card is secondary????  not sure how to do that though...
				}

		} 
		else if (parts [0] == "GiveStat") {
			float amount = 0;
			if (float.TryParse (parts [2], out amount)) {
				if (amount > 0)
					partial_description += " Gain " + amount + " " + parts [1] +'\n'; 
				else if (amount < 0)
					partial_description += " Lose " + (-amount) + " " + parts [1]+'\n';
			}
		} 
		else if (parts [0] == "PayRent") {
			partial_description += "Pay your rent";
		}

		else if (parts [0] == "GiveItem") {
			if (parts.Length == 3) {
				float amount = 0;
				if (float.TryParse (parts [2], out amount)) {
					if (amount > 0)
						partial_description += "Gain " + amount +" ";
					else if (amount < 0)
						partial_description += "Lose " + (-amount)+" ";
				}
			}
			else partial_description+="Gain ";

			partial_description += create_link(parts[1]) +'\n';
		}


		else if (parts [0] == "Unlock") {
			partial_description += create_link(parts [1]) + '\n';
		}

		else if (parts [0] == "MakeHome") {
			partial_description += "New Home: " + create_link(parts [1]) + '\n';
		}

		return partial_description;
	}


	string get_craft_description(Item item)
	{	
		//this is not catching for some reason.
		if (item.crafting_requirements.Count<1) return "";
//		if (item.crafting_requirements == new List<string>())
//			return "";
		
		string description_part = "";
		description_part+='\n'  +"Crafting requirements:"+'\n';
		//eg. item|plate armor|1, item|thread|5 
		foreach (string requirement in item.crafting_requirements) {
			string[] parts = requirement.Split ('|');
			int stacks = 1;
			if (parts.Length == 3)
				int.TryParse (parts [2], out stacks);

			description_part += " " + stacks.ToString () + " ";

			description_part += create_link (parts[1])+'\n';
		}
		return description_part;
	}










	public string create_link(string word){
		return	"<link=\"" + word + "\"><u><#80ff80>" + word + "</color></u></link>";
	}

	#endregion 



	#region make cards shift around

    //lets pretend for now we're sending based on inventories.. but.. are we.. we should be sending based on a card?  or on the image of a card?
    //what if a card come from a transform though?
    //could search for the card.. then convert the 
    //can also "create the card.. hide it.. put it in the to inventory.. is it possiblt the card is nowhere??? 
    public void transfer(GameObject Card_in_View=null, bool keep_original = true, Inventory Inventory_to = null, Inventory Inventory_from = null, Vector3 move_to=default(Vector3), Vector3 move_from = default(Vector3), Card card=null)
    {
 //lets say we sell or buy a card from the middle...
 //a lot of things have to happen.. we have card in view... and an inventory to...
 //all of the cards in teh inventory from need to become unhooked.
		ImmediatelyCancelDescription ();

        View_Inventory view_from = null;
        View_Inventory view_to = null;

        //lets say we are adding a card to the inventory.. 

		if (TopView.isActiveAndEnabled) { 
			if (Inventory_from != null && TopView.current_inventory == Inventory_from)
				view_from = TopView;
			if (Inventory_to != null && TopView.current_inventory == Inventory_to)
				view_to = TopView;
		}

		if (BottomView.isActiveAndEnabled) { 
			if (Inventory_from != null && BottomView.current_inventory == Inventory_from)
				view_from = BottomView;
			if (Inventory_to != null && BottomView.current_inventory == Inventory_to)
				view_to = BottomView;
		}


        //all of this should only apply if we have to actually create the card somehow anyway...
		if (Card_in_View == null) 
		{
			if (move_from == default(Vector3)) 
			{ 
				if (Inventory_from != null) 
				{
					if (view_from == null && Inventory_from.player_inventory == true)
						move_from = player_image.position;
					else if (view_from == null && Inventory_from.location_inventory == true)
						move_from = Inventory_from.transform.position;
					else
						move_from = nonflying_cards.transform.position;			
				}
				else move_from = nonflying_cards.transform.position;
			}

			Card_View card_view = new Card_View (card);
			Card_in_View = create_card (card_view, move_from ,nonflying_cards);
			//we create a card here just to disappear it a second later.
		}

        List<GameObject> moving_cards= new List<GameObject>();
        List<CardTransfer> dupes = new List<CardTransfer>();

        //somehow viewfrom i not working...  view from should be getting et based on beinig the avialalbe inventory right?
        //if (view_from != null)
        //{
        //    for (int i = 0; i < view_from.transform.childCount; i++)
        //    {
        //        moving_cards.Add(view_from.transform.GetChild(i).gameObject);
        //    }
        //}
        //else moving_cards.Add(Card_in_View);


        moving_cards.Add(Card_in_View);


        //okay so now we have a list of cards to create dupes-- either everything in tranform from, or just the 1 card...
        //before this point is where we'd create a card... if we were drawing from thin air. 



        //foreach (GameObject moving_card in moving_cards)
        for (var i=0; i < moving_cards.Count;  i++)
        {
            GameObject dupe = create_temporary_image(moving_cards[i]);
            dupes.Add(dupe.GetComponent<CardTransfer>());
            dupes[i].realcard = moving_cards[i];
            moving_cards[i].SetActive(false);

        }

        if (keep_original == false) Destroy(Card_in_View);
        
        

        if (view_to != null)
        {
            //Card_in_View.transform.SetParent(view_to.transform);
            //merely setting this isn't going to work anyway!  UHG.

            //Transform_to = Card_in_View.transform;
            move_to = GetScreenCoordinates(view_to.transform.parent.GetComponent<RectTransform>()).center;
                //gameObject.GetComponent<RectTransform>().rect.center;
            //could alwyas keep a single invisible dummy card around.. of course. we'd have to decide where it's kept...
        }


        if (move_to == default(Vector3))  //this might also be a specific place in the view... perhaps...
        {
            if (view_to == null && Inventory_to.player_inventory == true) move_to = player_image.position;  //character image
            if (view_to == null && Inventory_to.location_inventory == true) move_to = Inventory_to.transform.position;  //place on map?
        }
        

        //else if (view_from != null)
        //{
        //    Card_in_View.transform.SetParent(transform);
        //    Card_in_View.transform.SetAsLastSibling();
        //}           so we're putting the card in the scene if it has somehwere to come from.. not really neceary though.. we could just memorize where it's supposed to go...


        //se print("view from and view to are both null.. do we need to make a card slide between places?");

        //if (Transform_to != null) Card_in_View.transform.position=Transform_to.position;
        //not actually moving the card at all then?

        for (var i = 0; i < dupes.Count; i++)
        {
        	dupes[i].keeprealcard = keep_original;
            //LeanTween.move(dupes[i].gameObject, moving_cards[i].transform.position, 0.7f).setEase(LeanTweenType.easeInQuad).setOnComplete(dupes[i].EnableRealCard);
            LeanTween.move(dupes[i].gameObject, move_to, 0.4f).setEase(LeanTweenType.easeInQuad).setOnComplete(dupes[i].EnableRealCard);
        }

        //then I need to have the card there... so i need to add the card to the inventory.. and to the inventory view!

    refresh_all_views_asap();

    }

    GameObject create_temporary_image(GameObject card_to_dupe)
    {
        GameObject temporary_image = Instantiate(card_to_dupe, card_to_dupe.transform.parent);
        //temporary_image.transform.localScale = transform.localScale;
        temporary_image.GetComponent < RectTransform >().sizeDelta= card_to_dupe.GetComponent<RectTransform>().sizeDelta;

        temporary_image.transform.SetParent(flying_cards,true);
        //card_to_dupe.
        temporary_image.transform.SetAsLastSibling();
        //= transform .local scale?
        //var group = temporary_image.AddComponent<CanvasGroup>();
        //this is quetionable?
        //group.blocksRaycasts = false;

        //temporary_image.GetComponent<CardUIImage>().initialize(pic.sprite, header.text);
        //temporary_image.GetComponent<CardUIScript>().enabled = false;
        //this is slow...
        temporary_image.transform.position = card_to_dupe.transform.position;

        return temporary_image;
    }



	void refresh_all_views_asap()
	{
		StartCoroutine("refresh_all_views_if_clean");
	}

	IEnumerator refresh_all_views_if_clean()
	{
		while (flying_cards.childCount > 0) yield return new WaitForSeconds(.05f);
		TopView.refresh ();
		BottomView.refresh ();
		if (nonflying_cards.childCount > 0) 
		{
			for (var i = nonflying_cards.childCount - 1 ;i>0;i--)
			{
				Destroy (nonflying_cards.GetChild (i).gameObject);
			}
		}
		yield return null;
	}


    bool check_for_loose_ends()
    {
        if (flying_cards.transform.childCount > 0) return true;


        //eg check for cards that are flying around.
        return false;
    }


	#endregion



	#region draw hand

	public void draw_cards_from_to(List<Card_View> card_views, Vector2 from, RectTransform to = null)
    {
        if (to == null) to = hand_area;
        destroy_spread(to);

        List<GameObject> card_GOs = new List<GameObject>();

		//what I really want is a new array of ints that I can count as my sort?


		List<int> sorting_values = new List<int> ();


		//List<long> docIds = new List<long>() { 6, 1, 4, 7, 2 };
		//List<T> docs = GetDocsFromDb(...)
		//docs = docs.OrderBy(d => docsIds.IndexOf(d.Id)).ToList();
		//card_GOs = card_GOs.OrderBy(d => cards.IndexOf(d.card_name)).ToList();


		for (var i=0;i<card_views.Count;i++)
        {
			card_GOs.Add(create_card(card_views[i], from, to));

		}
		//card_GOs.Sort((x,y)=>x.

		//objListOrder.Sort((x, y) => x.OrderDate.CompareTo(y.OrderDate));



        draw_cards_to(card_GOs, to);
    }
    public void draw_cards_to(List<GameObject> cards_dirty, RectTransform hand)
    {
        
        if (hand == null) hand = hand_area;


        List<GameObject> cards = new List<GameObject>();
        foreach (var card in cards_dirty) {
            if (card != null) cards.Add(card);
            card.transform.SetParent(hand);
        }


    }

	public GameObject create_card(Card_View card_view, Vector2 place = default(Vector2), Transform parent=default(Transform))
    {
		//place means position...
        if (place == default(Vector2)) place = new Vector2(500, 200);

		string card_name = card_view.card.card_name;
			
		
        //this stuff should already be done... duh.  should just initiate it then drop it if it's a fail...
        //if (!Card_Dictionary.Cards.ContainsKey(card_name) && !Item_Dictionary.Items.ContainsKey(card_name)
        //    && !Job_Dictionary.Jobs.ContainsKey(card_name))
        //{
        //    print(card_name + " not in dictionary"); return null; }


        //card_prefab shouldn't have the actual object on it.  Should just be a picture iwth a link to the object???...
        //furniture need to be installed in furniture slots????
        //ie. bed = fancy_bed.
        //endtable= fancy_endtable.
        //alchemy_table=true?
        //create garden... which needs its own inventory maybe?
        //make things and sell them in town... would be a pretty interesting game to itself.... maybe not what we're going for here...
        //but craftsman the game...

		GameObject new_card = Instantiate(card_prefab, this.transform);

        new_card.transform.position = place;
		new_card.transform.SetParent(parent,true);
		new_card.GetComponent<CardUIScript>().initialize(card_view.card, this, card_view.disabled, card_view.description);
	
		return new_card;
    }
		
    public void destroy_spread(Transform hand = null)
    {
        if (hand == null) hand = hand_area;
        for (var i = 0; i < hand.childCount; i++)
        {
            Destroy(hand.GetChild(i).gameObject);
        }
    }

    List<Vector2> create_viewport_spread(int card_count, RectTransform hand=null, float min_visibility = 1.1f, RectTransform empty_card = null, int max_count = 5)
    {
        //if (empty_card == null) empty_card = card_slot.GetComponent<RectTransform>();
        if (hand == null) hand = hand_area;
        List<Vector2> positions = new List<Vector2>();
        //List<Transform> temp_transforms = new List<Transform>();


        

        //for (var i = 0; i < hand.childCount; i++)
        //{
        //    Destroy(hand.GetChild(i).gameObject);
        //}


        //       positions.Add(Instantiate(empty_card, hand).position);
        for (var i = 0; i < card_count; i++)
        {
            //var card_slot =Instantiate(empty_card, hand);

         //   foreach (var card_slot in card_slots) card_slot.parent = hand;
        }
       //

        for (var i = 0; i < hand.childCount; i++)
        {
            positions.Add(hand.GetChild(i).position);
        }
       

        return positions;
    }
 
    List<Vector2> create_spread(int card_count, RectTransform box,  float min_visibility = 1.1f, RectTransform sample_card = null, int max_count=5)
    {
        if (sample_card == null) sample_card = card_prefab.GetComponent<RectTransform>();
        sample_card.localScale = new Vector3(1,1,1);

        float length = 0.0f;
        List<Vector2> positions = new List<Vector2>();

        if (card_count == 1)
        {
            positions.Add(new Vector2(box.position.x, box.position.y));
            return positions;
        }

        for (var i = 0; i<card_count -1 ;i++)  
        {
            length += min_visibility * sample_card.sizeDelta.x;
            if (i == max_count-1) break;
        }


        float y_value = box.position.y;

        int row = 0;
        int col = 0;

        float left = length / 2;
        float right;
        if (card_count <= max_count) right = length / (card_count - 1);
        else right = length / (max_count - 1);

        for (var i = 0; i < card_count; i++) 
        {
            
            positions.Add(new Vector2(box.position.x, y_value - row * sample_card.sizeDelta.y * 1.1f) 
                + Vector2.left * left 
                + Vector2.right * col * right
                );
            col++; if (col == 5) { row++; col = 0; }
        }

        

        return positions;
    }

	#endregion




	#region misc

	public Rect GetScreenCoordinates(RectTransform uiElement)
	{
		var worldCorners = new Vector3[4];
		uiElement.GetWorldCorners(worldCorners);
		var result = new Rect(
			worldCorners[0].x,
			worldCorners[0].y,
			worldCorners[2].x - worldCorners[0].x,
			worldCorners[2].y - worldCorners[0].y);
		return result;
	}



	public void show_screen (ScreenName screen_name)
	{
		foreach(GameObject screen in screens)
		{
			if (screen_name.ToString() == screen.name)
				screen.SetActive (true);
			else screen.SetActive (false);
		}
	}
	public string get_active_screen_name ()
	{
		foreach (GameObject screen in screens) {
			if (screen.activeSelf)
				return screen.name;
		}
		return "";
	}

	//participant
	public enum ScreenName {StatsScreen,MapScreen,EncounterScreen }


	public void switch_stats_screen()
	{
		if (check_for_loose_ends()) return;

		 
		if (get_active_screen_name() == ScreenName.StatsScreen.ToString()) {
			if (pvar.is_in_encounter ())
				show_screen (ScreenName.EncounterScreen);
			else
				show_screen (ScreenName.MapScreen);
		
		} else {
			show_screen (ScreenName.StatsScreen);		
		}
	}

//	public void hide_character_screen()
//	{
//		if (check_for_loose_ends()) return;
//		map.gameObject.SetActive(true);
//		character_sheet.gameObject.SetActive(false);
//		showing_character_screen = false;
//	}
//	public void show_character_screen()
//	{
//		if (check_for_loose_ends()) return;
//		if(showing_map_screen) map.gameObject.SetActive(false);
//		if(showing_encounter_screen) pvar.encounter.gameObject.SetActive(false);
//
//		character_sheet.gameObject.SetActive(true);
//		character_sheet.GetChild(0).GetComponent<View_Stats>().refresh_stats();
//		showing_character_screen = true;
//	}
//
//	public void show_encounter_screen()
//	{
//
//
//		//if (check_for_loose_ends()) return;
//		if (showing_character_screen) hide_character_screen();
//		if(showing_map_screen) map.gameObject.SetActive(false);
//
//
//		pvar.encounter.gameObject.SetActive(true);
//		showing_encounter_screen = true;
//	}






	#endregion



}




