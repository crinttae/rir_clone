﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


//protected static float HoverStartTime = 0.0f;
//protected static readonly float MinHoverTime = 0.75f;



public class CardUIScript : MonoBehaviour, IBeginDragHandler, IEndDragHandler,
    IPointerClickHandler, IDragHandler, IPointerEnterHandler, IPointerExitHandler
//, IDragHandler,
//shoudl try phone game... do you have to drag cards to play them??? what happens with just clicking?
{
    //each slot has to hve an item...
    //not an instance but the thing itself... is that what gets pulled really??
    //I guess I coudl try altering the item from the view and see what happens...

    public Card card;

    static bool InvalidateClick = false;
    static bool TouchHover = false;
    static float HoverStartTime = 0.0f;
    static readonly float MinHoverTime = 0.75f;

    public bool disabled;

    public Image pic;
    public Text header;

    public PlayerVariables pvar;
    //what about a dictionary between transforms and tehir inventories?? no... silly...  just having the transforms is fine...

	string description="";

    [SerializeField]  Material grayscale;
	[SerializeField]  Transform border;



    public GameObject CardUI_Drag_Prefab;
    public Camera maincamera;

    public GameObject player;

    GameObject temporary_image;

	int price;
	int sellprice;



    
    public CardImageManager card_image_manager;
    GameObject canvas;

    bool on_click_disabled = false;
    private Dictionary<int, GameObject> m_DraggingIcons = new Dictionary<int, GameObject>();
    private Dictionary<int, RectTransform> m_DraggingPlanes = new Dictionary<int, RectTransform>();
    private Dictionary<int, Vector3> m_TempPositions = new Dictionary<int, Vector3>();


    bool dragOnSurfaces = true;

    View_Inventory view;
    //I can include in the initiate a link to the inventory, as well as the card...
    //I can't include further details though, like I can't REALLY know which card it is referring to...
    //once the card is moved I'll have to look back for this card.





    //card_image_manager?? this is yet another place that could be improved in terms of perfromance...

    //now need some kind of ON CLICK type code, that pulls from the data...

    private void Start()
    {
    }

	public virtual void initialize(Card card, CardImageManager card_image_manager = null, bool disable_card=false, string description="")
    {
		this.card = card;
		this.description = description;
		this.disabled = disable_card;
		this.card_image_manager = card_image_manager;

        
        //this is probably a dumb way to do this.

        canvas = DataManager.Current_Canvas;
        
		//this is coming from a card image manager...
		//don't really need view for most of this stuff...

		pvar = card_image_manager.pvar;
		player = pvar.gameObject;

		view = transform.parent.gameObject.GetComponent<View_Inventory>();
		//this might fail-- that's theoretically okay?


        try
        {
			pic.sprite = card.sprite;
        }
//        catch
//        {
//            try
//            {
//                if (card.type == "Action")
//                {
//                    Action action = card as Action;
//					pic.sprite = card.sprite;
//                }
//            }
        catch
        {
            print("sprite not found!");
        } 
        

        header.text = card.card_name;

		if (view != null) {

			if (card.type == "Job") {


				//set_description ();
				//if (view.conditions.check_conditions (card.requirements))
					//really I need a list of numbers and strings... which need to get used for Details
					//also some of those details are going to be in different colors maybe?
					//return string
					//with tags
					//and a boolean I guess.
					//rather call get description and use that to set both the boolean and the description which are saved as scripts.
				//	disabled = true;
				
			} else if (card.type == "Item") {
				Item item;
				item = card as Item;

				if (item.stackable) {
					header.text += "   x" + item.stacks;
				}





				if (can_be_bought ()) {
					price = Mathf.RoundToInt (item.price * view.buyprice_modifier);
					header.text += "  " + price;
				} else if (can_be_sold ()) {
					sellprice = Mathf.RoundToInt(item.sellprice* view.sellprice_modifier);
					header.text += "  " + sellprice;
				}
				if (can_be_unequipped ()) {
					border.GetComponent<Image> ().color = Color.black;
				}
			} 
			else if (card.type == "Action" && !can_be_done ()) {
				disabled = true;

			}
		}


	    if (disabled) pic.material = grayscale;


	    }

	    public void OnPointerExit(PointerEventData eventData)
	    {

	        InvalidateClick = false;
	        TouchHover = false;
	        //view.Card_UI_Transform.gameObject.SetActive(false);
	        card_image_manager.Cancel_Description_Maybe();
	        //there should be some lag...here

	    }


	    public static Rect RectTransformToScreenSpace(RectTransform transform)
	    {
	        Vector2 size = Vector2.Scale(transform.rect.size, transform.lossyScale);
	        Rect rect = new Rect(transform.position.x, Screen.height - transform.position.y, size.x, size.y);
	        rect.x -= (transform.pivot.x * size.x);
	        rect.y -= ((1.0f - transform.pivot.y) * size.y);
	        return rect;
	    }

	    

	    public void OnPointerEnter(PointerEventData eventData)
	    {
	        //OnHover.Invoke(eventData, this);
	        //okay let's try this...

	        //check to see if the image is available to look at?
	        if (this.transform.GetChild(1).gameObject.activeSelf == false) return;


	        //this really shouldn't be instant.. there should be some kind of update function somewhere that initiates after a hover time...


	        //Because hovering on a touch-device necessitates touching the control we need a special case here.
	        //If we are using a touch device and we were within the hovering phase before clicking, we can conclude
	        //that this should not be considered a click.
	        if (Input.touches.Length > 0)
	        {
	            HoverStartTime = Time.unscaledTime;
	            TouchHover = true;
	        }
		card_image_manager.View_Description (pic.sprite, header.text, description );
		//get_description ()

	        //no no no.. if it's already there, then I want to run it immediately. 
	        //if it's not there then we want to wait 1.5 seconds...


        //view.View_Description(card, pic.sprite, header.text, get_description());    
        //on_enter_temporary_image.transform.GetChild(0).GetComponentInChildren<Text>().text = description;
    }

    
    //so first get the conditional descpription then add this part?

	public void set_description()
	{
//		string type = card.type;
//		description = "";
//		description = type + '\n';
//		if (type == "Job")
//		{
//			description += "\n";
//			Job job = card as Job;
//			float total_contribution = 0;
//			foreach (string requirement in job.requirements)
//			{
//				float contribution = 0;
//				string[] parts = requirement.Split(' ');
//				contribution = view.conditions.check_condition_by_parts (parts);
//				string color= "";
//				if (contribution > 0)
//					color = "<color=green>";
//				else if (contribution >= -20)
//					color = "<color=orange>";
//				else 
//					color = "<color=red>";
//				description += color + requirement + "   "+ contribution + "</color>"+ '\n';
//				total_contribution += contribution;
//			}
//			if (total_contribution < 0)
//				disabled = true;
//		}
//	
		if (card.type == "Item")
		{
			Item item = card as Item;

			if (item.subtype == "Food") description += "Food: " + "\n\n";
			if (item.equip_to != "") description += "Equipped to: " + item.equip_to + "\n\n";

			foreach (string modifier in item.stat_modifiers)
			{
				description += modifier + '\n';
			}

			if (can_be_sold())
			{
				description += "Sell for " + sellprice;
			}
		}

	}




//    string get_description()
//    {
//        string type = card.type;
//        string description = type + '\n';
//
//        if (type == "Job")
//        {
//            description += "\n\n";
//            Job job = card as Job;
//            foreach (string requirement in job.requirements)
//            {
//                description += requirement + '\n';
//
//            }
//        }
//		else if (type == "Item")
//        {
//            Item item = card as Item;
//
//            if (item.item_type == "Food") description += "Food: " + "\n\n";
//            if (item.equip_to != "") description += "Equipped to: " + item.equip_to + "\n\n";
//
//            foreach (string modifier in item.stat_modifiers)
//            {
//                description += modifier + '\n';
//            }
//
//            if (can_be_sold())
//            {
//                description += "Sell for " + sellprice;
//            }
//        }
//
//        return description;
//    }





    //problem: if the picture is invisible, then how do we refresh this???
    //do we refresh it with a new invisible picture??  would htat even work??  no... not likely...
    //do we wait for the picture to hit before refreshing??? it is techniaclly just a view...

    public void OnPointerClick(PointerEventData eventData)
    {
        if (disabled) return;
        //if we've been hovering for a while then nevermind...
        if (TouchHover && Time.unscaledTime - HoverStartTime > MinHoverTime) return;
      //  if (on_enter_temporary_image != null) GameObject.Destroy(on_enter_temporary_image);

		pvar.uitext.clear_current_actions_to_display ();

        if (on_click_disabled) return;
        //print("click!");

		if (eventData.button == PointerEventData.InputButton.Right)
		{
			
			card_image_manager.Get_Immediate_Description(card);	
			return;
		}

        card_image_manager.Cancel_Description_Maybe();
  


        
        if (card.type== "Action" && can_be_done())
        {
            Do_Action();
			view.refresh();
            return;
        }


        if (card.type == "Item")
        {
			//"Item/type/-1
			if (view.unaquire)
			{
				pvar.delete_card (card);
				if (view.action_on_complete!="") pvar.DoActionString(view.action_on_complete,view,"");
				view.refresh();
				return;
			} 


			if (view.acquiring) 
			{
				if (disabled) return;
				Item item = card as Item;

				if (item.crafting_requirements.Count > 0) 
				{
					foreach (string requirement in item.crafting_requirements) 
					{
						//"Item|Ingot|2"
						string[] parts = requirement.Split('|');
						int count = 1;
						if (parts.Length==3) count = int.Parse(parts [2]);
						count *= -1;
						pvar.give_item (parts [1], count);
					}
				}
				acquire_or_consume_item (item);

				if (view.action_on_complete!="") pvar.DoActionString(view.action_on_complete,view,"");
				view.refresh();
				return;
			}


            Transform options_transform = transform.GetChild(2);
            List<string> options = getoptions();
            print(options);

            if (options.Count == 0)
            {
                print("nothing to do with this card");
                return;
            }

            if (options.Count == 1)
            {
                print(options[0]);
                ChooseOption(options[0]);
                return;
            }

            for (int i = 0; i < options.Count; i++)
            {
                options_transform.GetChild(i).gameObject.SetActive(true);
                options_transform.GetChild(i).GetChild(0).GetComponent<Text>().text = options[i];
            }
        }


        //all of these could be generalized right?
        if (card.type == "Job" || card.type == "Class" || card.type == "Home")
        {

            Job job = card as Job;
            Job playerjob = pvar.Job;
            
            if (pvar.DoesPlayerHave(card))
            {
				
                player.GetComponent<Conditions>().do_job(job);

                view.refresh();
                return;
            }
            else
            {
				if (card.type == "Home") {
					bool paid_rent = pvar.pay_rent (job, true);
					if (paid_rent) 
					{
						view.uitext.DisplayText ("You paid the first month's rent.");
					}
					else 
					{
						view.uitext.DisplayText ("You can't afford the first month's rent.");
						return;
					}
				}
				pvar.changejob(job);
				view.card_image_manager.transfer(gameObject,false,null,view.current_inventory,view.playerimage.position);
                view.refresh();
                return;
            }
            //if equippable then maybe show what's currently equipped??? or .. use an extra column of text in description to compare.. or what?


        }

        //card.time += 1;
        //if (card.type == "Item") 
    }




    //so we need all oepn inventories.. and we need to know hwich inventory this is in...


    //card dropped on player...?
    //do we always know what that means??
    //maybe shoudl jsut have a single function that pulls a list of all possible actions rather than checking them one at a time like this...


    public void Equip(bool Unequip = false)
    {
        Item item = card as Item;
        if (Unequip) pvar.Equip(item.equip_to);
            else pvar.Equip(item.equip_to, item);

        view.refresh();
        //foreach (KeyValuePair<string, Item> equipped_item in Equipment)
    }




    public void Consume()
    {
        Item item = card as Item;
		player.GetComponent<Conditions>().modify_attributes(item.stat_modifiers);
		if(view.current_inventory.player_inventory) view.pvar.delete_card(card);
    }

	public void acquire_or_consume_item(Item item)
	{
		//view.current_inventory.cards.Add(card);  need to grab the players inventory if open and add it there.
		if (item.consumed_on_purchase)
		{
			player.GetComponent<Conditions>().modify_attributes(item.stat_modifiers);
		}
		else
		{
			pvar.give_item(item.Clone());
			view.card_image_manager.transfer(gameObject, true, player.GetComponent<Inventory>(), view.current_inventory);
		}
		pvar.refresh_modifiers ();
		view.refresh();
	}


    public void Buy()
    {
        if (!can_be_bought()) return;
        Item item = card as Item;
        pvar.give_gold(-price);
		acquire_or_consume_item (item);
        

      

        //check to see if player view is open...also check for stacks if i do stacks
    }

    public void Sell()
    {
        if (!can_be_sold()) return;


		Item item = card as Item;
		if (can_be_unequipped()) Equip(true);

		pvar.give_item (item.Clone (), -1);
		//assuming here we're just selling one, regardless of stacks.

		pvar.give_gold(sellprice);
		//no that's it trying to pop it back... 
		view.card_image_manager.transfer (gameObject, false, pvar.location.GetComponent<Inventory> (), view.current_inventory);
       // view.refresh();

        //this will not work well with the moving card interface later... sadly...
        //check to see if the location view is open... if so move the card there..?
    }
    public void Do_Action()
    {
        Action action = card as Action;

		//action is not a player card!

		action.count_this_week++;
        player.GetComponent<Conditions>().modify_attributes(action.stat_modifiers);

        if (action.stress != 0) pvar.give_stress(action.stress);
        if (action.gold!= 0) pvar.give_gold(action.gold);
        if (action.time != 0) pvar.give_time(action.time);
        if (action.virtue != 0) pvar.give_virtue(action.virtue);
        if (action.action != "")

        action.DoAction(view);
            
        
        //view.refresh();
        //should really not have multiple ways of doing this!

    }
    //most of these actions should be part of the inventory code...
    public void ChooseOption(string choice)
    {
        HideAllOptions();
        
        switch (choice)
        {
            //should probably have warnings for buying equipment?
            case "Autoconsume":
                Buy();
                break;
            case "Use":
                Consume();
                break;
            case "Buy":
                Buy();
                break;
            case "Sell":
                Sell();
                break;
            case "Equip":
                Equip();
                break;
            case "Unequip":
                Equip(true);
                break;
            default: print("Error, nothing chosen?"); break;
        }
        
    }


    public List<string> getoptions()
    {
        //probably a nice dictioanry i could use to do this...
        if (can_be_autoconsumed()) return new List<string> { "Autoconsume" };

        //this could easily be sped up by checking for item up front and looking through these...
        List<string> options = new List<string>();
        if (can_be_consumed()) options.Add("Use");
        if (can_be_equipped()) options.Add("Equip");
        if (can_be_unequipped()) options.Add("Unequip");
        if (can_be_bought()) options.Add("Buy");
        if (can_be_sold()) options.Add("Sell");
        //if (can_be_worked()) options.Add("Work");
        if (can_be_done()) options.Add("Do");
        //if (can_be_applied_for()) options.Add("Sign up");

        return options;

    }


    public void HideAllOptions()
    {
        Transform options_transform = transform.GetChild(2);
        //shoul probaly set active recursively...
        for (var i = 0; i < options_transform.childCount; i++)
        {
            options_transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    public bool can_be_autoconsumed()
    {
        if (card.type != "Item") return false;
		if (view.current_inventory.player_inventory)
			return false;
		if (!can_be_bought())
			return false;
        Item item = card as Item;
        //if (!can_be_bought() && !view.current_inventory.player_inventory) return false;

        if (item.consumed_on_purchase) return true;
        return false;
    }

    public bool can_be_consumed()
    {
        if (card.type != "Item") return false;

        Item item = card as Item;
		if (!view.current_inventory.player_inventory) return false;
        if (item.consumable) return true;
        return false;
    }
    public bool can_be_unequipped()
    {
        if (!view.current_inventory.player_inventory) return false;
        if (card.type != "Item") return false;

        Item item = card as Item;
        if (item.equip_to == "") return false;

        // if (item.equipped) return true;
        //       print(item.equip_to+ "   " + item.card_name);

        if (pvar.Equipment[item.equip_to] == item) return true;
        
        //do we really need to do this here.. well when making the view we could pass equipped along...
        return false;
    }
    public bool can_be_equipped()
    {
        if (!view.current_inventory.player_inventory) return false;
        if (card.type != "Item") return false;
        Item item = card as Item;
        if (item.equip_to == "") return false;
        if (pvar.Equipment[item.equip_to] == item) return false;
        //okay let's say you can't equip something already equipped... not that that matters.
        return true;
    }
    public bool can_be_put_in_inventory()
    {

        bool check = false;
        return check;
    }
    public bool can_be_bought()
    {
        //if we don't already own it... and it is unique... 
        if (card.type != "Item") return false;

        if (view.current_inventory.player_inventory) return false;
        Item item = card as Item;
		if (item.price  == 0) return false;

		if (price > pvar.gold) return false;
        return true;
    }
    public bool can_be_sold()
    {
        if (card.type != "Item" ) return false;
		//this might not be true.. you mgith be able to sell statuses...
        Item item = card as Item;
        if (!view.current_inventory.player_inventory) return false;
		if (item.sellprice <= 0) return false;
		//if (item.sellable == false)	return false;  
   
		foreach (string othertype in card_image_manager.BottomView.itemtypes)
        {
			if (othertype == item.subtype) return true;
        }
        return false;
    }
    public bool can_be_done()
    {
        if (card.type != "Action") return false;
        //if (!view.current_inventory.player_inventory) return false;
        Action action = card as Action;
        
		if (action.count_per_week != 0) {
			if (action.count_this_week >= action.count_per_week)
				return false;
		}
        //foreach (string card_name_played_this_turn in pvar.Cards_Played_This_Turn)
        //{
        //    if (card.card_name == card_name_played_this_turn) return false;
        //}
        return true;
    }


    public void OnDrag(PointerEventData eventData)
    {
        if (m_DraggingIcons[eventData.pointerId] != null)
            SetDraggedPosition(eventData);
    }

    //void move_to_transform(Transform transform)
    //{

        //CardTransfer  cardtransfer= GetComponent<CardTransfer>();
        //disable this image..
        //move this transform to target...
        //completely disable the original?

        //GameObject icon = create_temporary_image();
        

        //LeanTween.move(icon, transform.position, 0.6f).setEase(LeanTweenType.easeInQuad).setOnComplete(cardtransfer.EnableImage);
    //}

    //DisableImage();


    //create an inventory with all disabled images...
    //on start, they auto drag a bunch of fake images from teh inventory of origin...???

    GameObject create_temporary_image()
    {
        temporary_image = Instantiate(gameObject, canvas.transform);
        
        temporary_image.transform.SetParent(canvas.transform, false);
        temporary_image.transform.SetAsLastSibling();
        temporary_image.transform.localScale = transform.localScale;
        var group = temporary_image.AddComponent<CanvasGroup>();
        group.blocksRaycasts = false;

        //temporary_image.GetComponent<CardUIImage>().initialize(pic.sprite, header.text);
        temporary_image.GetComponent<CardUIScript>().enabled = false;

        temporary_image.transform.position = transform.position;

        return temporary_image;
    }
    //do we assume it's possible for some reason that we want to create mulitple???
    //lets not okay!


    public void OnBeginDrag(PointerEventData eventData)
    {

        print("on Begin Drag");

        // We have clicked something that can be dragged.
        // What we want to do is create an icon for this.

        m_DraggingIcons[eventData.pointerId] = Instantiate(CardUI_Drag_Prefab);

        //, this.transform); but it can't have any ... stuff?

       // m_DraggingIcons[eventData.pointerId] = new GameObject("dragging_object");


//        m_DraggingIcons[eventData.pointerId].transform.SetParent(canvas.transform, true);
        m_DraggingIcons[eventData.pointerId].transform.SetParent(canvas.transform, false);
        m_DraggingIcons[eventData.pointerId].transform.SetAsLastSibling();
        m_DraggingIcons[eventData.pointerId].transform.localScale = this.transform.localScale;

        //var image = m_DraggingIcons[eventData.pointerId].AddComponent<Image>();
        // The icon will be under the cursor.
        // We want it to be ignored by the event system.
        var group = m_DraggingIcons[eventData.pointerId].AddComponent<CanvasGroup>();
        group.blocksRaycasts = false;

        m_DraggingIcons[eventData.pointerId].GetComponent<CardUIImage>().initializeBig(card_image_manager,pic.sprite, card.card_name);

        //image.sprite = GetComponent<Image>().sprite;
        //image.SetNativeSize();


            
        if (dragOnSurfaces)
            m_DraggingPlanes[eventData.pointerId] = transform as RectTransform;
        else
            m_DraggingPlanes[eventData.pointerId] = canvas.transform as RectTransform;

        m_TempPositions[eventData.pointerId] = default(Vector3);
        SetDraggedPosition(eventData);


        DisableImage();
    }


    private void DisableImage()
    {
        transform.GetChild(1).gameObject.SetActive(false);
    }


    private void EnableImage()
    {
        //why wouldn't this exist?
        //because it has been bought or sold.. and possibly this view has already been refreshed.
        if (this !=null)  transform.GetChild(1).gameObject.SetActive(true);
        //of course if it's gone then the temporary image certainly isn't going to be around either right?
        
        //why has this object been destroyed???  shouldn't it still be accssible?

        
        //this will destroy extras if for some weird reason the player has extras in transit...shoudl really trakc which buttons were released?
        if (temporary_image != null) Destroy(temporary_image);

        foreach (KeyValuePair<int, GameObject> pair in m_DraggingIcons)
        {
            if (pair.Value != null)
            {
                Destroy(pair.Value);
            }
        }
        view.refresh();
        //might need to refresh multiple views though...  how do we even do that? some kind of global knowledge of all the open views.

    }
    //   if (this.transform.GetChild(1).gameObject.activeSelf == false) return;

    Vector3 get_global_position(Transform transform)
    {
        //

        Vector3 global_pos;
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(canvas.transform as RectTransform, transform.position, maincamera, out global_pos))
        {
            return global_pos;
        }
        else print("no luck");
        return default(Vector3);
                //rt.GetWorldCorners


    }

    public Rect GetScreenCoordinates(RectTransform uiElement)
    {
        var worldCorners = new Vector3[4];
        uiElement.GetWorldCorners(worldCorners);
        var result = new Rect(
                      worldCorners[0].x,
                      worldCorners[0].y,
                      worldCorners[2].x - worldCorners[0].x,
                      worldCorners[2].y - worldCorners[0].y);
        return result;
    }


    private void SetDraggedPosition(PointerEventData eventData)
    {
        if (dragOnSurfaces && eventData.pointerEnter != null && eventData.pointerEnter.transform as RectTransform != null)
            m_DraggingPlanes[eventData.pointerId] = eventData.pointerEnter.transform as RectTransform;
        //drag on surfaces don't really know what that means... but lets set it to true...
        //theoretically this dragging plane thing is where i'm trying to drop???

        var rt = m_DraggingIcons[eventData.pointerId].GetComponent<RectTransform>();
        Vector3 globalMousePos;
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(m_DraggingPlanes[eventData.pointerId], eventData.position, eventData.pressEventCamera, out globalMousePos))
        {
            rt.position = globalMousePos;
            rt.rotation = m_DraggingPlanes[eventData.pointerId].rotation;

            if (m_TempPositions[eventData.pointerId] == default(Vector3)) m_TempPositions[eventData.pointerId] = transform.position;
                //m_TempPositions[eventData.pointerId] = get_global_position(transform);

        }
    }
    //Player_Job
    //what about counting?????  Or do we just hve a count of stackable cards, and display them as such?


    public bool Is_Drag_Logical()
    {
        //is it an action??? can't pick up... is it an occupation.. probably don't want to pick it up... though could...  easier to just make it a click on...
        return false;
    }

    //is there an inventory here? for example i know about buying and selling but not what dragging a job means...
    public bool Is_Drop_Logical(Transform target)
    {
        //can't equip an action for example, or drag an action to an inventory...
        return false;
    }

    //do you have enough money? for example?
    //or maybe too full to eat...
    public bool Is_Drop_Legal(Transform target)
    {
        //in which case we want some kind of alert-- you don't have enough money for that... etc.
        return false;
    }

    //is it a reaonable thing to do-- which will help us ask if it is the only reasonable thing to do...
    public bool Is_Drop_Practical(Transform target)
    {
        return false;
    }


    public void OnEndDrag(PointerEventData eventData)
    {
        //LeanTween.move(m_DraggingIcons[eventData.pointerId], m_TempPositions[eventData.pointerId], 0.7f).setEase(LeanTweenType.easeInQuad).setOnComplete(EnableImage);

        //check m_DraggingIcons[eventData.pointerId] game object for a reasonability...  

        //need to know player stats...  and should know all the places to drop to...  but don't NEED to know all those places..
        LeanTween.move(m_DraggingIcons[eventData.pointerId], m_DraggingPlanes[eventData.pointerId], 0.4f).setEase(LeanTweenType.easeInQuad).setOnComplete(EnableImage);

        //okay so this move to thing seems like it might work???
     

        ;
        print("on END DRAGON");
    }
    //ExecuteEvents.Execute<IPointerClickHandler>(objFM, new PointerEventData(EventSystem.current), ExecuteEvents.pointerClickHandler);


    public void ChooseOption(Text choice)
    {
        ChooseOption(choice.text);
    }


}
