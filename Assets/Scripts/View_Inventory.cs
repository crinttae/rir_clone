﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class View_Inventory : MonoBehaviour {
    public Conditions conditions;
    // public GameObject active_player;
    public PlayerVariables pvar;
    public Controller controller;
    public CardImageManager card_image_manager;

    public bool already_refreshed = false;


    public Inventory current_inventory;
    public GameObject player;
    public Transform playerimage;
    public List<string> itemtypes = new List<string>();

	public List<Card> cards_to_draw = new List<Card> () ;
	public List<bool> disableds;
	public List<string> descriptions;


	public List<Card_View> card_views = new List<Card_View>();


	public bool conditions_checked = false;

	public UIText uitext;

	public string inventory_name;
	public float buyprice_modifier=1;
	public float sellprice_modifier=1;

	public bool acquiring = false;
	public bool unaquire = false;
	public string action_on_complete="";


    //send objects to locations...
    float description_timer;
    void Start()
    {
		
        player = transform.GetComponentInParent<PlayerVariables>().gameObject;
        
        pvar = player.GetComponent<PlayerVariables>();
        controller = player.GetComponent<Controller>();
        conditions= player.GetComponent<Conditions>();
        card_image_manager = player.GetComponent<CardImageManager>();
        playerimage = card_image_manager.player_image;
		uitext = pvar.uitext;
        refresh();
    }

    


    void OnEnable()
    {

        refresh();
    }

    void OnDisable()
    {

    }

    public void refresh()
    {
        if (already_refreshed)
        {
            already_refreshed = false;
            return;
        }
        if(current_inventory!=null) Process(current_inventory);
        
        //really should jsut go through each card and refresh based on that.. 
        //or possibly should just change up the colors!
    }


    


    //default inventory vs. alternative inventory???
    //some kind of player switches that knows which cards should be where?
    //eg. equipment + inventory


	//each location has an inventory.  each invntory has a bunch of "cards", which include jobs, actions, items...
	//when you open an inventory, the game has to determine what to show you.. if you're opening your own player inventory you see all your items..
	//but like if you open a location's inventory, then you see things you can buy, actions you can do, jobs you have...

	//also actions can open up further?? inventories... for items you can craft, jobs you can take, etc.  Which is what "acquisition" means here...

	public void Process(Inventory inventory, string acquisition="", string action_on_complete="" , bool unacquire = false)
    {
		//"Pet" , "GiveStat|Strength|8", true

		if (acquisition != "") {
			acquiring = true;
			this.unaquire = unacquire;
		} else {
			acquiring = false;
		}
		this.action_on_complete = action_on_complete;


		buyprice_modifier = 1;
		sellprice_modifier = 1;

        if (!this.isActiveAndEnabled) return;
        if (card_image_manager==null) return;

        current_inventory = inventory;
		inventory_name = inventory.gameObject.name;
			
		if (current_inventory.location_inventory) 
		{
			float discount = 0;
			//this should be passed from the refresh... 
			if( pvar.Discount.TryGetValue(inventory_name,out discount)){
				buyprice_modifier *= (1 - discount);		
			}
				
		}

        inventory.view = this;

        itemtypes = new List<string>();

        card_image_manager.destroy_spread(transform);
        //get the material of the sprites and then change them to grayscale which is default sprite with grayscale added

        //current_location = location;
        /*ist<string> cards = new List<string>();*/
        

        //Inventory inventory = location.GetComponent<Inventory>();
		card_views.Clear ();
        


        if (inventory.location_inventory)
        {
            foreach (Card card in inventory.cards)
            {
				conditions_checked = false;

                //probably a lot easier to do this by reasonable pieces...
                if (!acquiring)
                {
					
					if (card.type == "Job" || card.type == "Class" || card.type == "Home") {
						if (inventory.gameObject.name == "Notice Board")
							continue;

						if (!pvar.DoesPlayerHave (card))
							continue;
						
						if (!conditions.check_conditions (card))
							continue;




					} else if (card.type == "Action") {
						if (!conditions.check_conditions (card, 0, this))
							continue;
						//will this mess up the order of conditions???
					} 


					  else if (card.type == "Item" ||card.type == "Status") 
					{
						if (inventory.home_inventory)
							continue;
						if (!conditions.check_conditions (card))
							continue;


					}




                }
                else   //if we are acquiring!
                {
					
                    if (card.subtype != acquisition) continue;

					if (card.type == "Job" || card.type == "Class" || card.type == "Home") {
						if (pvar.DoesPlayerHave (card))
							continue;
					}


					//do we even have a gold check?
					if (card.type == "Job"  || card.type == "Home")
					{
					if (!conditions.check_conditions(card, 50,this)) continue;
					}


					else if (card.type == "Item" || card.type == "Status")
					{//we want to check the crafting requirements, but want to pass regardless so the player can see the cards as disabled.
						if (conditions.check_crafting_conditions(card, 0,this)){}
					}

					else if (!conditions.check_conditions(card, 0,this)) continue;	




					//loosely acquiring jobs-- could be an issue for other thigns though...

					//okay way less crazy: we keep the lists here.. it's a gerneal list and we clear it.
					//the lists are all public.
					//we run the check conditions function and it fills in the lists here...
					//then we pass those lists on to cardimagemanager...
					//which is maybe a dumb degree of passing things around...
					//especially since the list is here and could literally be accessed by the card itself.
					//all the card needs is an index.

                }
				//we actually never set add card to false.
				//also if we own the card, how do we get its description???
				//so we want to call something like
				//conditions.process_card(check condition?,this)



                //this is an ugly little bit.
				if (card.type == "Item" || card.type == "Status")
                {
                    Item item = card as Item;
                    bool found =false;
                    foreach (string type in itemtypes)
                    {
						if (item.subtype == type)
                        {
                            found=true;
                        }
                    }
					if (!found) itemtypes.Add(item.subtype);
                }
                //itemtypecode...  these are the kinds of things being sold at hte store...
			
			//Create_Full_Card_object(card,

				//if we get this far we are assuming to be adding something although who knows exactly what.
			if (conditions_checked)
					card_views.Add(new Card_View(card,conditions.disabled_to_send,card_image_manager.get_standard_description (card) + conditions.description_to_send));
				else
					card_views.Add(new Card_View(card,false,card_image_manager.get_standard_description (card)));

//			cards_to_draw.Add (card);
//				if (conditions_checked) {
//					descriptions.Add (card_image_manager.get_standard_description (card) + '\n' + conditions.description_to_send);
//					disableds.Add (conditions.disabled_to_send);
//
//				} else {
//					descriptions.Add (card_image_manager.get_standard_description (card));
//					disableds.Add (false);	
//				}
			

			}

        }//end of location inventory...


        //player inventory automatically gets everything... should be above this stuff.
        if (inventory.player_inventory)
        {
			conditions_checked = false;
            foreach (Card card in inventory.cards)
            {
				card_views.Add(new Card_View(card,false,card_image_manager.get_standard_description (card)));
               
//				cards_to_draw.Add(card);
//				descriptions.Add (card_image_manager.get_standard_description (card));
//				disableds.Add (false);


				//we still don't have inventory
            }
        }


		//create the whole list here... and sort that here?

		//List<Card_View> card_views_sorted = card_views.OrderBy(o=>o.order_value).ToList();

		card_views.Sort((x, y) => x.order_value.CompareTo(y.order_value));


		card_image_manager.draw_cards_from_to (card_views, inventory.transform.position, this.GetComponent<RectTransform> ());

		//card_image_manager.draw_cards_from_to(cards_to_draw, inventory.transform.position,this.GetComponent<RectTransform>(),disableds,descriptions);
		//what if we added description to this function...
    }

}
public class Card_View
{
	public Card card;
	public bool disabled;
	public string description;
	public int order_value=0;

	public Card_View(Card new_card,  bool new_disabled=false, string new_description="")
	{
		card = new_card;
		disabled = new_disabled;
		description = new_description;
		//all I really care about is sorting value...

		if (disabled)
			order_value = order_value + 10000;
		

		if (card.type == "Job") {
			Job job = card as Job;
			order_value = job.gold;
			order_value -= 1000;
		}
		else if (card.type == "Item") {
			Item item = card as Item;
			order_value = item.price;
			order_value += 1000;
		}

	
	}

	public void change_descrption (string new_description)
	{
		description=new_description;
	}
}