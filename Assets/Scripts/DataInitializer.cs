﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class DataInitializer : MonoBehaviour {
    GameObject canvas;
    GameObject[] locations;
    
    // Use this for initialization
    //we want to call this last somehow...

    //THIS gets called from start... everything else here is already available...
    //so I can change the save slot.. but I don't call this.. what I call is the scene reset.




    void Start()
    {
        print (this.name);
		DataManager.initialize ();

        bool loading = DataManager.loading;
        string file_name= DataManager.current_save_slot;

        List<GameObject> players = new List<GameObject>();
        DataManager.Current_Canvas = GameObject.Find("PrimaryCanvas");

        GameObject player1 = GameObject.FindGameObjectWithTag("Player");

        //inventories = Object.FindObjectsOfType(typeof(Inventory)) as Inventory[];
        //canvas = GameObject.Find("PrimaryCanvas");

        //locations = Object.FindObjectsOfType(typeof(Location)) as Location[];
        DataManager.locations = GameObject.FindGameObjectsWithTag("Location");

        //would be better if i actually made a dicitonary of locaitons wouldn't it... then be able to be like...
        //location0, location1...  go 1-10 for each, if it has the right location then add..


        foreach (GameObject location in DataManager.locations)
        {
            //these dictionaries are all the same, uhg!  it would be better if were instantiating somehow 
            Inventory location_inventory = location.GetComponent<Inventory>();
            string location_name = location.name;


            foreach (KeyValuePair<string, Job> entry in Job_Dictionary.Jobs)
                if (entry.Value.type == "Class")
                {
                    // if (locatio)
                    //are classes in the school inventory? or some secret inventory behidn the school?
                    //lets say they're jst in the school inventory.. but you can't just see them unless you're enrolled in them...
                    //so i need a special action card.. which is enroll in classes which you call from the school. which pulls up all the classes?
                    //enrollment is free but it takes time?  but lets say not much time... can you be enrolled in multiple?  Sure for now.
                    if (entry.Value.location == location_name)
                    {
                        Job job = entry.Value.Clone();
                        location_inventory.cards.Add(job);
                    }
                }

                else if (entry.Value.type == "Home")
                {
					if ("Notice Board" == location_name  &&  entry.Value.card_name!="Log Cabin")
                    {
						
                        Job job = entry.Value.Clone();
                        location_inventory.cards.Add(job);

                    }
                    if (entry.Value.location == location_name)
                    {
                        Job job = entry.Value.Clone();
                        location_inventory.cards.Add(job);
                    }

                }

                else if (entry.Value.type == "Job")

                {
                    if (entry.Value.location == location_name)
                    {
                        Job job = entry.Value.Clone();
                        location_inventory.cards.Add(job);
                        // location_inventory.cards.Add(new Tuple<Card,int>(entry.Value,1));
                    }

                    if ("Notice Board" == location_name)
                    {
                        Job job = entry.Value.Clone();
                        location_inventory.cards.Add(job);
                    }
            }

            foreach (KeyValuePair<string, Item> entry in Item_Dictionary.Items)
            {
				
                if (entry.Value.location == location_name)
                {
                    Item item = entry.Value.Clone();
                    location_inventory.cards.Add(item);
                }

				if (location_inventory.home_inventory && entry.Value.type=="Item") 
				{
					
					if (entry.Value.crafting_requirements != default(List<string>)) {
						Item item = entry.Value.Clone ();
						location_inventory.cards.Add(item);
					}
				}


                
            }

            foreach (KeyValuePair<string, Action> entry in Action_Dictionary.Actions)
            {

				if (entry.Value.location == location_name || entry.Value.location=="Home" && location_inventory.home_inventory)
                {
                    Action action= entry.Value.Clone();
                    location_inventory.cards.Add(action);

                }
            }

        }
        //load stuff..

        players.Add(player1);

        //if we're loading then we should load the number of players...
        //if we're not loding, then we should know the number of players from the dataManager...
        //so if we're loading lets just load them into the datamanager...
        //should we be passing which file to load from?????
        if (loading) DataManager.number_of_players = ES2.Load<int>(file_name + "?tag=" + "player_count");

        for (var player_number = 1; player_number < DataManager.number_of_players; player_number++)
        {
            GameObject newplayer = Instantiate(player1,DataManager.Current_Canvas.transform );
            var player_variables = newplayer.GetComponent<PlayerVariables>();
            player_variables.player_number = player_number;

            players.Add(newplayer);
        }



        foreach (GameObject player in players)
        {
            var pvar = player.GetComponent<PlayerVariables>();

            if (!loading)
            {
                Job no_job;
                if (Job_Dictionary.Jobs.TryGetValue(DataManager.base_job, out no_job)) pvar.Job = no_job.Clone();
                pvar.gold = DataManager.starting_gold;
                

                pvar.base_home = DataManager.base_home;

				Job no_home;
				if (Job_Dictionary.Jobs.TryGetValue(DataManager.base_home, out no_home)) pvar.Home = no_home.Clone();


                //now is the time to set jobs and stats and so on for all the loading stuff...
            }
			else if (loading)
            {

                //things to add: base home...

                string Ptag = file_name + "?tag=P" + pvar.player_number;

                pvar.name = ES2.Load<string>(Ptag + "name");
                pvar.time = ES2.Load<float>(Ptag + "time");
                pvar.virtue = ES2.Load<float>(Ptag + "virtue");

                pvar.Salary = ES2.Load<int>(Ptag + "Salary");
                pvar.gold = ES2.Load<int>(Ptag + "gold");
                pvar.stress = ES2.Load<int>(Ptag + "stress");
                pvar.food = ES2.Load<int>(Ptag + "food");

                pvar.location = GameObject.Find(ES2.Load<string>(Ptag + "location"));
                
				Job try_home;
				string home_name = ES2.Load<string>(Ptag + "Home");
				if (Job_Dictionary.Jobs.TryGetValue(home_name, out try_home)) pvar.Job = try_home.Clone();



                pvar.Friend = ES2.Load<string>(Ptag + "Friend");
                player.GetComponent<Controller>().player_sprite.transform.position = ES2.Load<Vector2>(Ptag + "position");

                string job_name = ES2.Load<string>(Ptag + "Job");

                Job try_job;
                if (Job_Dictionary.Jobs.TryGetValue(job_name, out try_job)) pvar.Job = try_job.Clone();


                foreach (Stat stat in pvar.Stats)
                {
                    stat.Actual= ES2.Load<float>(Ptag + stat.Name);
                }


                Inventory inventory = player.GetComponent<Inventory>();

                string load_string = ES2.Load<string>(Ptag + "Inventory"); ;
                string[] item_names = load_string.Split('|');
                Item try_item;
                foreach (string card in item_names)
                {
                    string[]  item_parts = card.Split('/');
                    string item_name = item_parts[0];
                    if (Item_Dictionary.Items.TryGetValue(item_name, out try_item))
                    {
                        if(item_parts.Length>1) try_item.stacks = int.Parse(item_parts[1]);
                        inventory.cards.Add(try_item.Clone());
                    }
                }
            }//if loading
		pvar.refresh_modifiers();
		pvar.refresh_sliders();
			//ultimately this player spite transform should be decided here, for both loading and nonloading games.

		}





        if (!loading)
        {
            TurnManager.active_player_number = 0;
            TurnManager.week = 0;
        }
        if (loading)
        {
            TurnManager.active_player_number = ES2.Load<int>(file_name + "?tag=" + "current_player");
            TurnManager.week = ES2.Load<int>(file_name + "?tag=" + "week");
        }
        

        DataManager.players = GameObject.FindGameObjectsWithTag("Player");
        TurnManager.players = DataManager.players;


        foreach (GameObject player in players)
        {
            player.SetActive(false);
            
        }
        //we will set the active player in turnmanager.

        ;
        DataManager.game_playing = true;


		DataManager.build_dictionary_list ();
        TurnManager.Initiate();

        
        }

    }





