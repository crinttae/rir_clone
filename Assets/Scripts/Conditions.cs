﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System;

public class Conditions : MonoBehaviour {

    PlayerVariables pvar;
    Controller controller;
    Sounds sounds;
    CardImageManager cardimagemanager;
    UIText uitext;



	//well everything could be publc... that's pretty weird but it would work.
	//public List <float> Contributions;

	public string description_to_send;
	public bool disabled_to_send;
	Card card_to_send;

    float failure_modifier = 1;    //can fail  0=can not fail...

	float critical_success_multiplier = .2f;
	float critical_exponent = 2;
	//so initial prob success .5
	//*.2=.1
	//^2=.01
	//meeting reqs... 1
	//*.2
	//+50...  .4..
	//.8 if 2 stats...


	//you double your modifiers...so.. req +50
	//.05 becomes .1...
	//req  +100 all around...
	//but you should be getting it allt he bloody time then...?  right?
	//so maybe it hsoudl just be like... final p ^2?
	//then it really will be all the time...

	//p crit
	//for each condition a critical success is .05 to begin with and scales???


	float stress_problems_impossible = 80f;
	float stress_problems_begin = 50f;

    float certain_failure = -50f;
    float certain_success = 0f;
    //float bonus_rate = .5f;
    //float max_bonus = .5f;

	//these are the respective p's for a bonus...
	float stat_reward_1= 2f;
	float stat_reward_2= 1.5f;
	float stat_reward_3= 1f;
//	float gain3=0f;



    void Awake()
    {
        pvar = GetComponent<PlayerVariables>();
        controller = GetComponent<Controller>();
        sounds = GetComponent<Sounds>();
        cardimagemanager = GetComponent<CardImageManager>();
        uitext = cardimagemanager.UIText;
        // do_job(get_job("Baker"));

    }

    //for each card of type x, y, z... loop through dictionaries to collect conditions...




    public bool modify_attributes(List<string> mods, bool Change_Base = true)
    {
        var modified_attributes = 0;
        for (var i = 0; i < mods.Count; i++)
        {
            if (modify_attribute(mods[i], Change_Base)) modified_attributes++;
        }
        return modified_attributes == mods.Count;
    }

    bool modify_attribute(string mod, bool Change_Base = true)
    {
        if (mod == "") return true;
        string[] parts = mod.Split(' ');

        string left = parts[0];
		string right = parts[1];
        float float_right = 0;

        bool is_number = float.TryParse(right, out float_right);
        if (is_number)
        {
            try {

                pvar.change_stat(left, float_right, Change_Base);
            }
            catch {
                print("something wrong with modifying! " + left);
                return false;
            }

            return true;
        }
        else
        {
            print("trying to modify but the second part isn't a number!");
            return false;
        }

    }



    Job get_job(string job)
    {
        foreach (KeyValuePair<string, Job> entry in Job_Dictionary.Jobs)
        {
            if (entry.Key == job) return entry.Value;
        }
        return null;
    }





//	float get_probability_of_requirement (string requirement, bool actually_doing_job=false)
//	{
//	}


	public bool can_afford_to_do(Job job)
	{
		if (pvar.stress > stress_problems_impossible && job.stress > 0)
		{
			uitext.DisplayText("You're too tired to even try!");
			return false;
		}

		if (job.stress<0 && pvar.stress==0 && job.type=="Home")
		{
			uitext.DisplayText("You aren't tired.");
			return false;
		}

		if (job.type == "Class")  //for some dumb reason rent is handled seperately 
		{
			if (job.gold + pvar.gold < 0) 
			{
				uitext.DisplayText ("You can't afford the tuition.");
				return false;	
			}
		}

		return true;
	}


    public bool do_job(Job job)
    {

		if (!can_afford_to_do(job) )
			return false;
		
		string text_to_display = "";



        if (job.type == "Home")
        {
            if (job.stress != 0) pvar.give_stress(job.stress);
            if (job.time != 0) pvar.give_time(job.time);
            if (job.virtue != 0) pvar.give_virtue(job.virtue);
            return true;
        }
        

        List<float> ps = new List<float>();
        List<Stat> stats_to_check = new List<Stat>();
        Stat stat_to_check = null;
        bool success = false;
		bool critical_success = false;
        int stat_reward = 0;
        if (job.type == "Class")
        {
			
            if (job.stress != 0) pvar.give_stress(job.stress);
            if (job.time != 0) pvar.give_time(job.time);
            if (job.gold != 0) pvar.give_gold(job.gold);
            if (job.virtue != 0) pvar.give_virtue(job.virtue);

            foreach (string requirement in job.requirements)
            {
                //eg. Science >= 250     this is all pretty dumb, just should say science | 200   ...but whatever...
                string[] parts = requirement.Split('|');
                stat_to_check = pvar.get_stat_from_string(parts[0]);
                stat_reward = 3;
                stat_to_check.change(stat_reward,true);
				text_to_display ="You spent " + (- job.gold) + "GP and improved " + stat_to_check.Name + " " + stat_reward +". It is now "+stat_to_check.Actual;
                uitext.DisplayText(text_to_display);
            }
            return true;
        }


		string probability_log = "prob for job: ";

		//pull this into its own function based on requiremetns and requirement...
		//Pickpocket:  {"Agility|-|30","Stealth|-|30","Law|+|25"}



		foreach (string requirement in job.requirements) {
			//float p = get_probability_of_requirement (requirement, true);

			string[] parts = requirement.Split ('|');
			float float_right;
			bool is_number = float.TryParse (parts [2], out float_right);
			if (is_number) 
			{
				stat_to_check = pvar.get_stat_from_string (parts [0]);

				if (!stat_to_check.Repuation) {
					
					stats_to_check.Add (stat_to_check);
					float float_left = stat_to_check.Actual;
					float p = probability_of_success (float_left, float_right);


					ps.Add (p);
					probability_log += stat_to_check.Name + ":" + p;

				} else {  //reputations
					//"Law|+|25"  So for example we want a negative law here...
					float aim=0;
					if (parts [1] == "+") {
						if (stat_to_check.Actual > float_right) {
							uitext.DisplayText ("Your reputation with " + parts [0] + " is too high!");
							return false;
						}
						aim = float_right - 100;
					} else if (parts [1] == "-") {
						if (stat_to_check.Actual < float_right) {
							uitext.DisplayText ("Your reputation with " + parts [0] + " is too low!");
							return false;
						}
						aim = float_right + 100;
					}

					float change_reputation = Mathf.RoundToInt ((aim - stat_to_check.Actual) / 30); 

					//if the aim differs by more than 25

					//if (Mathf.Abs(aim)>25)  //if the aim is significant in the first place... 
					if (Mathf.Abs (change_reputation) > 0) {
						stat_to_check.change (change_reputation);
						print (stat_to_check.Name + " just changed to " + stat_to_check.Actual);
					}
				}
			}
		}

		print (probability_log);


		//care about the stress problem too....


		float final_p = get_total_probability_of_success (ps, true);
		float critical_p = Mathf.Pow(critical_success_multiplier * final_p, critical_exponent);

		float test = Random.Range (0f, 1f);  
		if ((test) * failure_modifier < final_p) {
			success = true;
			if ((test) * failure_modifier < critical_p)
				critical_success = true;
		}





		if (critical_success)
			text_to_display = "Wow!  You did an great job, you get " + job.gold * 1.5 + "GP.";
		else if (success)
			text_to_display = "Good work! Valentine, you get " + job.gold + "GP.";
		else if (!success)
			text_to_display = "You did a poor job, and get no money.";


		List<float> challenges = new List<float> ();
		float challenge_sum = 0f;
		foreach (float p in ps) {
			float challenge = stat_reward_1 - p;
			if (challenge < 0)
				challenge = 0;
			challenge_sum += challenge;
			challenges.Add (challenge_sum);
		}
		if (challenge_sum > 0) {
			float random_stat_roll = Random.Range (0, challenge_sum);
			for (int j = 0; j < challenges.Count; j++) {
				if (random_stat_roll <= challenges [j]) {
					Stat stat_boosted = stats_to_check [j];

					if (ps [j] < stat_reward_3)
						stat_reward = 3;
					else if (ps [j] < stat_reward_2)
						stat_reward = 2;
					else
						stat_reward = 1;
					
					//stat_boosted.change (stat_reward);
//					text_to_display += stat_boosted.Name + " improved " + stat_reward;
					pvar.DoActionString("GiveStat|"+stats_to_check [j].Name+"|"+stat_reward,true,text_to_display);
					break;  

				}
			}
		}

		//this works at the top but not th bottom.. why..
		int exp;
		if (pvar.Job_Exp.TryGetValue(job.card_name, out exp)) 
		{
			pvar.give_job_exp(job.card_name);
		}
		else print("job experience can't be allocated to " + job.card_name);
		//trygetvalue(pvar.Job_Exp()

		//uitext.DisplayText (text_to_display);

		pvar.give_stress (job.stress);
		pvar.give_time (job.time);

		if (critical_success)
			pvar.give_gold(Mathf.RoundToInt(job.gold*1.5f));


		if (success)
			pvar.give_gold (job.gold);


		sounds.play_sfx (sounds.work);

		return success;
    }

	public float get_total_probability_of_success(List<float> ps, bool actually_doing_job=false)
	{
		float total_p = 1 * DataManager.base_job_probability;
		foreach (float p in ps) {
			//.5,.8,1.5
			total_p *= p;
		}
		float stress_problem = ((pvar.stress - stress_problems_begin) / (stress_problems_impossible- stress_problems_begin));
		if (stress_problem < 0) stress_problem = 0;
		//0 at 50, 1 at 80.
		total_p *=(1-stress_problem);

		if (stress_problem>0 && actually_doing_job) uitext.DisplayText("You're getting tired...  You should rest soon");

		return total_p;
	}


    public float probability_of_success (float stat, float req)
    {

		//0,-50
		//80/100????

		//shoudl be like 1+... 
		//req:0
		//stat:0
		//then p should = .5??

        //certain success=100,  certain failure=-30
		//okay lets say certain success is zero, certain failure is -50...
//		though.. really teh denominator doesn't matter... so lets say certain failure is -50 and the denominator is 50.

		float p =  (stat - certain_failure - req) / (certain_success - certain_failure );
		//so.. at -50 p is zero...
		//at stat=req, p = 50/50=1... which then gets multiplied by base probability -- so .5

        //if (p > 1 + max_bonus) p = 1 + max_bonus;
        if (p < 0) p = 0;
        return p;
    }




	public bool check_conditions(Card card, float slack=0, View_Inventory view = null, bool include_description=true)
	{
		
		if (check_conditions (card.requirements, slack, view ,include_description)) {
			if(view!=null) view.conditions_checked = true;
			return true;
			//really what's wrong with printing this stuff on the card?????

		}
		return false;
	}



	public bool check_crafting_conditions(Card card, float modifier=0, View_Inventory view = null,bool include_description=true)
	{
		if (card.type != "Item")
			return false;
		Item item = card as Item;

		if (check_conditions (item.crafting_requirements, modifier, view , include_description)) {
			if(view!=null) view.conditions_checked = true;
			return true;
		}

		return false;
	}


	//returns bool if false we continue if true that means we've added the card to the list
	//as well as something for description and something for enabled.
	public bool check_conditions(List<string> conditions, float slack=0, View_Inventory view = null, bool pull_description=true)
    {

		description_to_send = "";
		disabled_to_send = false;

        float success_sum = 1f;
        for (var i = 0; i<conditions.Count; i++)
        {
			float contribution = check_condition (conditions [i] , false,slack); 
			success_sum *= contribution;
			}

		if (success_sum < 1)
			return false;

		float no_slack_sum = 1f;
		for (var i = 0; i<conditions.Count; i++)
		{
			float contribution = check_condition (conditions [i] , pull_description,0); 
			no_slack_sum *= contribution;
		}

		if (no_slack_sum < 1)
			disabled_to_send = true;	

		if(pull_description)
			if(description_to_send != "") 
				description_to_send = "Requirements" + '\n' + description_to_send;

		if (success_sum >= 0)
			return true;


		return false;
		//of course now instead of doing some things twice, we're needlessly doing everything twice...

    }
    //could look for greater less than equal to.. etc. 

    //this really should be converted to a number, not the other way! duh.
    


    string convert_condition_string(string condition_text)
    {
        switch (condition_text)
        {
            //could get an integer for all locations instead...
		case "Rent is due": {
				if (pvar.weeks_till_rent == 0  && pvar.Home.gold<0)
					return "true";
				else return "false";
			}
			
			case "location": return pvar.location.name;
            case "home": return pvar.Home.card_name;
			
            default: print("failed to find condition" + condition_text); return "";
        }
    }

    
    //could return how much we fail by...
    public float check_condition(string condition_to_check, bool pull_description = true, float slack = 0)
	{
		if (condition_to_check == "")
			return 1;
		if (condition_to_check == "home") {
			if (pvar.Home.location == pvar.location.name)
				return 1;
			else
				return 0;
		}
		string[] parts = condition_to_check.Split ('|');
		return check_condition_by_parts (parts, slack ,pull_description);
	}

	public float check_condition_by_parts(string[] parts,float slack=0, bool pull_description = true)
		{

		float contribution =1;
		string description_part = "";
		string color= "";
		string left = parts [0];
		string middle = parts [1];
		string right = "";
		if (parts.Length>2) right = parts [2];

			//if we are checking to see if the player has items, as in the case of crafting.
		if (left == "Item") {
			int stacks = 1;
			if (right != "")
				int.TryParse (right, out stacks);

			if (pvar.DoesPlayerHave (middle, stacks))
				contribution = 1;
			else
				contribution = 0;
			description_part =  cardimagemanager.create_link(middle);
			if (stacks > 1)
				description_part += " " + stacks.ToString ();
		} else {
			//this could be an issue here...
		
			float float_right = 0;
			bool is_number = float.TryParse (right, out float_right);


			//we're checking to see if the player has a stat, reputation, or job experience
			if (is_number) {
				try {
					//first we lower the bar...

					Stat stat = pvar.get_stat_from_string (left);



					
					//we handle the contirbutions differently between stat and reputation..
					//lets just leave the reputation in there for now regardless?
					if (stat.Repuation) {
						if (middle == "-") 		float_right-=slack;
						else if (middle == "+") float_right+=slack;
						//lets say the slack is just at 50 for reputation?


						float float_left = stat.Actual;
						if (middle == "-")						
							contribution = 1 + (float_left - float_right)/100;
						else if (middle == "+")
							contribution = 1 + (-(float_left - float_right))/100;
						else
							contribution = 0;

//						if (contribution >1.2)
//							description_part = "";
//						else 
						{
							if ((float_right > 0 && middle == "+") || (float_right < 0 && middle == "-"))
							description_part = "Not " + pvar.get_reputation_description (stat.Name, float_right);
							else
							description_part = pvar.get_reputation_description (stat.Name, float_right);
						}
						
						//for a reputation it doesn't matter how much you exceed the mark.
						if (contribution > 1) {
							contribution = 1;
						} 
						if (contribution < 1) {
							contribution = 0;
						} 

					} else {
						float_right -= slack;
						contribution = probability_of_success(stat.Actual,float_right);	
						description_part = left;
						if (float_right > 0)
							description_part += " " + right;
						
					}
				
				} catch {
					//                print(left + " is not a stat?");
						int int_left=0;
						contribution = 0; //just assume total fail unless otherwise.
						if (pvar.Job_Exp.TryGetValue(left ,out int_left)) {
							float_right = DataManager.default_job_mastery;
							
							description_part = left + " experience: " + right;
							if (check_if_left_equals_right (int_left, middle, float_right))	contribution = 1;
						} else if (left == "Gold") {
							if (check_if_left_equals_right (pvar.gold, middle, float_right))	contribution = 1;
						} else if (left == "Stress") {
							if (check_if_left_equals_right (pvar.stress, middle, float_right))	contribution = 1;
						} else if (left == "Time") {
							if (check_if_left_equals_right (pvar.time, middle, float_right))	contribution = 1;
						}
						else {print (left + " is not a stat or a job");
							contribution = 0;
						}
				} 
			} else {
				string string_left = convert_condition_string (left);
				if (middle == "==") {
					if (string_left == right)
						contribution = 1;
					else
						contribution = 0;
				} else {
					print ("attempting to compare a string with something besides ==");
					contribution = 0;
				}
			} //not number

		}//not item


		if (contribution >= 1)
			color = "<color=green>";
		else if (contribution > 0)
			color = "<color=orange>";
		else 
			color = "<color=red>";

		if (pull_description && description_part != "") 
		{
			description_part = color + description_part +"</color>" + '\n';
			description_to_send += description_part;
		}
		
		return contribution;
    }

	bool check_if_left_equals_right(float left, string middle, float right)
	{
		//eg. combat <= 50 intro combat
		//experience == 100

		switch (middle)
		{
		case "<":
			{
				return left < right;
			}
		case ">":
			{
				return left > right;
			}
		case "==":
			{
				return left == right;
			}
		case "<=":
			{
				return left <= right;
			}
		case ">=":
			{
				return left >= right;
			}
		case "!=": 
			{
				return left != right;
			}

		default: print("condition fail! "+ left + middle + right); return false;
		}
	}

    //list of conditions
    //
    //each card has a lit of conditions to be displayed...
    //min age.. min str.. min.. 
    //working job and location = x

    //OR not working job and location = y and conditions 1:20...
    //stat
    //>
    //20
    
    //can buy condition gray out... ones can't buy...

}

