﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Item : Card
{
//    public string item_type;

    public int price = 50;
    public int sellprice = 50;
    public int weight = 1;
    public bool consumable = false;
    public bool stackable = false;
    public int stacks = 1;
    public bool consumed_on_purchase = false;
    public int use_by = 9999;
	public string equip_to;
    public bool gradual = false;
    public bool sellable = true;
    public List<string> stat_modifiers = default(List<string>);  
	public List<string> crafting_requirements = default(List<string>);  

    //assume these are modifiers for equipment, and permenant for consumables?  I guess?
    

    public Item(string newcard_name, string newtype, string newdescription = "", string newlocation = "", List<string> newrequirements = default(List<string>),
		string newaction= "" , float newtime = 0,
       string newsubtype= "", int newprice = 1, int newsellprice = 0, int newweight = 1, bool newconsumable = false,   bool newstackable =false 
		, int newstacks = 1,  bool newconsumed_on_purchase=false, int newuse_by = 9999,
        string newequip_to = "", bool newgradual = false,  bool newsellable = true, List<string> newstat_modifiers = default(List<string>),
		List<string> newcrafting_requirements = default(List<string>)
        )
      
		: base(newcard_name, newtype, newdescription, newlocation, newrequirements, newaction, newtime,newsubtype)
    {
        //base constructor is called first?
		type = newtype;
		subtype = newsubtype;

        price = newprice;
        sellprice = newsellprice;
        weight = newweight;
        consumable = newconsumable;
        stackable = newstackable;
        stacks = newstacks;
        consumed_on_purchase = newconsumed_on_purchase;
        use_by = newuse_by;
        equip_to = newequip_to;
        gradual = newgradual;
        sellable = newsellable;
        stat_modifiers = newstat_modifiers;
		crafting_requirements = newcrafting_requirements;
    }

    public override void initialize(string name)
    {
        base.initialize(name);
        if (Item_Dictionary.Items.ContainsKey(card_name))
        {
            description = Item_Dictionary.Items[card_name].description;
            weight = Item_Dictionary.Items[card_name].weight;
        }
    }

    public Item Clone()
    {
        return (Item)this.MemberwiseClone();
    }

	public bool Is_a_Curse()
	{
		bool Is_a_Curse = false;
		foreach (string stat_modifer in stat_modifiers)
			//how to know if each stat is good or bad?
		{
		}
		return Is_a_Curse;
	}



}

public static class Item_Dictionary
{
    public static Dictionary<string, Item> Items = new Dictionary<string, Item>
    {
		{"Bread",new Item("Bread","Item","","Bakery",new List<string>(),"",0,"Baker",10,5,1,true,false,1,true,0,"",false,true,new List<string>(){"Food 1"},new List<string>(){})},
		{"Pie",new Item("Pie","Item","","Bakery",new List<string>(),"",0,"Bake Cake",20,10,1,true,false,1,true,0,"",false,true,new List<string>(){"Food 1","Stress -5"},new List<string>(){})},
		{"Fish",new Item("Fish","Item","","Market",new List<string>(),"",0,"Food",15,8,1,true,false,1,true,0,"",false,true,new List<string>(){"Food 1","Reason 1"},new List<string>(){})},
		{"Sheep",new Item("Sheep","Item","","",new List<string>(),"",0,"Pet",150,75,1,false,false,1,false,0,"Pet",true,true,new List<string>(){"Stress -10"},new List<string>(){})},
		{"Formal Dress",new Item("Formal Dress","Item","","Market",new List<string>(),"",0,"Sew Clothes",210,105,1,false,false,1,false,0,"Body",false,true,new List<string>(){"Charisma 20","Decorum 20"},new List<string>(){"Item|Thread|4"})},
		{"Cauldron",new Item("Cauldron","Item","","Market",new List<string>(),"",0,"Craft Equipment",60,30,1,false,false,1,false,0,"Hands",false,true,new List<string>(){"Cooking 10"},new List<string>(){})},
		{"Leather Armor",new Item("Leather Armor","Item","","Market",new List<string>(),"",0,"Gear",150,75,1,false,false,1,false,0,"Body",false,true,new List<string>(){"Combat 10","Stealth 20"},new List<string>(){})},
		{"Dark Robe",new Item("Dark Robe","Item","","Market",new List<string>(),"",0,"Sew Clothes",30,15,1,false,false,1,false,0,"Body",false,true,new List<string>(){"Combat 5","Stealth 5"},new List<string>(){"Item|Thread|3"})},
		{"Blessed Knife",new Item("Blessed Knife","Item","","Enchant Item",new List<string>(),"",0,"Gear",240,120,1,false,false,1,false,0,"Hands",false,true,new List<string>(){"Combat 40"},new List<string>(){"Item|Knife|1"})},
		{"Knife",new Item("Knife","Item","","Smith",new List<string>(),"",0,"Gear",120,60,1,false,false,1,false,0,"Hands",false,true,new List<string>(){"Combat 20"},new List<string>(){})},
		{"Holy Necklace",new Item("Holy Necklace","Item","","Church",new List<string>(),"",0,"Gear",420,210,1,false,false,1,false,0,"Head",false,true,new List<string>(){"Ritual 40","Life 30"},new List<string>(){})},
		{"Plate Armor",new Item("Plate Armor","Item","","Market",new List<string>(),"",0,"Craft Equipment",240,120,1,false,false,1,false,0,"Body",false,true,new List<string>(){"Combat 40","Stealth -20"},new List<string>(){"Item|Ingot|5"})},
		{"Lucky Shovel",new Item("Lucky Shovel","Item","","",new List<string>(),"",0,"Gear",150,75,1,false,false,1,false,0,"Hands",false,true,new List<string>(){"Reason 10","Strength 10","Charisma 10"},new List<string>(){})},
		{"Necromancer's Staff",new Item("Necromancer's Staff","Item","","School",new List<string>(),"",0,"Gear",240,120,1,false,false,1,false,0,"Hands",false,true,new List<string>(){"Ritual 40","Life -30"},new List<string>(){})},
		{"Deviled Egg",new Item("Deviled Egg","Item","","Bakery",new List<string>(),"",0,"Food",30,15,1,true,false,1,true,0,"",false,true,new List<string>(){"Food 1","Strength 1","Virtue -1"},new List<string>(){})},
		{"Mushroom",new Item("Mushroom","Item","","Bakery",new List<string>(),"",0,"Herbalist",30,15,1,true,false,1,true,0,"",false,true,new List<string>(){"Food 1"},new List<string>(){})},
		{"Magic Mushroom",new Item("Magic Mushroom","Item","","Bakery",new List<string>(),"",0,"Herbalist",30,15,1,true,false,1,true,0,"",false,true,new List<string>(){"Food 1","Strength 3"},new List<string>(){})},
		{"Angels Food Cake",new Item("Angels Food Cake","Item","","Bakery",new List<string>(),"",0,"Chef",30,15,1,true,false,1,true,0,"",false,true,new List<string>(){"Food 1","Stress -5","Purity 1"},new List<string>(){})},
		{"Spectacles",new Item("Spectacles","Item","","Smith",new List<string>(),"",0,"Gear",330,165,1,false,false,1,false,0,"Head",false,true,new List<string>(){"Reason 50"},new List<string>(){})},
		{"Beer",new Item("Beer","Item","","Bar",new List<string>(),"",0,"Bartender",15,8,1,true,false,1,true,0,"",false,true,new List<string>(){"Food 1","Stress -5","Virtue -1"},new List<string>(){})},
		{"Wine",new Item("Wine","Item","","Bar",new List<string>(),"",0,"Food",30,15,1,true,false,1,true,0,"",false,true,new List<string>(){"Food 1","Stress -10"},new List<string>(){})},
		{"Embroidered Armor",new Item("Embroidered Armor","Item","","Market",new List<string>(),"",0,"Sew Clothes",390,195,1,false,false,1,false,0,"Body",false,true,new List<string>(){"Combat 10","Combat 30","Decorum 30"},new List<string>(){"Item|Plate Armor|1","Item|Thread|5"})},
		{"Potion of Strength",new Item("Potion of Strength","Item","","Alchemist",new List<string>(),"",4,"Brew Potion",100,50,1,true,false,1,true,0,"",false,true,new List<string>(){"Strength 50","Combat 50"},new List<string>(){})},
		{"Ursus",new Item("Ursus","Item","","",new List<string>(),"",4,"Change Form",-1,-1,1,false,false,1,false,0,"",false,false,new List<string>(){"Strength 100","Reason -50","Stealth -50"},new List<string>(){})},
		{"Homine",new Item("Homine","Item","","",new List<string>(),"",4,"Change Form",-1,-1,1,false,false,1,false,0,"",false,false,new List<string>(){},new List<string>(){})},
		{"Vulpes",new Item("Vulpes","Item","","",new List<string>(),"",4,"Change Form",-1,-1,1,false,false,1,false,0,"",false,false,new List<string>(){"Agility 50","Wit 50","Strength -50"},new List<string>(){})},

		{"Wood",new Item("Wood","Item","","",new List<string>(),"",0,"Crafting",-1,-1,1,false,true,1,false,0,"",false,false,new List<string>(){},new List<string>(){})},
		{"Thread",new Item("Thread","Item","","Tailor",new List<string>(),"",0,"Crafting",10,5,1,false,true,1,false,0,"",false,true,new List<string>(){},new List<string>(){})},
		{"Ingot",new Item("Ingot","Item","","Smith",new List<string>(),"",0,"Crafting",30,15,1,false,true,1,false,0,"",false,true,new List<string>(){},new List<string>(){})},
		{"Discount",new Item("Discount","Status","","",new List<string>(),"",0,"Status",10,5,1,false,false,1,false,0,"",false,true,new List<string>(){"DiscountMarket 10"},new List<string>(){})},
		{"Jogger",new Item("Jogger","Status","","",new List<string>(),"",0,"Status",-1,-1,1,false,false,1,false,0,"",false,false,new List<string>(){"Speed 0.5"},new List<string>(){})},
		{"Boots",new Item("Boots","Item","","Market",new List<string>(),"",0,"Craft Equipment",100,50,1,false,false,1,false,0,"Feet",false,true,new List<string>(){"Speed 0.5"},new List<string>(){"Item|Ingot|2"})},
		{"Horse",new Item("Horse","Item","","Market",new List<string>(),"",0,"Pet",500,250,1,false,false,1,false,0,"Mount",false,true,new List<string>(){"MaxSpeed 2"},new List<string>(){})},
		{"Sword",new Item("Sword","Item","","Market",new List<string>(),"",0,"Gear",120,60,1,false,false,1,false,0,"Hands",false,true,new List<string>(){"Combat 20"},new List<string>(){"Item|Ingot|3"})},
		{"Chainmail",new Item("Chainmail","Item","","Smith",new List<string>(),"",0,"Craft Equipment",120,60,1,false,false,1,false,0,"Body",false,true,new List<string>(){"Combat 20","Stealth -10"},new List<string>(){"Item|Ingot|2"})},
		{"Parasol",new Item("Parasol","Item","","Market",new List<string>(),"",0,"Sew Clothes",210,105,1,false,false,1,false,0,"Hands",false,true,new List<string>(){"Decorum 20","Charisma 20"},new List<string>(){"Item|Ingot|2"})},
		{"Melancholia",new Item("Melancholia","Status","","",new List<string>(),"",0,"Status",-1,-1,1,false,false,1,false,0,"",false,false,new List<string>(){"Strength -10","Wit -10"},new List<string>(){})},
		{"Influenza",new Item("Influenza","Status","","",new List<string>(),"",0,"Status",-1,-1,1,false,false,1,false,0,"",false,false,new List<string>(){"Strength -10","Combat -10"},new List<string>(){})},
		{"Plague",new Item("Plague","Status","","",new List<string>(),"",0,"Status",-1,-1,1,false,false,1,false,0,"",false,false,new List<string>(){"Strength -10","Combat -10"},new List<string>(){})},
		{"Tough",new Item("Tough","Status","","",new List<string>(),"",0,"Status",-1,-1,1,false,false,1,false,0,"",true,false,new List<string>(){"Stress -7"},new List<string>(){})},





		{"Beautiful Hair",new Item("Beautiful Hair","Status","","",new List<string>(),"",0,"Status",10,5,1,false,false,1,false,0,"",false,true,new List<string>(){"Charisma 30","Decorum 30"},new List<string>(){})},
		{"Wizards Robe",new Item("Wizards Robe","Item","","Market",new List<string>(),"",0,"Gear",100,50,1,false,false,1,false,0,"Body",false,true,new List<string>(){"Ritual 10"})},
		{"Sexy Dress",new Item("Sexy Dress","Item","","Market",new List<string>(),"",0,"Gear",100,50,1,false,false,1,false,0,"Body",false,true,new List<string>(){"Charisma 10","Decorum -5"})},


		{"Cat",new Item("Cat","Item","","Market",new List<string>(),"",0,"Pet",100,50,1,false,false,1,false,0,"Pet",true,true,new List<string>(){"Stress -4","Agility 1"},new List<string>(){})},
		{"Dog",new Item("Dog","Item","","Market",new List<string>(),"",0,"Pet",100,50,1,false,false,1,false,0,"Pet",true,true,new List<string>(){"Stress -4","Wit 1"},new List<string>(){})},
	};
}



