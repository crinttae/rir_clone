﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class View_Stats : MonoBehaviour {
    GameObject player;
    PlayerVariables PlayerVariables;

    List<Text> NumberText = new List<Text>() ;
    List<Text> NameText = new List<Text>();

	public Transform ReputationTransform;
	List<Text> ReputationText = new List<Text>() ;


    [SerializeField] Text JobNameText ;
    [SerializeField] Text JobNumberText;
    [SerializeField] Text HomeNameText;
    [SerializeField] Text HomeNumberText;

    Color defaultcolor = Color.gray;

    //really needs to be a refresh.. so just go straight down the list...
	public void refresh_stats()
    {
		PlayerVariables = GetComponentInParent<PlayerVariables>();
		//okay this is clearly broken.
		//shouldn't have to call this here...
		//something is calling  before it should...


        for (var i = 0; i < NumberText.Count ;i++)
        {

            Stat stat = PlayerVariables.Stats[i];

            NumberText[i].text = stat.Actual.ToString(); 

            if (stat.Actual == stat.Base) NumberText[i].color = defaultcolor; 
            else if (stat.Actual < stat.Base) NumberText[i].color = Color.red;
            else if (stat.Actual > stat.Base) NumberText[i].color = Color.cyan;
        }
        JobNameText.text = PlayerVariables.Job.card_name;
        JobNumberText.text = PlayerVariables.Job.gold.ToString();

		for (var i = 0; i < ReputationText.Count; i++) 
		{
			string text = "";
			text = PlayerVariables.get_reputation_description(PlayerVariables.Reputations[i]);   

			if (text == "") {
				ReputationText [i].transform.parent.gameObject.SetActive (false);
			} else {
				ReputationText [i].transform.parent.gameObject.SetActive (true);
				ReputationText [i].text = text;
			}

		//
		}


        //HomeNameText.text = PlayerVariables.Job.card_name;
        //HomeNumberText.text = PlayerVariables.Job.gold.ToString();
    }

	//should probably somewhere have a list of reputation names...




    // Use this for initialization
    void Start () {
        //player = transform.parent.gameObject;
        PlayerVariables = GetComponentInParent<PlayerVariables>();

        for (var i = 0; i < transform.childCount; i++)
        {
            NameText.Add(transform.GetChild(i).FindChild("Name").GetComponent<Text>());
            NumberText.Add(transform.GetChild(i).FindChild("Number").GetComponent<Text>());
            NameText[i].text = " " + PlayerVariables.Stats[i].Name;
        }
		for (var i = 0; i < ReputationTransform.childCount; i++) 
		{
			ReputationText.Add(ReputationTransform.GetChild(i).FindChild("Name").GetComponent<Text>());
		}

        refresh_stats();


    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
