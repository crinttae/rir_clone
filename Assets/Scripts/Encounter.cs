﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class Encounter :MonoBehaviour {


//	public Encounter(){
//
//
//	}




	//initiate battlers?
	//context
	//time based system?  you'd have a timer for each combatant that follows a general battle timer... which counts down till their next move...
	//or possibly just a timer and each combatant moves up it.. 

	public float time=0;  
	//do I want to do a speed based system? 
	//lets just use a very simplified time for now... 
	//eg. player 1 moves on time=1, player 2 moves on time=2.  player 3 moves on time=3... player 1 moves on time=4...



	public Encounter_State initial_state;
	public Encounter_State state;
	public List <Participant> participants;

	public List <Participant> Encounterviews;
	//??do we actually want the views here or attached to the participant??

	public Participant current_participant;
	public UIText encounter_text;


	//how woudl the ai work???
	//if it was smart... it would loop through all the possible actions and all the possible targets..
	//then assign some kind of value to each one...?

	//how do we know who is actually in the encounter?

	//appropriateness..
	//during 'social combat' some things are considered inappropriate?
	//eg. punching someone in the middle of court?

	void Awake(){

		participants = new List<Participant>();
	}

	[SerializeField] TMPro.TextMeshProUGUI description_text;

	Text header_text;
	bool choosing_target=false;

	public void player_clicks_on_action(Text action_text){

		if( current_participant.player_clicks_on_action(action_text)) 
			print(action_text +" pushed");
	}

	public void player_clicks_on_target(Text target_text){
		print(target_text + " pushed");
		current_participant.player_clicks_on_target(target_text) ;
	}


	public void Initiate_Encounter()
	{
		

		for (var i =0;i<participants.Count;i++)
		{
			participants [i].time_till_next_turn = i;


		}

		pass_time ();
	}


	void set_state(Encounter_State state)
	{
		this.state = state;
	}



	void initiate_choose_target()
	{
		encounter_text.DisplayText("Choose Target");
		choosing_target = true;	
	}



	public void pass_time()
	{
		while (!check_for_next_participant ()  && !is_battle_over())
			time += 1.0f;
	}
	bool check_for_next_participant()
	{
		foreach (Participant participant in participants)
			if (time <= participant.time_till_next_turn) {
				current_participant = participant;
				encounter_text.DisplayText("It is "+current_participant.unique_name+ "'s turn.");
				participant.take_turn();
				return true;
			}	
		return false;
	}







	bool is_battle_over()
	{  
		bool lose = false;
		bool win=true;

		List<Participant> winners = new List<Participant> ();
		//assuming a multiplayer encounter, for whatever dumb reason:
		//if a player has no more enemies then the encouter is over for that player.
		//if anyone dies, then the encounter is over for that player....
		//if there are still players who still have enemies then the encounter continues?

		//(this all supposes enemies btw, it may be an encounter features an opportunity... in which case it needs to be ended in some other way... 
		foreach (var participant in participants) {
			if (participant.is_player)
			{
			if (participant.hp.Actual == 0) {
				lose = true;
				end (false,true,new List<Participant>(){participant});
				return true;
			}

			else foreach(var other_participant in participants)
			{
				if(participant.is_my_enemy(other_participant) && other_participant.hp.Actual>0 )
				win=false;
				break;
			}
			if (win) {
				winners.Add (participant);
				end (true,false, winners);
				return true;
			}
			}

		}
	return false;
	}



//	IEnumerator QuitText()
//	{
//		yield return new WaitForSeconds(2f);
//		description_text.gameObject.SetActive(false);
//	}





	public void end(bool win = false, bool lose=false, List<Participant> players = default(List<Participant>) )
	{
		
		if (win) {
			foreach (var winner in players)
			{
				if (winner.is_player) {
					winner.pvar.give_gold (5);
					winner.pvar.Quit_Encounter ();
				}
			}
		}
		else 
			if (lose) {
				foreach (var loser in players)
				{
					if (loser.is_player) {
						loser.pvar.give_stress (5);
					loser.pvar.Quit_Encounter ();
					}
				}
			}


		//somehow quit?



	}




}



















//either i make a lsit-- friend, enemy, orc, etc.  and check each potential target to see if it aligns with one in the list..
//or i makea  list of permutations.. any, all, friend, enemy... etc.  
public enum Act_On {ally, any, enemy, allies , enemies, all, self, player, npc, none, others};
public enum Encounter_State {precombat, combat, low_social, high_social};

					public static class Status_EffectDictionary {
	public static Dictionary<string, Status_Effect> Status_Effects = new Dictionary<string, Status_Effect>
	{
		{"Poison", new Status_Effect("Poison","has been poisoned")}

	};
}

public class Status_Effect{
	string status_name;
	string description_predicate;

	public Status_Effect (string new_status_name,string newdescription_predicate)
	{
		status_name = new_status_name;
		description_predicate = newdescription_predicate;
	}
}

public class Act {
	//public enum valid_target{friend, enemy, all}

	public Act_On valid_target;
	public List<Encounter_State> valid_states;
	public float AI_standard_value;
	public string name="";
	public string description="";


	public Act(string newname, Act_On newvalid_target, List<Encounter_State> newvalid_states)
	{
		name = newname;
		valid_target = newvalid_target;
		valid_states = newvalid_states;
	}

	string get_description()
	{
		return (name + " (" + valid_target.ToString () + ") :" + description);
	}
	float get_value(Encounter_State state, Participant participant)
	{




		return 0;
	}
	public bool do_act(Participant doer, List<Participant> targets)
	{
		foreach (Participant target in targets) {
			do_act (doer, target);
		}
		return true;
	}
	public bool do_act(Participant doer, Participant target)
	{
		//what if the act does multiple things.. then probbably want a string? similar to the and values in action...


//		print ("Doing: " + action_string);
//		string[] parts = action_string.Split ('|');
//		uitext.add_to_current (action_string);
//		if (parts [0] == "And") {
//			string[] multiactions = action_string.Substring (parts[0].Length+1).Split ('&');
//			foreach (string action_part in multiactions) {
//				DoActionString (action_part , view);
//			}
//			return;
//		}			



		return true;
	}
		
	//public void 
	//foreach (Stat stat in Stats)
	//{
	//	if (stat.Name == stat_name) return stat;
	//}

	//coeficient 1, stat1, coefficient 2, stat2, + constant
	public bool DoActString(Participant doer, Participant target, string act_string)
	{
		Debug.Log("Doing: " + act_string);
		string[] parts = act_string.Split ('|');


		if (parts [0] == "And") {
		//And|Status_Effect|Poison&Damage|HP|1|Combat|-1|Defense|0
			string[] multiactions = act_string.Substring (parts[0].Length+1).Split ('&');
			foreach (string action_part in multiactions) {
				DoActString (doer, target, action_part);
			}
			return true;
		}

		else if (parts [0] == "Damage") {
			//Damage|HP|Combat|1.5|Agility|-1|0
			//do damage to opponents hp = (1.5 * attackers combat) - 1* (target's agility) + 0
			//Damage|HP|AVG,Combat,Strength,Magic|1|Combat|-1|Agility|0
			float damage = 0;
			Stat stat_to_change = get_stat (parts [1], target);

			damage = calculate_effect (float.Parse (parts [2]), parse_stat_effect (parts [3],doer), float.Parse (parts [4]), parse_stat_effect (parts [5],target), float.Parse (parts [6]));
			damage *= -1;
			if (damage > 0)
				damage = 0;
			
			stat_to_change.change (damage);

			target.encounter.encounter_text.DisplayText (target.unique_name + " loses " + (-damage) + " " + stat_to_change.Name);
		}
		else if (parts [0] == "Heal") {
			float healing = 0;
			Stat stat_to_change = get_stat (parts [1], target);

			healing = calculate_effect (float.Parse (parts [2]), parse_stat_effect (parts [3],doer), float.Parse (parts [4]), parse_stat_effect (parts [5],target), float.Parse (parts [6]));
			if (healing < 0)
				healing = 0;
			
			stat_to_change.change (healing);
			target.encounter.encounter_text.DisplayText (target.unique_name + " gains " + healing + " " + stat_to_change.Name);

		} 
		else if (parts [0] == "Status_Effect") {
			target.status_effects.Add(parts[1]);
			//this isn't actually going to work with enums.. will have to make status effects strings I guess?
			target.encounter.encounter_text.DisplayText (target.unique_name + " is now affected by " +  parts[1]);

		}
		else if (parts [0] == "End_Encounter") {
			target.encounter.end();

		}
		return true;
	}

	public Stat get_stat(string stat_string, Participant participant)
	{
		foreach (Stat stat in participant.Stats)
		{
			if (stat.Name == stat_string) return stat;
		}
		return null;

	}

	public float parse_stat_effect (string stat_string, Participant participant)
	{
		if(stat_string.Substring(0,3)=="AVG")
		{
			float running_total = 0;
			string[] multistats = stat_string.Substring (4).Split (',');
			foreach (string stat_name in multistats) {
				running_total += get_stat (stat_name,participant).Actual;
			}
			return running_total / multistats.Length;
		}

		else return get_stat (stat_string, participant).Actual;

		return -999;
	}


	/// <summary>
	/// calculates C1*S1 +C2*S2 + C3
	/// </summary>
	/// <param name="">.</param>
	/// <param name="stat_value1">Stat value1.</param>
	/// <param name="coefficient2">Coefficient2.</param>
	/// <param name="stat_value2">Stat value2.</param>
	public float calculate_effect(float C1, float S1, float C2, float S2, float C3)
	{
		return ( C1*S1 + C2*S2 + C3);
	}



}


public static class ActDictionary
{
	public static Dictionary<string, Act> Acts = new Dictionary<string, Act>
	{
		{"Attack", new Act("Attack" , Act_On.enemy,  new List<Encounter_State>() {Encounter_State.combat,Encounter_State.low_social,Encounter_State.precombat})},
		{"Surprise Attack", new Act("Attack",Act_On.enemy, new List<Encounter_State>() {Encounter_State.precombat})},
		{"Defend", new Act("Defend",Act_On.self,new List<Encounter_State>() {Encounter_State.combat})},
		{"Run", new Act("Run",Act_On.self, new List<Encounter_State>(){Encounter_State.combat,Encounter_State.low_social,Encounter_State.high_social,Encounter_State.precombat})},
		{"Insult", new Act("Insult",Act_On.any, new List<Encounter_State>(){Encounter_State.combat,Encounter_State.low_social,Encounter_State.high_social,Encounter_State.precombat})},
		{"Challenge", new Act("Insult",Act_On.any, new List<Encounter_State>(){Encounter_State.high_social})},
		{"Ignore", new Act("Insult",Act_On.any, new List<Encounter_State>(){Encounter_State.low_social,Encounter_State.high_social})},
	};


}


public static class CharacterDictionary{
	public static Dictionary<string, Character> NPCs = new Dictionary<string, Character> {
		{ "Orc", new Character ("Orc", 20, 20,5) },
		{ "Drunk", new Character ("Drunk", 20, 20,5) }

	};
}



public class Character : MonoBehaviour
{
	//these are standard? npcs... so not like a king, but like a drunk..??
	//they need stats... 
	//could these inherit from participant???
	public string character_name ="";
	[SerializeField] public List<Stat> Stats = new List<Stat>(){};
	[SerializeField] public Stat hp = new Stat("hp");
	[SerializeField] public Stat mp = new Stat("mp");
	public List<string> unlocked_skills = new List<string>();
	float default_stat;
	public int gold_drop=2;


	public Character(string newcharacter_name,float newmp,float newhp, float default_stat)
	{
		character_name = newcharacter_name;
		mp.Base = newmp;
		hp.Base = newhp;

		Stats = new List<Stat> () {
			new Stat ("Strength"),
			new Stat ("Agility"),
			new Stat ("Reason"),
			new Stat ("Spirit"),
			new Stat ("Charisma"),
			new Stat ("Combat"),
			new Stat ("Decorum"),
			new Stat ("Wit"),
			new Stat ("Science"),
			new Stat ("Stealth"),
			new Stat ("Cooking"),
			new Stat ("Crafting"),
			new Stat ("Music"),
			new Stat ("Ritual"),
			new Stat ("Nature"),
		};

		foreach (Stat stat in Stats) {
			stat.Base = default_stat;
		}
	
	}



	public Character CopyCharacter(Character original)
	{
		Character new_character  = new Character(original.character_name, original.mp.Base ,original.hp.Base, original.default_stat );

		new_character.character_name = original.character_name;
		new_character.mp.Base = original.mp.Base;
		new_character.hp.Base = original.hp.Base;
		new_character.mp.Actual = original.mp.Actual;
		new_character.hp.Actual = original.hp.Actual;

		for(var i=0; i<original.Stats.Count ;i++)
		{
			new_character.Stats[i].Base = original.Stats[i].Base;
			new_character.Stats[i].Actual = original.Stats[i].Actual;
		}
		return new_character;
	}




//
//	void Awake()
//	{
//		Stats = new List<Stat> () {
//			new Stat ("Strength"),
//			new Stat ("Agility"),
//			new Stat ("Reason"),
//			new Stat ("Spirit"),
//			new Stat ("Charisma"),
//			new Stat ("Combat"),
//			new Stat ("Decorum"),
//			new Stat ("Wit"),
//			new Stat ("Science"),
//			new Stat ("Stealth"),
//			new Stat ("Cooking"),
//			new Stat ("Crafting"),
//			new Stat ("Music"),
//			new Stat ("Ritual"),
//			new Stat ("Nature"),
//		};
//
//		foreach (Stat stat in Stats) {
//			stat.Base = default_stat;
//		}
//	}
//


	public Character Clone()
	{
		return (Character)this.MemberwiseClone();
	}



}
 


public class AI_Considieration {
	public Act act;
	public List <Participant>  selected_targets = new List<Participant>();
	public float AI_Value;
	//okay so theoretically.. lets just make the AI go through all the things it can do to all the targets?
	//or at least up to some reasonable limit...?
	//and store them?  then look through the list and do the one with the most value..????
	//will that take too long?
	//we don't actually need to store them at all.. but we do need a sort of long iff statement where we decide against doing things like poisoning the already poisoned character...

	//to calculate value.. we need to know if it's a status effect???  if someone doesn't need healing???  etc.


}








