﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public static class Logger
{
	public static bool log_evrything= true;

	public static void log(object message)
	{
		if (log_evrything)
			Debug.Log (message);
	}

}


public static class DataManager
{
    


	public static bool testing = true;
	public static int testing_job_mastery = 5;
	public static int testing_starting_gold = 1000;


	public static int default_job_mastery = 15;
    public static int starting_gold = 100;
    public static string base_home = "Alley";
	public static string base_job = "Beggar";

	public static float base_job_probability=.5f;
	public static float home_move_in_factor = 1.5f;


    public static GameObject[] locations;
    public static GameObject[] players;
    public static GameObject Current_Canvas;
    public static bool log_all = false;
    
	public static Sprite[] sprites;
	public static string[] sprite_names;

    public static bool game_playing=false;
    public static bool loading = false;
	public static bool initialized= false;
	public static List<string> card_name_list = new List<string>();

    static char delimiter = '|';
    static char delimiter2 = '/';
 
    static string autosave_name = "autosave.txt";
    //we should know how many players for non loaded games....

    static public int number_of_players=1;
    public static string current_save_slot = "autosave.txt";
    //thish shoul dbe a list shouldn't.. one that you can move through.. but lets forget abotu that for now... no need.


	public static List<Dictionary<string,Card>> Dictionaries = new List<Dictionary<string,Card>>();



	public static void initialize()
	{
		if (testing) {
			default_job_mastery = testing_job_mastery;
			starting_gold = testing_starting_gold;
		}
		
		#region load_sprites
		sprites = Resources.LoadAll<Sprite>("Textures");
		sprite_names = new string[sprites.Length];



		for (int i = 0; i < sprite_names.Length; i++)
		{
			sprite_names[i] = sprites[i].name;
			// print(sprites[i].name);
		}
		#endregion
		initialized = true;


	}




	public static Card get_dictionary_card(string card_name,string type)
	{
		if (type == "Item") {
			Item item;
			if (Item_Dictionary.Items.TryGetValue (card_name, out item))
				return item as Card;
		} else if (type == "Job") {
			Job job;
			if (Job_Dictionary.Jobs.TryGetValue (card_name, out job))
				return job as Card;
		} else if (type == "Action") {
			Action action;
			if (Action_Dictionary.Actions.TryGetValue (card_name, out action))
				return action as Card;
		} else
			Debug.LogError ("trying to pull " + card_name + " from teh dictionary but " + type + " is not a type");
		Debug.LogError (card_name + " not found ");
		return null;
	}


	public static Card get_dictionary_card(string card_name)
	{
		Item item;
		if (Item_Dictionary.Items.TryGetValue(card_name, out item )) return item as Card;
		Job job;
		if (Job_Dictionary.Jobs.TryGetValue(card_name, out job)) return job as Card;
		Action action;
		if (Action_Dictionary.Actions.TryGetValue(card_name, out action)) return action as Card;
		Debug.LogError (card_name + " not found ");
		return null;
	}

	public static void build_dictionary_list()
	{
		foreach (string card_name in Item_Dictionary.Items.Keys)
			card_name_list.Add (card_name);
		
		foreach (string card_name in Job_Dictionary.Jobs.Keys)
			card_name_list.Add (card_name);
		
		foreach (string card_name in Action_Dictionary.Actions.Keys)
			card_name_list.Add (card_name);
	}


    public static void Reset()
    {
        SceneManager.LoadSceneAsync(1);
    }
    //load would set loading to true, choose which to hit, then reset...




    public static void Switch_Save_Slot(string i)
    {
        Debug.Log("switching to slot " + i);
        current_save_slot = "save" + i + ".txt";
        if (i=="0") current_save_slot = autosave_name;
    }
    

    static void save_inventory(Inventory inventory)
    {
        string tag = inventory.gameObject.transform.name;
        if (inventory.player_inventory)
        {
            tag = inventory.gameObject.GetComponent<PlayerVariables>().player_number.ToString() + tag;
        }
        ES2.Delete(current_save_slot + "?tag=" + tag);

        string save_string = "";

        foreach (Card card in inventory.cards)
        {
            save_string += card.card_name + delimiter;
            if (card.type == "Item")
            {
                Item item = card as Item;
                save_string += delimiter2 + item.stacks;
            }
        }
        ES2.Save<string>(save_string, current_save_slot + "?tag=" + tag);
    }






    public static void Load_Game()
    {
        //probably need a way to wipe everything first. so like load scence or reset scene or something...
        //string file_name = current_save_slot;
        loading = true;
        Reset();
    }



	public static void Save_Game(int slot=-1, bool autosave=false)
    {
        float SaveStartTime = Time.realtimeSinceStartup;

        string save_string ="";

        string file_name = current_save_slot;
        if (autosave) file_name = autosave_name;


        
        ES2.Delete(file_name);

        
        ES2.Save<int>(players.Length, file_name + "?tag=" + "player_count");
        ES2.Save<int>(TurnManager.active_player_number, file_name + "?tag=" + "current_player");
        ES2.Save<int>(TurnManager.week, file_name + "?tag=" + "week");
        foreach (GameObject player in players)
        {
            PlayerVariables pvar = player.GetComponent<PlayerVariables>();
            int player_number = pvar.player_number;
            string Ptag = file_name+ "?tag=P" + player_number ;
            ES2.Save<string>(pvar.name,  Ptag + "name");
            ES2.Save<float>(pvar.time,  Ptag + "time");
            ES2.Save<int>(pvar.gold,  Ptag + "gold");
            ES2.Save<int>(pvar.stress,  Ptag + "stress");
            ES2.Save<int>(pvar.food, Ptag + "food");
            ES2.Save<float>(pvar.virtue, Ptag + "virtue");
            ES2.Save<int>(pvar.Salary, Ptag + "Salary");
            ES2.Save<string>(pvar.location.transform.name, Ptag + "location");
            ES2.Save<string>(pvar.Home.card_name, Ptag + "Home");
            string job_name = pvar.Job.card_name;
            ES2.Save<string>(job_name, Ptag + "Job");
            ES2.Save<string>(pvar.Friend, Ptag + "Friend");

            ES2.Save<Vector2>(player.GetComponent<Controller>().player_sprite.transform.position, Ptag + "position");
        


            save_string = "";
            foreach (Stat stat in pvar.Stats)
            {
                ES2.Save<float>(stat.Actual, Ptag +stat.Name);
            }
         


            Inventory inventory = player.GetComponent<Inventory>();
            save_string = "";
            foreach (Card card in inventory.cards)
            {
                save_string += card.card_name + delimiter;
                if (card.type == "Item")
                {
                    Item item = card as Item;
                    save_string += delimiter2 + item.stacks;
                }
            }
            ES2.Save<string>(save_string, Ptag + "Inventory");


             player_number++;
        }

        Debug.Log("saving game with " + players.Length + "players in " + (Time.realtimeSinceStartup -SaveStartTime ) + "seconds");
   
	
	


		//update on escape close whatever menus are open.
		//on click send the player to the next place...


	
	
	
	}





    //    using(ES2Writer writer = ES2Writer.Create("myFile.txt"))
    //{
    //    // Write our data to the file.
    //    writer.Write(this.name, "nameTag");
    //writer.Write(this.transform, "transformTag");
    //writer.Write(new int[]{1,2,3},"intArrayTag");
    //    // Remember to save when we're done.
    //    writer.Save();
    //}
    //using(ES2Reader reader = ES2Reader.Create("myFile.txt"))
    //{
    //    // Read data from the file in any order.
    //    reader.Read<Transform>("transformTag", this.transform);
    //    this.name = reader.Read<string>("nameTag");
    //    int[]
    //myIntArray = reader.ReadArray<int>("intArrayTag");
    //}
    //using(ES2Writer writer = ES2Writer.Create("myFile.txt"))
    //{
    //    // Write our data to the file in the order we are going to read it.
    //    writer.Write(this.name);
    //writer.Write(this.transform);
    //writer.Write(new int[]{1,2,3});
    //    // Remember to save when we're done.
    //    writer.Save(false);
    //}
    //Unless you are using the ES2Reader as part of a Using statement, you must call the reader’s Dispose() method when you are finished with it.

//using(ES2Reader reader= ES2Reader.Create("myFile.txt"))
//    {
//this.blahblahblah= reader.Read<string>();
//    }

  



	//so currently operating off of a list of sprite names... would be nice if these were enums instead..
	//or were directly connected to the transforms with prefabs...
	public static Sprite get_sprite_from_name (string sprite_name)
	{
		if (initialized == false)
			initialize();

		Sprite selected_sprite = null;

		for (int i = 0; i < sprite_names.Length; i++)
		{
			if (sprites[i].name == sprite_name)
			{
				selected_sprite = sprites[i];
				break;
			}
		}

		if (selected_sprite == null)
		{ 
			if(DataManager.log_all) Debug.Log("no sprite found! for " + sprite_name);

		}
		return selected_sprite;
	}
    



}


