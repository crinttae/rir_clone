﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//this should get claled from begin game. and it hsould LOAD the turn.  and if there is no turn then it's zero...

public static class TurnManager {
    public static int week = 0;

    public static GameObject[] players;
    public static int active_player_number=0;
    public static GameObject active_player;

    public static bool simultaneous_turns = false;
   // public static float time_per_week = 12;

    public static void Initiate()
    {
        //if not loading...  if loading we need to figure out whose turn it is and what time it is and all that stuff...
        
        

        start_week();
    }


    public static void end_turn()
    {
        //if turn based, whcih we should probably jsut assume.................
        //go to next player... until all the players have gone, then set time at 0 for all players.
        //filler bar to the left that gets gradually eaten up by moving... and doing...

		PlayerVariables pvar = active_player.GetComponent<PlayerVariables> ();
        //need some kidn of filler.. like moving the player ot the next place... exiting menus... sending them home...
        active_player.GetComponent<CardImageManager>().show_screen(CardImageManager.ScreenName.MapScreen);
		pvar.give_time(-20);


		active_player.GetComponent<Controller>().MoveTo(GameObject.Find(pvar.Home.location));
        //i might need to deactive it after it gets there... or something... 

        //not actully getting there in time to set location???

        //where it's clearing view..
        //should wait until the player actualy gets there though...
        active_player.SetActive(false);
        if (active_player_number < players.Length-1)
        {
            active_player_number++;
            start_turn();
        }
        else
        {
            active_player_number = 0;

            end_week();
        }
        //close all views...
        //finish all work..
        //make sure the player is somewhere real.

    }

    public static void end_week()
    {
        week++;
        start_week();
    }

    public static void start_week()
    {
       // DataManager.Save_Game(-1, true);
        start_turn();

    }

    //start turn:  disable things belonging to the active player...
    public static void start_turn()
    {
		//refresh the map.
		//make all items with turns in the player's equpiment go down 1
        active_player = players[active_player_number];
        active_player.SetActive(true);
        PlayerVariables pvar = active_player.GetComponent<PlayerVariables>();


		if (pvar.Home.gold < 0) {
			if (pvar.weeks_till_rent == 0) {
				bool paid_rent = pvar.pay_rent ();
				if (paid_rent) {
					pvar.uitext.DisplayText (pvar.player_name + " paid " + pvar.Home.gold + " for the monthly rent.");
				} else {
					pvar.changejob (pvar.Home, true);
					pvar.uitext.DisplayText ("You didn't have enough for rent!");
				}
			} else {
				if (pvar.weeks_till_rent > 0)
					pvar.weeks_till_rent--;
				if (pvar.weeks_till_rent == 0)
					pvar.uitext.DisplayText ("Your rent will be paid next week.");
			}
		}



		List <Card> cards_to_delete = new List <Card>();
		foreach (KeyValuePair<string, Item> entry in pvar.Equipment)
		{
			Item equipped_item = entry.Value;
			if (equipped_item != null)
			{ 
				if (equipped_item.gradual) 
				{
					pvar.conditions.modify_attributes(equipped_item.stat_modifiers, true);
				}
			}
		}



		foreach (GameObject location in DataManager.locations) {
			var inventory = location.GetComponent<Inventory> ();
			foreach (Card card in inventory.cards) {
				if (card.type == "Action") {
					Action action = card as Action;
					action.count_this_week = 0;
				}
			}
		}


		//create a list of cards to destroy?
		foreach (Card inventory_card in pvar.inventory.cards)
		{
			Item inventory_item = inventory_card as Item;
			if (inventory_item.type == "Status" && inventory_item.gradual) 
			{
				pvar.conditions.modify_attributes(inventory_item.stat_modifiers, true);
			}
			if (inventory_item.use_by>0)
			{
				if (inventory_item.use_by == 1) {
					cards_to_delete.Add (inventory_card);
				} else
					inventory_item.use_by -= 1;
			}
		}
		pvar.delete_cards (cards_to_delete);

		pvar.Cards_Played_This_Turn.Clear();
		pvar.refresh_map(true);

		pvar.turn_start();


    }









}
