﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class CardUIImage : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler{
    public Image pic;
    public Text header;
	public Text description;
	public TextMeshProUGUI textmeshprougui;
	CardImageManager card_image_manager;
	public GameObject invisbutton;
	public bool disappear_on_own = true;

	Sprite new_sprite;
	string new_header_string;
	string new_description_string="";
	CardImageManager delayed_card_image_manager;
	public int DescriptionImageNumber;


	void Awake()
	{
		card_image_manager = gameObject.GetComponentInParent<CardImageManager> ();

	}


	public void invis_button_click()
	{
		//this only sort of works bcause the button is not upfront?
		card_image_manager.description_button_click (DescriptionImageNumber);
	}


	public void Stop_description_coroutine()
	{
		StopCoroutine ("Draw_delayed_description");
	}


	//argh can't start these delays with this being inacive.. could maybe run it elsewhere?
	public void Set_delayed_description(CardImageManager newcard_image_manager, Sprite sprite, string header_string, string description_string="")
	{
		
		Stop_description_coroutine();
		new_sprite = sprite;
		new_header_string = header_string;
		new_description_string = description_string;
		delayed_card_image_manager = newcard_image_manager;
		StartCoroutine ("Draw_delayed_description");
	}


	IEnumerator Draw_delayed_description()
	{
		yield return new WaitForSeconds(.4f);
		initializeBig (delayed_card_image_manager, new_sprite, new_header_string, new_description_string);
	}
	//finally I need a way of stopping it?



    public virtual void initializeBig(CardImageManager new_card_image_manager, Sprite sprite, string header_string, string description_string="",bool disappear_automatically=true)
    {
		
		disappear_on_own = disappear_automatically;




		this.card_image_manager = new_card_image_manager;

        try
        {
            pic.sprite = (sprite);
        }
        catch
        {
            print("sprite not found!");
        }

        header.text = header_string;

        if (description_string != "")
        {
			textmeshprougui.text=create_hyperlinks(description_string);
        }
    }



	public void OnPointerEnter(PointerEventData eventData)
	{
		if (this.transform.GetChild(1).gameObject.activeSelf == false) return;

		if (Input.touches.Length > 0)
		{
			//HoverStartTime = Time.unscaledTime;
			//TouchHover = true;
		}

		card_image_manager.Keep_all_previous_cards (DescriptionImageNumber);
//		card_image_manager.View_Description(pic.sprite, header.text, description.text);

		//this resets the descpription code... 
		//so saying we exit the third card and enter the second...
		//when we exit the third card, all are set to disappear...
		//but entering the second card tells the 2nd and first card to stay.
		//so all we really need to know is which card we're on... which we shoudl know anyway since we need to create a new spot.
		//and ideally we need a code that cancels the cancelling
	}

	public void View_additional_card(string card_name)
	{
		if (DescriptionImageNumber == 3)
			return;
		
		card_image_manager.View_Description (card_name, DescriptionImageNumber+1);

	}


	public void OnPointerExit(PointerEventData eventData)
	{
		//this cancels the description code...
		//somehow here we need to know that it's being kept for all the previous iterations as well..

		//InvalidateClick = false;
		//TouchHover = false;

		//view.Card_UI_Transform.gameObject.SetActive(false);
		if(disappear_on_own) 
		card_image_manager.Cancel_Description_Maybe();
		//there should be some lag...here
	}



	public string create_hyperlinks(string description_base)
	{
		//liekly need to do more than just split by ' '..
		//probably need to eliminate the pagebreaks as well...
		//or replace them with spaces.
		string[] words = description_base.Split(' ');
		foreach (string word in words) {
			foreach (string card_name in DataManager.card_name_list) {				
				//sorted list woudl be faster...
				if (word == card_name) {
					print ("found a card " + card_name);
				//YIKES.
					//hypertext is definitely as serious probelm here..
				}
			}
		}
		return description_base;
	}





//    public virtual void initialize(Sprite sprite, string header_string, string description_string = "")
//    {
//        try
//        {
//            pic.sprite = (sprite);
//        }
//        catch
//        {
//            print("sprite not found!");
//        }
//
//        header.text = header_string;
//
//        if (description_string != "")
//        {
//            description.text = description_string;
//			textmeshprougui.text=description_string;;
//        }
//
//
//    }

}
