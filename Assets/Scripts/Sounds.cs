﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//should this be static???  theoretically it's going to have to play for individaul players... but lets just forget about that for now?

public class Sounds : MonoBehaviour {
    public AudioSource sound_effects_source;
    public AudioSource music_source;

    public static Sounds instance = null;
    public AudioClip work;
    public float lowPitchRange = .95f;              //The lowest a sound effect will be randomly pitched.
    public float highPitchRange = 1.05f;            //The highest a sound effect will be randomly pitched.

    // Use this for initialization

    public void play_sfx(AudioClip clip)
    {
        //Set the clip of our efxSource audio source to the clip passed in as a parameter.
        //sound_effects_source.clip = clip;

        //Play the clip.
        sound_effects_source.PlayOneShot(clip);
    }
}


