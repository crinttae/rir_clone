﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[System.Serializable]




public class PlayerVariables : MonoBehaviour
{


	#region declarations

	[SerializeField]    public int player_number=0;
	public Encounter encounter;

    public UIText uitext;
    [SerializeField]    View_Stats view_stats;
    [SerializeField]    public Conditions conditions;

    public List<Card> modifiers = new List<Card>();
 

	public string character_name ="Valentine";
	[SerializeField] public List<Stat> Stats = new List<Stat>(){};
	[SerializeField] public Stat hp = new Stat("hp");
	[SerializeField] public Stat mp = new Stat("mp");
	public List<string> unlocked_skills = new List<string>();
	float default_stat;




	[SerializeField]   GameObject encounter_prefab;
	[SerializeField]   GameObject participant_prefab;

	Transform encounter_transform;
	EncounterView encounterview;





    [SerializeField]    public string Friend = "";
	[SerializeField]    public Job Home ;
    [SerializeField]    public Job Job ;
    [SerializeField]    public int Salary = 0;
	[SerializeField]    public string player_name = "Valentine";
    public List<string> Enrolled_Classes= new List<string>();


    [SerializeField]    public float base_walking_speed = 1f;
    [SerializeField]    public float actual_speed = 1f;
	[SerializeField]    public float actual_walking_speed = 1f;


    [SerializeField]    public GameObject location ;
    // [SerializeField]    public GameObject location ;

    [SerializeField]    public int gold=200;
    [SerializeField]    public int stress;
    [SerializeField]    public float time;
    [SerializeField]    public int food;
    [SerializeField]    public float virtue;

    [SerializeField]    public int maxtime=12;
    [SerializeField]    public int maxstress=100;
    [SerializeField]    public int maxgold = 9999;
    [SerializeField]    public int maxfood = 4;
    [SerializeField]    public float maxvirtue=999f;

    [SerializeField]    public int mintime = 0;
    [SerializeField]    public int minstress = 0;
    [SerializeField]    public int mingold = 0;
    [SerializeField]    public int minfood = 0;
    [SerializeField]    public float minvirtue = -999f;

    [SerializeField]    public Slider food_slider;
    [SerializeField]    public Slider stress_slider;
    [SerializeField]    public Slider time_slider;
    [SerializeField]    public Text gold_text;

	[SerializeField]  public Dictionary<string, int> Job_Exp;

	public List<string> stored_actions = default(List<string>);


	public int weeks_till_rent = 0;
    public string base_home;
    public string base_job;
	CardImageManager cardimagemanager;
	public Inventory inventory;

    Color default_color;


	public GameObject action_button_panel;
	public GameObject target_button_panel;


    public List<string> Cards_Played_This_Turn = new List<string>();
    //could just add a 1 number to keep all the cards ever played...2 numbers to know when...  

   
    [SerializeField]    public Dictionary<string, Item> Equipment     ;

	//again this should be by integer... 
	[SerializeField]    public Dictionary<string, float> Discount ;


	Dictionary<int,string[]> Reputation_terms;
    //[SerializeField] public List<Stat> Stats = new List<Stat>(){};
	[SerializeField] public List<Stat> Reputations = new List<Stat>(){};
	#endregion





    private void Awake()
    {

		Discount = new Dictionary<string, float>();

		Equipment = new Dictionary<string, Item> () {
			{ "Hands",null },
			{ "Body",null },
			{ "Head",null },
			{ "Feet",null },
			{ "Mount",null },
			{ "Pet",null },

		};

		stored_actions.Clear ();

        default_color = GameObject.FindGameObjectWithTag("Location").transform.GetChild(1).GetChild(0).GetComponent<Image>().color;
		cardimagemanager = gameObject.GetComponent<CardImageManager> ();
		uitext = cardimagemanager.UIText; 
		inventory = GetComponent<Inventory> ();
			
        refresh_sliders();
        // refresh_map();
		encounter_transform=GameObject.Find("Encounters").transform;
		encounterview = GetComponentInChildren<EncounterView>();



		Stats = new List<Stat>()
        {
        new Stat("Strength"),
        new Stat("Agility"),
        new Stat("Reason"),
        new Stat("Spirit"),
        new Stat("Charisma"),
        new Stat("Combat"),
        new Stat("Decorum"),
        new Stat("Wit"),
        new Stat("Science"),
        new Stat("Stealth"),
        new Stat("Cooking"),
        new Stat("Crafting"),
        new Stat("Music"),
        new Stat("Ritual"),
        new Stat("Nature"),


        };
        
		Reputations = new List<Stat> () {
			new Stat ("Law", 0, 0, -999, 999, true, 1),
			new Stat ("Purity", 0, 0, -999, 999, true, 1),
			new Stat ("Life", 0, 0, -999, 999, true, 1),
			new Stat ("Fame", 0, 0, -999, 999, true, 1)
		};

		Reputation_terms = new Dictionary<int,string[]> () {
			{100,new string[]{"An Avatar of Justice","Pristine","Beloved by The Goddess Herself","Hero to the people."}},
			{50,new string[]{"Defender of Law","Chaste","The Goddess Smiles at You","Loved by the folk."}},
			{25,new string[]{"A Good Citizen","Clean","The Goddess Knows Your Name","Well liked."}},
			//{-25,new string[]{"","","",""}},
			{-50,new string[]{"A Nuisance","Impure","The Lord of Death Whispers","Disliked by the people."}},
			{-100,new string[]{"Wanted","Wanton","The Lord of Death Knows You","Despised by the people."}},
			{-9999,new string[]{"Wanted, Dead or Alive","Your Name is a Dirty Word.","The Lord of Death Loves you","Folks throw rocks at you."}},

		};


		Job_Exp = new Dictionary<string, int>();
        foreach (var job in Job_Dictionary.Jobs)
        {
            Job_Exp.Add(job.Key,0);
        }

		hp.Base = 100;
		mp.Base = 100;


    }


	public bool is_in_encounter(){
		if (encounter == null)
			return false;
		else
			foreach (Participant participant in encounter.participants) {
				if (participant.pvar = this)
					return true;
			}
		return false;
	}
	public void test_encounter()
	{
		Create_Encounter (this, new List<string> (){ "Orc"}, new List<string> (){ "Orc" }, new List<string> (){ "Orc" });

	}
	public void Create_Encounter(PlayerVariables pvar, List<string> Enemy_names, List<string> Ally_names , List<string> Neutral_names)
	{


		GameObject encounter_object =  Instantiate(encounter_prefab, encounter_transform);
		encounter = encounter_object.GetComponent<Encounter> ();
		encounterview.encounter = encounter;



		time = 0;

		encounter.participants.Clear();
		encounter.initial_state = Encounter_State.combat;
		encounter.state = encounter.initial_state;



		List<Participant> Enemies=new List<Participant>(); 
//		List<Participant> Allies; 
//		List<Participant> Neutrals; 



		encounter.time = 0;
		encounter.state = Encounter_State.combat;




		Character dictionary_character;
		Participant new_participant;

		//i don't want to be doing new participant.. i want to be instantiating a participant prefab..
		new_participant = Instantiate(participant_prefab, encounter_object.transform).GetComponent<Participant>();

		new_participant.Initialize (this, encounter);
		encounterview.participant = new_participant;

		encounter.participants.Add(new_participant);
		cardimagemanager.show_screen (CardImageManager.ScreenName.EncounterScreen);



		foreach (string enemy_name in Enemy_names) 
		{


		if (CharacterDictionary.NPCs.TryGetValue (enemy_name, out dictionary_character)) 
			{
			//Character  Character= dictionary_character.Clone();
				Participant enemy_participant = Instantiate(participant_prefab, encounter_object.transform).GetComponent<Participant>();
				enemy_participant.Initialize (dictionary_character,encounter);
				Enemies.Add (enemy_participant);
				encounter.participants.Add(enemy_participant);
			}

		}
	
		foreach (Participant part in encounter.participants) {
			part.create_unique_name ();
		}

		foreach (Participant self in encounter.participants) 
		{
			foreach (Participant other in encounter.participants) 
			{
				if (Enemies.Contains (other) && Enemies.Contains (self)) {
					self.aggros.Add (other, 50);
				} else {
					self.aggros.Add (other, -50);
				}	
			}
			
		}

		encounter.Initiate_Encounter ();
	}


	public void Quit_Encounter()
	{
		//delete the participants
		//manage the results
		//go back to the map.
		encounter.gameObject.SetActive(false);


	}





	public void player_button_click()
	{
	
	}


    //this is for moving highlights of home and job.. but it could be used to disappear places that have nothing in them...  or hidden places like the thieves guild.
    public void refresh_map(bool full_refresh=false)
    {
		if (full_refresh) print ("full map refresh"); 
		else print ("map refresh");
		foreach (GameObject location in DataManager.locations) {
			//this might should be done less often.
			location.transform.GetChild (1).GetChild (0).GetComponent<Image> ().color = default_color;

			var inventory = location.GetComponent<Inventory> ();
			if (inventory.home_inventory) {
				if (location.name == Home.location) {
					location.SetActive (true);
					location.transform.GetChild (1).GetChild (0).GetComponent<Image> ().color = Color.magenta;
				} else {
					location.SetActive (false);
				}
			}
            //job should start as beggar...
            else if (location.name == Job.location) {
				location.transform.GetChild (1).GetChild (0).GetComponent<Image> ().color = Color.cyan;
				location.SetActive (true);
			} else if (full_refresh){ 

				 if (check_conditions_of_inventory (inventory)) 
					location.SetActive (true);
				 else location.SetActive (false);
			}

			//should probably be an always active property of inventory...
        }
    }

   
	public bool check_conditions_of_inventory(Inventory inventory)
	{
		foreach (Card card in inventory.cards)
		{
			if (card.type!="Job" && conditions.check_conditions (card))
				return true;
		}
	return false;
	}







    //so we can equip nothing to the arm...
    //or we can equip something...
    public void Equip(string equip_to, Item item = null)
    {
        Equipment[equip_to] = item;


        //    foreach (Tuple<string, Item> equipped_item in Equipment)
        //    {

        //        if (equipped_item.First == item.First)
        //        {
        //            Equipment.Remove(item);
        //            print("removing " + equipped_item.Second.card_name);
        //        }
        //    }
        //Equipment.Add(equip_to, item);
        //print("equipping " + item.Second.card_name);
        //}

        refresh_modifiers();
        return;
    }

    public void refresh_modifiers()
    {

        foreach (Stat stat in Stats)
        {
            stat.Actual = stat.Base;
        }
		foreach (Stat stat in Reputations)
		{
			stat.Actual = stat.Base;
		}
		Discount.Clear ();


		actual_speed = base_walking_speed;
		actual_walking_speed = base_walking_speed;

        foreach (KeyValuePair<string, Item> equipped_item in Equipment)
        {
            if (equipped_item.Value != null)
            { 
				if(!equipped_item.Value.gradual)
           		 conditions.modify_attributes(equipped_item.Value.stat_modifiers, false);
				
            //equipped_item.Value.equipped = true;  //this is redundnant
            }
        }

		foreach (Card inventory_card in inventory.cards)
		{
			Item inventory_item = inventory_card as Item;
			if (inventory_item.type=="Status" && !inventory_item.gradual)

			{ 
				conditions.modify_attributes(inventory_item.stat_modifiers, false);
			}
		}

		if (actual_speed<actual_walking_speed) actual_speed= actual_walking_speed;
		if (actual_speed < .3f)
			actual_speed = .3f; 
 
        view_stats.refresh_stats();
    }

	public string get_reputation_description(Stat stat)
	{
		if (!stat.Repuation) Debug.LogError("Not a reputation stat");
		return get_reputation_description (stat.Name, stat.Actual);
	}

	public string get_reputation_description(string stat_name, float amount)
	{

		if (amount>-25 && amount<25) return "";
		for (var i = 0; i < Reputations.Count; i++) 
		{
			if (stat_name == Reputations[i].Name) 
			{
				foreach (KeyValuePair<int,string[]> entry in Reputation_terms)
				{

					if (amount >= entry.Key) {
						//						Debug.Log (entry.Value[i]);
						return entry.Value [i];
					}
				}
				Debug.LogError ("Looking for reputation, reputation found but not value.");
			}
		}
		Debug.LogError ("Looking for reputation, reputation name not found."+ stat_name);
		return "";
	}





    public void turn_start()
        {
            if (TurnManager.week != 0)
            {
            
                GetComponent<Controller>().MoveToAndProcess(location);

                Cards_Played_This_Turn.Clear();
                if (food < 3)
                {
                    int hunger = 3 - food;

                    string text_to_display = "You feel hungry!";
                    uitext.DisplayText(text_to_display,true);


                    Debug.Log("You feel hungry!");
                    give_stress(5 * hunger);
                    give_time(1 * hunger);
                }

                give_food(-3);

            }


		test_encounter ();

        }
	public bool pay_rent(Job home = null, bool first_month = false)
	{
		if (home == null)
			home = Home;
		
		int rent = 0;
		if (first_month)
			rent = Mathf.RoundToInt(home.gold * DataManager.home_move_in_factor);
		else
			rent = Home.gold;

		if (gold + rent < 0) 
			return false;
		else {
			give_gold (rent);
			return true;		
		}

	}

	public void check_time()
	{
		
		if (cardimagemanager.Active_description_card_choice() > 0)
			return;

		if (time >= maxtime) TurnManager.end_turn();
	}


	#region Give_Stats
    public void give_gold(int amount)
    {
		
		//this should be a boolean that returns failures when you're too poor.....
        gold += amount;
        if (gold > maxgold) gold = maxgold;
		if (gold < mingold) {
			gold = mingold;
			Debug.LogError ("Taking gold down to zero");
		}

        refresh_sliders();
    }


    public void give_food(int amount=1)
{
    food += amount;
    if (food > maxfood) food = maxfood;
    if (food < minfood) food = minfood;
    refresh_sliders();
}
    public void give_stress(int amount)
{
    stress += amount;
    if (stress > maxstress) stress = maxstress;
    if (stress < minstress) stress = minstress;
    refresh_sliders();
}
    public void give_time(float amount)
{
    time += amount;
    if (time > maxtime) time = maxtime;
    if (time < mintime) time = mintime;
    refresh_sliders();

	check_time ();

}

	public void refresh_sliders()
	{
		gold_text.text = "Gold: "+gold.ToString();
		stress_slider.value = stress;
		time_slider.value = time;
		food_slider.value = food;


	}

    public void give_virtue(float amount)
{
    virtue += amount;
    if (virtue > maxvirtue) virtue = maxvirtue;
    if (virtue < minvirtue) virtue = minvirtue;
    refresh_sliders();

    if (virtue >= maxvirtue) TurnManager.end_turn();

}


	public void give_job_exp(string job_name, int amount = 1)
    {
        Job_Exp[job_name] += amount;
        try
        {
			if (Job_Dictionary.Jobs[job_name].mastery == Job_Exp[job_name] )
            {


				if (DataManager.game_playing)
				{
					View_Inventory view =cardimagemanager.BottomView;
					print("You mastered soemthing");						
					//yikes.
					//uitext.add_to_current(Job_Dictionary.Jobs[job_name].action);
					//uitext.DisplayText("Way to go Valentine, you mastered " + job_name);
					//view.View_Description (this,view.card_image_manager.get_sprite_from_name(job_name),job_name,Job_Dictionary.Jobs[job_name].mastery_description);
					//this description is probably duplicated.. should just have the description on the reward card?
					//actualyl do we really need any kind of mastery description?  won't that be implicit when you acquire a card or whatever?
					Job_Dictionary.Jobs[job_name].DoAction(view,true,true,"Way to go Valentine, you mastered " + job_name);
					//view is how we know who is doing the action...



				}
            }
        }
        catch
        {
            print("couldn't find job " + job_name);
        }
        //check for new actions... and tell the player about them...
        //wonder if somewhere we coul djust have a list of booleans for whether hte player has access to each card or whatever?
        //and include it in the modify stats code instead of in the view_inventory/conditions code...

        

    }
 	void give_speed(float amount, bool change_base=false)
	{
		actual_walking_speed += amount;
	}


	void give_discount(string location, float amount)
	{
		float current_discount = 0;
		if (Discount.TryGetValue (location, out current_discount)) 
		{
			amount += current_discount;
			Discount.Remove (location);
			//a little complicated!
		}
		amount *= .01f;
		Discount.Add (location, amount);

	}


	public void give_item(string item_name, int count =1)
	{
		Item new_item = Item_Dictionary.Items [item_name].Clone();
	//	new_item.stacks = stacks;
		give_item (new_item,count);
		refresh_modifiers();
	}
	public void give_item(Item new_item, int count=1)
	{
		//Item dictionary_item = Item_Dictionary.Items [item_name];
		//lets say negative 1 deletes it...

		if (new_item.stackable) {
			bool item_found = false;
			foreach (Card inventory_card in inventory.cards) {
				if (inventory_card.type == "Item" && inventory_card.card_name == new_item.card_name) {
					item_found = true;
					Item inventory_item = inventory_card as Item;
					inventory_item.stacks += count;
					if (inventory_item.stacks <= 0)
						inventory.cards.Remove (inventory_card);
					refresh_modifiers();
					return;
				}
			}
			if (!item_found && count >= 0) {
				new_item.stacks = count;
				inventory.cards.Add (new_item);
				refresh_modifiers();
				return;
			}
		} else //not stackable
			if (count > 0) {
			for (var i = 0; i < count; i++) {
				inventory.cards.Add (new_item);
			}
			refresh_modifiers();
			return;
			} else if (count < 0) {
				for (var i = 0; i > count; i--) {
					Card card_to_remove = null;
					foreach (Card inventory_card in inventory.cards) {
							if (inventory_card.card_name == new_item.card_name)	card_to_remove = inventory_card;
					}
				if ( card_to_remove!=null) inventory.cards.Remove (card_to_remove);
				}
				refresh_modifiers();
				return;
			}
	}


	public Stat get_stat_from_string(string stat_name)
	{
		foreach (Stat stat in Stats)
		{
			if (stat.Name == stat_name) return stat;
		}
		foreach (Stat stat in Reputations)
		{
			if (stat.Name == stat_name) return stat;
		}
			
		return null;


	}
	public void change_stat(string stat_name, float amount, bool Change_Base=true)
	{

		foreach (Stat stat in Stats){
			if (stat.Name == stat_name) {
				stat.change (amount, Change_Base);
				view_stats.refresh_stats();
				return;
			}
		}


		foreach (Stat reputation in Reputations) { 
			if (reputation.Name == stat_name) {
				reputation.change (amount, Change_Base);
				view_stats.refresh_stats();
				return;
			}
		}

		if (stat_name == "MaxSpeed") {
			if (actual_speed < amount)
				actual_speed = amount;

			return;
		}

		else if (stat_name == "Speed") give_speed (amount, Change_Base);

		else if (stat_name == "Stress") give_stress((int)amount);
		else if (stat_name == "Food") give_food((int)amount);
		else if (stat_name == "Gold") give_gold((int)amount);
		else if (stat_name == "Virtue") give_virtue(amount);

		else if (stat_name.StartsWith("Discount") )
		{
			foreach(GameObject location in DataManager.locations)
			{
				if(stat_name.EndsWith(location.name)) give_discount(location.name,amount);

			}

		} 


	}
		
	#endregion Give_Stats


	

	#region player cards
	public void delete_cards(List <Card> cards_to_delete)
	{
		//this will need more cleanup for things like jobs and homes.
		foreach (Card card in cards_to_delete) {
			delete_card (card);
		}
	}
	public void delete_card(Card card)
	{
		if (card.type == "Item") {
			Item item = card as Item;

			if (is_item_equipped (item))
				Equip (item.equip_to);
		}

		inventory.cards.Remove (card);
		//Destroy (card);


	}

	public void changejob(Job job, bool remove=false)
	{
		if (job.type == "Job")
		{

			//base_job  if (remove) Job = Job no_job;
			if (remove) {
				Job no_job;
				Job_Dictionary.Jobs.TryGetValue (DataManager.base_job, out no_job);
				Job = no_job.Clone();
			}
			else Job = job.Clone();

			refresh_map();
			return;
		}
		else if (job.type == "Class")
		{
			Enrolled_Classes.Add(job.card_name);
			return;
		}
		else if (job.type == "Home")
		{


			if (remove) {
				Job no_home;
				Job_Dictionary.Jobs.TryGetValue (DataManager.base_home, out no_home);
				Home = no_home.Clone();

			}
			else
				Home = job.Clone();

			weeks_till_rent = 4;
			refresh_map ();
			return;		
		}
		else print(job.card_name + " was not able to be added");
	}



	public bool is_item_equipped(Item item){
		bool found = false;	

		foreach (KeyValuePair<string, Item> entry in Equipment)
		{
			if (entry.Value==item)
				found = true;		
		}
		return found;

	}



	//maybe it should return the card that the player has?
	public Card GetPlayerCard(Card card)
		{
		
			if (card.type == "Job")
			{
				if (Job.card_name == card.card_name) return card;
			}

			else if (card.type == "Class")
			{
				foreach (string enrolled_class in Enrolled_Classes)
				{
					if (enrolled_class == card.card_name) return card;
				}
			}
			else if (card.type == "Home")
			{
			if (Home.card_name == card.card_name) return card;
				//no this is not it.. the home has to be a card not a place?
			}
			else if (card.type == "Item")
			{
				foreach (Item item in inventory.cards)
					//this is a bad place for this!
				{

					if (item.card_name == card.card_name) return item;

				}
			}

		return null;
		}

	//this could certainly be simplified!
	public bool DoesPlayerHave(Card card)
	{
		if (card.type == "Job")
		{
			if (Job.card_name == card.card_name) return true;
		}

		else if (card.type == "Class")
		{
			foreach (string enrolled_class in Enrolled_Classes)
			{
				if (enrolled_class == card.card_name) return true;
			}
		}
		else if (card.type == "Home")
		{
			if (Home.card_name == card.card_name) return true;
			//no this is not it.. the home has to be a card not a place?
		}
		else if (card.type == "Item")
		{
			foreach (Item item in inventory.cards)
				//this is a bad place for this!
			{

				if (item.card_name == card.card_name) return true;

			}
		}

		return false;
	}
		
	//this could certainly be simplified!
	public bool DoesPlayerHave(string card_name, int count =1)
	{
		if (Job.card_name == card_name) return true;
	
		if (Home.card_name == card_name) return true;
			
		foreach (string enrolled_class in Enrolled_Classes){
				if (enrolled_class == card_name) return true;
		}
				
		foreach (Item item in inventory.cards){
			if (item.card_name == card_name) {
				if (item.stacks >= count)
					return true;
			}
		}
		return false;
	}
	#endregion


	#region player actions


	public void DoActionString(string action_string , bool display_text=false, string optional_text="",View_Inventory view =null , string display_card = "")
	{

		//eg. Random|GiveItem|Potion|2/And|GiveGold|0&Display|How Unlucky!
		//Random|And|GiveItem|Potion|2&Text|YouGetItem

		//eg. Random|GiveItem|Potion|2/And|GiveGold|0&Text|How Unlucky!
		//@#$%^&
		//   Random|GiveItem|Potion|2#And|GiveGold|0@Text|How Unlucky!

		print ("Doing: " + action_string);





		string[] parts = action_string.Split ('|');


		uitext.add_to_current (action_string);



		if (parts [0] == "Choose") {
			//Choose|GiveItem|Potion
			//Choose|
			//eg. Choose|GiveItem|Potion|2/And|GiveGold|40
			stored_actions.Clear ();

			//view.already_refreshed = true;
			string[] multiactions = action_string.Substring (parts[0].Length+1).Split ('%');

			//if there's just one option, then just display the message on that actions.
			if (multiactions.Length == 1) {
				uitext.add_to_current (multiactions [0]);
				uitext.print_all_actions (uitext.current_actions_to_display);

			}
				else 
				uitext.add_to_current ("Text|Choose: ");

			for (var i=0;i<multiactions.Length;i++)
			{
				stored_actions.Add (multiactions [i]);//this is super lazy-- i'm just turning the array into a list so i don't have to worry about length..


				string description="";
				string header ="";
				string card_name ="";
				string[] parts_of_stored = stored_actions[i].Split ('|');
				//also won't show the item... so actually i do want the item if possible...
				if (parts_of_stored [0] == "GiveItem"||parts_of_stored [0] == "Unlock") {
					card_name = parts_of_stored [1];
					header = card_name;

					if (parts_of_stored.Length > 2)
						header += " x" + parts_of_stored [2];


				} else {
					description = cardimagemanager.get_standard_action_description (stored_actions [i]);
					//get standard action header
					//card name which is basd on the action-- ie. GiveStat|Gold|20 needs to be a card which is a picture of a bag of gold.
					//in the case of actual charactr stats w could just reuse class pictures......
				}
				//get action description?
				//how do we handle ands?//what baout randoms?
				//answer we define cards for ands and randoms...
				//ie. choose|GiveItem|Potion|2%card|Take Risk
				//and take risk is itself an action card, with a dfined action.



				cardimagemanager.Get_Immediate_Description(DataManager.get_dictionary_card(card_name), header, description, i+1);

			}


			return;
			//somewher in th course of th rst of hte job action, this is getting removed.. probably the update...


			//somehow need to turn all thse into cards...
			//so need both a picture and a description..



			//choose needs to immediatly know the display cards somehow...
			//it'd b cool if could stack them also...
			//display some items...


			//also this would impede splits of splits unless i coudl somehow directly refer to another action...




		}




		if (parts [0] == "And") {
			string[] multiactions = action_string.Substring (parts[0].Length+1).Split ('&');
			foreach (string action_part in multiactions) {
				DoActionString (action_part , view);
			}
			return;
		}			



		else if (parts [0] == "Random") {
			//eg. Random|GiveItem|Potion|2/And|GiveGold|0&Text|How Unlucky!
			//Random|And|GiveItem|Potion|2&Text|YouGetItem
			string[] possibilities = action_string.Substring (parts[0].Length+1).Split ('/');

			int choice = 1;
			choice = UnityEngine.Random.Range (0, possibilities.Length);
			DoActionString (possibilities [choice], view );
			return;
		} 

		else if (parts [0] == "Unlock") {
			string text = "You've unlocked a new skill: ";
			text += parts [1];
			print (text);



			display_card = parts [1];

		} 


		else if (parts [0] == "Text") {
			//Text|You were caught!|GiveStat|Gold|-5
			string text = parts [1];

			//this is getting buried...
			string remainder = action_string.Substring (6 + text.Length);
			DoActionString (remainder , view );
			return;
		} 
		else if (parts [0] == "Acquire") {
			//eg. acquire/Job
			//eg. acquire/Blacksmith
			if (view.current_inventory != null) {
				view.Process (view.current_inventory, parts [1]);
			}
			view.already_refreshed = true;
		
		}
		else if (parts [0] == "Unaquire") {
			//eg. Unacquire/Job
			//eg. Unacquire/Status
			//eg. Unacquire|Pet&GiveStat|Strength|8

			//so say you want to sacrifice... you have to select a pet, upon selecting the pet it does the work...
			//so you the view opens up... it calls a function in the view... eg.
			//unaquire (dog)
			//that function deletes the dog?
			//calls for an action... but how does that view know what action to call?
			//could send the action string to view...  //of course that's going to float weirdly unless it gets cleaned up...
			//action on completion... send to view process...
			string[] AndParts = action_string.Split ('&');
			//we should be unaquiring the player.inventory... 


			//this isn't right.. the view is probably of a location, but we want to unaquire from the player... so somehow we should bring up the player view, and unaquire from there?
			if (view.current_inventory != null) {
				//view.process(inventory, Pet  , GiveStat|Strength|8)
				view.Process (inventory, parts [1],AndParts[1],true);

				//should really just have a list of stored actions???
				//which can be called at the end of a viwe refresh or somewhere else...
				//no need to esnd them TO the view itself though right?
			}

			view.already_refreshed = true;
			//if you click on a job and you alreayd have it then you lose it?
			//what if it's a item (status) how do we make you lose that?
			//Acquire your own statuses...
			//If you acquire something you alreayd have...
		}



		//choice
		//else if (type == "Job")
		//Master Job... OR should this be in player variables.. since it's standard..  and then what you acquire should be the aciton.

		//	if (parts [0] == "MustSpend") {
		//		if (DoesPlayerHave(parts[1]))
		//	}

		//how do we allow for pausing between multiple happenings?
		else if (parts [0] == "GiveItem") {
			//eg. GiveItem/Potion/2,  GiveItem/Sword
			int count = 1;
			if (parts.Length == 3)
				int.TryParse (parts [2], out count);
			Item check = null;
			Item_Dictionary.Items.TryGetValue (parts [1], out check);
			if (check == null)
				Debug.LogError ("faiure with " + parts [1]);
			cardimagemanager.transfer (null, false, inventory, null, default(Vector3), default(Vector3), check);
			//view.card_image_manager.transfer (null, false, inventory, null, default(Vector3), default(Vector3), check);
			give_item (parts [1], count);


			//so this is sort fo good.. but i want to force the player to click befor the give item transfer happens......
			//how in the world do I do that.. they're not even in the right order.

			display_card = parts [1];
		} 
		else if (parts [0] == "Message") {
			view.uitext.DisplayText (parts [1]);
		} 

		else if (parts [0] == "GiveStat") {
			float amount = 0;
			if (float.TryParse (parts [2], out amount)) {
				change_stat (parts [1], amount, true);
			}
		} 
		else if (parts [0] == "GiveStatTemp") {
			float amount = 0;
			if (float.TryParse (parts [2], out amount)) {
				change_stat (parts [1], amount, false);
			}
		} 

		else if (parts [0] == "MakeHome") {
			base_home = parts [1];
			changejob (DataManager.get_dictionary_card (parts [1], "Job") as Job);

			
		} 
		else if (parts [0] == "DestroyItemType") {
			List <Card> cards_to_delete = new List <Card>();

			foreach (Card card in inventory.cards) {
				if (card.subtype == parts [1]) 
				{
					cards_to_delete.Add (card);
				}
			}
			delete_cards (cards_to_delete);


		}
		else
			Debug.LogError ("ruhroh, " + parts [0] + " doesn't mean anything as an action");




		//right now we aren't displaying anything okay?
		//if (display_card!="") 
		//{
			//cardimagemanager.View_Description(DataManager.get_dictionary_card(display_card));
			//cardimagemanager.Get_Immediate_Description(DataManager.get_dictionary_card(display_card));
			//print ("trying to display " + display_card);
			//			cardimagemanager.View_Description (
			//				DataManager.get_sprite_from_name (display_card), display_card, DataManager.get_dictionary_card(display_card).description);
		//}
		if(display_text)
		{
			uitext.print_all_actions (uitext.current_actions_to_display, optional_text);
		}

		//if (outcome_text!="") 
		//	uitext.DisplayText (outcome_text);
	}

	#endregion


}




[System.Serializable]
public class Stat
{
    //public event System.EventHandler OnValueChange;

	public Stat(string name = "", float sBase = 0, float actual = 0, float min = 0, float max = 999, bool repuation = false, float decay = 0)
    {
        Name = name;
        Base = sBase;
        Actual = actual;
        Min = min;
        Max = max;

		Repuation = repuation;
		Decay = decay;
    }
    [SerializeField]
    public string Name;
    [SerializeField]
    public float Base;
    [SerializeField]
    public float Actual;
    [SerializeField]
    public float Min;
    [SerializeField]
    public float Max;
    [SerializeField]
	public bool Repuation;

    [SerializeField]
	public float Decay;


    //    [SerializeField]    public List<modifier> modifiers;





    public void change(float amount, bool Change_Base = true)
    {

        if (Change_Base)
        {
            Base += amount;
            if (Base < Min) Base = Min;
            if (Base > Max) Base = Max;
        }
            Actual += amount;
            if (Actual < Min) Actual = Min;
            if (Actual > Max) Actual = Max;
    }


}



//this is needlessly complciated...




//[System.Serializable]  public class Stat_Modifier
//{
//    public float Amount;
//    public string Reason;
//}
//public Card GetPlayerCard(string card_name)
//{
//wouldn't it be better if we just had a primary key?
//this is actually less effective than looping through dictionaries...
//Card dictionary_card = DataManager.get_dictionary_card(card_name);
//return GetPlayerCard (dictionary_card);
//don't even have a home card.. which seems particularly dumb right now...
//		if (Job.card_name == card_name)
//			return Job as Card;
//		if (Home.name == card_name) return true;
//		foreach (string enrolled_class in Enrolled_Classes)
//		{
//			if (enrolled_class == card.card_name) return true;
//		}
//		foreach (Item item in inventory.cards)
//		{
//			if (item.card_name == card.card_name) return true;
//		}
//		return null;
//}
//	public bool DoesPlayerHave(string card_name, int count=1)
//	{
//		Card player_card = GetPlayerCard (card_name);
//		if (player_card == null)
//			return false;
//		else if (count == 1)
//			return true;
//		
//		Item player_item = player_card as Item;
//		if (player_item.stacks >= count)
//			return true;
//		else
//			return false;
//	}