using System.Reflection;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;


//this should be part of a deck area script?
//drag enabled.
//drop enabled.

public class DropMe : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
	public Image containerImage;
	public Image receivingImage;
	private Color normalColor;
	public Color highlightColor = Color.yellow;

    public Dictionary<int, GameObject> m_HoverIcons = new Dictionary<int, GameObject>();
    public Dictionary<int, bool> m_ValidDrop = new Dictionary<int, bool>();
    //static public Dictionary<int,bool> All_ValidDrop = new Dictionary<int, bool>();



    public void OnEnable ()
	{
		if (containerImage != null)
			normalColor = containerImage.color;
	}
	
	public void OnDrop(PointerEventData data)
	{
		containerImage.color = normalColor;
		
		if (receivingImage == null)
			return;
		
		Sprite dropSprite = GetDropSprite (data);
		if (dropSprite != null)
			receivingImage.overrideSprite = dropSprite;

        if (dropSprite != null)
            receivingImage.sprite = dropSprite;

        //tell the drag me that it has a place to drop to?

    }




    public void OnPointerEnter(PointerEventData eventData)
	{
		if (containerImage == null)
			return;


        Sprite dropSprite = GetDropSprite(eventData);
        if (dropSprite != null)
        {
            containerImage.color = highlightColor;
            m_ValidDrop[eventData.pointerId]=true;
        }
        else {
            //make it popup...
            print("entered!");
            var canvas = DragMe.FindInParents<Canvas>(gameObject);
            if (canvas == null) return;
            m_HoverIcons[eventData.pointerId] = new GameObject("icon");


            m_HoverIcons[eventData.pointerId].transform.SetParent(transform, false);

            var image = m_HoverIcons[eventData.pointerId].AddComponent<Image>();
            image.preserveAspect = true;

            // The icon will be under the cursor.
            // We want it to be ignored by the event system.
            var group = m_HoverIcons[eventData.pointerId].AddComponent<CanvasGroup>();
            group.blocksRaycasts = false;

            image.sprite = GetComponent<Image>().sprite;
            //borrow the sprite from this-- only maybe multiple images here...
            var rt = m_HoverIcons[eventData.pointerId].GetComponent<RectTransform>();

            rt.sizeDelta = GetComponent<RectTransform>().sizeDelta;
            m_HoverIcons[eventData.pointerId].transform.SetParent(canvas.transform, false);
            m_HoverIcons[eventData.pointerId].transform.SetAsLastSibling();


            rt.position = GetComponent<RectTransform>().position;
            rt.rotation = GetComponent<RectTransform>().rotation;
            //rt.sizeDelta = new Vector2(100, 100);

        }
    }

	public void OnPointerExit(PointerEventData eventData)
	{
        print("exited!");

        if (containerImage == null)
			return;


        if (m_HoverIcons[eventData.pointerId] != null)
            Destroy(m_HoverIcons[eventData.pointerId]);

        m_HoverIcons[eventData.pointerId] = null;


        containerImage.color = normalColor;
	}
	
	private Sprite GetDropSprite(PointerEventData data)
	{
		var originalObj = data.pointerDrag;
		if (originalObj == null)
			return null;
		
		var dragMe = originalObj.GetComponent<DragMe>();
		if (dragMe == null)
			return null;
		
		var srcImage = originalObj.GetComponent<Image>();
		if (srcImage == null)
			return null;
		
		return srcImage.sprite;
	}
}
