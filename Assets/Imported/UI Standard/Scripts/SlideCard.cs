﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideCard : MonoBehaviour {

    public RectTransform rt;
   // public Vector2 targetPosition;
    public float animationTime = 1f;

    private void Awake()
    {
        rt = this.GetComponent<RectTransform>();
    }

    public void SlideIn(Vector2 targetPosition)
    {
        iTween.ValueTo(rt.gameObject, iTween.Hash(
            "from", rt.anchoredPosition,
            "to", targetPosition,
            "time", animationTime,
            "onupdatetarget", this.gameObject,
            "onupdate", "MoveGuiElement"));
    }

    public void MoveGuiElement(Vector2 position)
    {
        rt.anchoredPosition = position;

    }

}
